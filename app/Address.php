<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $table = 'address';
    protected $fillable = ['state', 'city', 'district', 'number', 'complement', 'cep', 'street', 'user_id'];

    public function user(){
        return $this->belongs(User::class);
    }

    public function order(){
        return $this->hasMany(Order::class);
    }
}
