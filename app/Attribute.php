<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    //
    protected $table = 'kit_attributes';
    public $fillable = ['name', 'quantity', 'kit_id'];

    public function kit(){
        return $this->belongsTo(Kit::class);
    }
}
