<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'discount_coupon';
    public $fillable = ['code', 'date_start', 'date_end', 'valid', 'type_id'];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function user(){
        return $this->belongsToMany('App\User', 'user_x_coupon')->withPivot('id');
    }
}
