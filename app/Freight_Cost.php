<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Freight_Cost extends Model
{
    protected $table = 'freight_cost';

    protected $fillable = ['cep_origin','cep_destination','price_freight', 'address'];
}
