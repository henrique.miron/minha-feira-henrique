<?php

namespace App\Helpers;
use DateTime;


class DateHelper
{
    public static function BRtoEN($date)
    {
        $date = explode("/",$date);
        return $date[2].'-'.$date[1].'-'.$date[0];
    }
    public static function ENtoBR($date)
    {
        return date("d/m/Y", strtotime($date));
    }
    public static function CCDate($date){
        $date = '01/'.$date;    
        return Self::BRtoEN($date);
    }

    function getDay($dia){
        $dayofweek = app('App\Helpers\PedidoHelper')->diaEn($dia);
        $date = new DateTime();
        $date->modify('next '.$dayofweek);
        return $date->format('Y-m-d');
    }

    function ramain($date){
        $today = time();
        $date = strtotime($date);
        $ramain = $date - $today;
        return round($ramain / (60 * 60 * 24));
    }
}