<?php

namespace App\Helpers;


class  PedidoHelper
{
    public static function formatarDiaEntrega($dia)
    {
        switch ($dia) {
            case '0';
            case 'seg':
            return 'Segunda';
            case '1';
            case 'ter':
            return 'Terça';
            case '2';
            case 'qua':
            return 'Quarta';
            case '3';
            case 'qui':
            return 'Quinta';
            case '4';
            case 'sex':
            return 'Sexta';
        }
    }

    public static function diaEn($dia)
    {
        switch ($dia) {
            case '0';
            return 'Monday';
            case '1';
            return 'Tuesday';
            case '2';
            return 'Wednesday';
            case '3';
            return 'Thursday';
            case '4';
            return 'Friday';
        }
    }

    public static  function formatarPeriodoEntrega($periodo)
    {
        switch ($periodo) {
            case '0';
            case 'manha' :
            return 'Manhã';
            case '1';
            case 'tarde':
            return 'Tarde';
            case '2';
            case 'noite' :
            return 'Noite';
        }
    }

}