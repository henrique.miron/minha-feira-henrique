<?php

namespace App\Helpers;


class  AssinaturaHelper
{
    public static function formatarStatus($status)
    {
        switch ($status) {
            case '1':
            return 'Aguardando pagamento';
            case '2':
            return 'Em análise';
            case '3';
            return 'Paga';
            case '4':
            return 'Disponível';
            case '5':
            return 'Em disputa';
            case '6':
            return 'Devolvida';
            case '7':
            return 'Cancelada';
        }
    }

}