<?php

namespace App\Helpers;


class  orderHelper
{
    public static function formatarStatus($status)
    {
        switch ($status) {
            case '0':
            return 'Aguardando';
            case '1':
            return 'Entregue';
        }
    }
    
    function ccMask($number) {
        return substr($number, 0, 4) . str_repeat('*', strlen($number) - 8) . substr($number, -4);
    }
    
    function calcData($order) {
        $data = strtotime(app('App\Helpers\DateHelper')->getDay($order->weekday));
        $data = date('Y-m-d', strtotime('+ 1 week', $data));
        // else{
        //     $data = date('Y-m-d', strtotime('+'.$key.' months', $data));
        // }
        app('App\Http\Controllers\DeliveryController')->store($order, $data);
    }
    
}