<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use Auth;
use DB;
use App\Http\Requests\compraRequest;
class AddressController extends Controller
{
    //
    public function store($request){
        DB::beginTransaction();
        try{
            $address = new Address;
            $address->user_id = Auth::user()->id;
            $address->fill($request->all());
            $address->save();
            DB::commit();
        }catch(Exception $e){
            return $e;
        }
        
    }
    public function newAddress(compraRequest $request){
        return $request;
        Self::alterMain($request);
        Self::store($request);
    }
    public function update(compraRequest $request){
        $endereco = Address::find($request->id);
        $endereco->update($request->all());
        return redirect()->back()->with('msg', 'Endereço alterado');
    }
    public function alterar(Request $request){
        $data = [$request->all(), Auth::user()];
        Self::alterMain($request);
        $card = UserCard::find($request->id);
        $card->main = 1;
        $card->update();
        return redirect()->back()->with('msg', 'Cartão principal alterado');
    }
    public function alterMain($data){
        if($data->has('main')){
            $currents = Auth::user()->where('main', 1)->get();
            foreach($currents as $current){
                $current->main = 0;
                $current->update();
            }
        }
    }

    public function show(){
        return Auth::user()->addresses;
    }
}
