<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Freight_Cost;

class AdminController extends Controller
{
    //
    public function cep(){
        $data = Freight_Cost::all();

        return view('admin-cep')->with('data', $data);
        
    }
}
