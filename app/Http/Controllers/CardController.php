<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserCard;
use Auth;
use DB;
use App\Http\Requests\compraRequest;
class CardController extends Controller
{
    //
    public function store($request){
        DB::beginTransaction();
        try{
            $card = new UserCard;
            $card->user_id = Auth::user()->id;
            $card->fill($request->all());
            if($request->has('main')){
                $card->main = 1;
            }else{
                $card->main = 0;
            }
            $card->valid_thru = app('App\Helpers\DateHelper')->CCDate($request->valid_thru);
            $card->save();
            DB::commit();
        }catch(Exception $e){
            return $e;
        }
        
    }
    public function newCard(compraRequest $request){
        $data = [$request->all(), Auth::user()];
        $pagseguro = app('App\Http\Controllers\PagseguroController')->alterCard($data);
        if(array_key_exists('error', $pagseguro)){
            DB::rollBack();
            return redirect()->back()->withInput()->withErrors($pagseguro['error']);
        }
        Self::alterMain($request);
        Self::store($request);
        return redirect()->back()->with('msg', 'Cartão Cadastrado');
    }
    public function alterar(Request $request){
        $request['card_name'] = UserCard::find($request->id)->card_name;
        $data = [$request->all(), Auth::user()];
        $pagseguro = app('App\Http\Controllers\PagseguroController')->alterCard($data);
        if(array_key_exists('error', $pagseguro)){
            //DB::rollBack();
            return redirect()->back()->withInput()->withErrors($pagseguro['error']);
        }
        Self::alterMain($request);
        $card = UserCard::find($request->id);
        if($request->has('main')){
            $card->main = 1;
        }else{
            $card->main = 0;
        }
        $card->update();
        return redirect()->back()->with('msg', 'Cartão principal alterado');
    }
    public function alterMain($data){
        if($data->has('main')){
            $currents = Auth::user()->cards()->where('main', 1)->get();
            foreach($currents as $current){
                $current->main = 0;
                $current->update();
            }
        }
    }

    public function excluir($id){
        UserCard::find($id)->delete();
        return redirect()->back();
    }
}
