<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;
use Auth;
use Carbon\Carbon;

class CouponController extends Controller
{
    //

    public function desconto(Request $request){
        $cupom = Coupon::where('code', $request->cupom);

        if($cupom->count() == 0){
            return 'false';
        }

        $cupom = $cupom->first();

        if(Self::periodo($cupom) == false || Self::valido($cupom) == false){
            return 'false';
        };

        if($cupom->user()->first() !== NULL){
            if($cupom->user()->first()->pivot->user_id == Auth::user()->id){
                return 'false';
            };
        }

        $porcentual = $cupom->type()->first();

        $desconto = (intval(\session('kit')->price)*intval($porcentual->percentage))/100;
        $valorFinal = \session('kit')->price-$desconto;

        \session('store')['price_discount'] = $desconto;

        return $data = array('Desconto' => $desconto, 'Valor' => intval(\session('kit')->price), 'Valor Final' => $valorFinal, 'Cupom' => $cupom);
    }

    public function periodo($cupom){
        if(strtotime($cupom->date_start) < strtotime(Carbon::now()) && strtotime($cupom->date_end) < strtotime(Carbon::now())){
            $cupom->update(['valid' => 0]);
            return false;
        }
        return true;
    }

    public function valido($cupom){
        if($cupom->valid == 0){
            return false;
        }
        return true;
    }
    
}
