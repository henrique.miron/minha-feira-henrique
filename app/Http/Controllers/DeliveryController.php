<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Delivery;

class DeliveryController extends Controller
{
    //
    public function store($request, $data){
        
        $delivery = new Delivery;
        $delivery->date = $data;
        $delivery->fill(['order_id' => $request->id, 'user_id' => $request->subscription->user_id])->save();
    }

    public function show(){
        return Delivery::all();
    }

    public function update($id){
        return Delivery::find($id);
    }
}
