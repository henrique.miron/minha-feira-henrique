<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Freight_Cost;
use Illuminate\Support\Facades\Session;


class FreightCostController extends Controller
{
    public function cep_available($cep)
    {
        return  Freight_Cost::where('cep_origin', '<=', $cep)->where('cep_destination', '>=', $cep)->get();
    }

    public function consultacep(Request $request)
    {
        $cep = $request->get('cep');
        $isAvailable = $this->cep_available($cep);

        if(count($isAvailable) > 0)
        {
            \session(['cep'=> $cep]);           
            return redirect('selecione-periodo-assinatura');
        } else {
            Session::flash('error','Ainda não entregamos na sua região');
            return view('informe-cep-assinatura');
        }
    }

    public function inserirCep(Request $request){
        $cep = new Freight_Cost;
        $cep->fill($request->all());
        $cep->save();

        return redirect()->back()->with('status', 'Sucesso!');
    }

    public function alterarCep(Request $request){
        $cep = Freight_Cost::find($request->id);
        $cep->update($request->all());
        
        return redirect('admin-cep')->with('status', 'CEP Alterado com Sucesso!');
        
    }

    public function getCep($id){
        return Freight_Cost::find($id);
    }
}

