<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\compraRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = app('App\Http\Controllers\KitController')->show();
        return view('principal', ['data' => $data]);
    }
    public function kits()
    {
        $data = app('App\Http\Controllers\KitController')->show();
        return view('kits', ['data' => $data]);
    }
    public function sobre()
    {
        return view('sobre-nos');
    }
    public function funciona()
    {
        return view('como-funciona');
    }
    public function login()
    {
        return view('login');
    }
    public function termos()
    {
        return view('termos-condicoes');
    }
    public function consultacep()
    {
        return view('Order.consulta-cep');
    }
    public function informecep()
    {
        return view('informe-cep-assinatura');
    }
    public function selecioneassinatura()
    {
        $data = ['periodoEntrega' => \Session::get('periodoEntrega'), 'diaEntrega' => \Session::get('diaEntrega')];
        return view('Order.selecione-periodo-assinatura', $data);
    }
    public function loginassinatura()
    {
        return view('login-assinatura');
    }
    public function checkoutassinatura(compraRequest $request)
    {
        app('App\Http\Controllers\OrderController')->startCheckout();

        return view('Order.checkout-assinatura', ['email' => $request->input('email'),
                'kit' => \session('kit'), 'periodoEntrega' => \session('periodoEntrega'),
                'diaEntrega' => \session('diaEntrega'), 'cep' => \session('cep')]);
    }
    public function checkoutlogadoassinatura()
    {
        app('App\Http\Controllers\OrderController')->startCheckout();

        return view('Order.checkout-logado-assinatura',
            ['kit' => \session('kit'), 'periodoEntrega' => \session('periodoEntrega'),
            'diaEntrega' => \session('diaEntrega'), 'cep' => \session('cep')]);
    }
    public function finalizarassinatura()
    {
        return view('Order.finalizar-assinatura');
    }
    public function painelhome()
    {
        return view('painel-home');
    }
     
     public function enderecobox()
    {
        return view('endereco-cadastrado');
    }
    public function adminhome()
    {
        return view('admin-home');
    }
    public function adminusuarios()
    {
        return view('admin-usuarios');
    }
    public function admincep()
    {
        return view('admin-cep');
    }

    public function cep()
    {
        return view('informe-cep-assinatura');
    }
}
