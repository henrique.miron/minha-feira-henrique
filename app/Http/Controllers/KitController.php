<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use App\Kit;
use App\Order;
use App\Attribute;
use DB;
use Auth;

class KitController extends Controller
{

    public function store(Request $request){
        DB::beginTransaction();
        try{
            $pagseguro = app('App\Http\Controllers\PagseguroController')->createPlan($request);

            $kit = new Kit;
            $kit->fill($request->all());
            $kit->code = $pagseguro['plano'];
            $kit->save();

            $attribute = new Attribute;
            $attribute->fill($request->all());
            $attribute->name = $request->name_attr;
            $attribute->kit_id = $kit->id;
            $attribute->save();

            DB::commit();
            return redirect()->back()->with('msg', 'Kit cadastrado!');
        }catch(Exception $e){
            return response()->json($e);
        }
    }

    public function update(Request $request){
        // return $request;
        DB::beginTransaction();
        try{
            $kit = Kit::find($request->id);
            $kit->update($request->all());
            
            $kit->attrs()->delete();

            foreach($request->attr_id as $key => $value){
                    $attr = new Attribute;
                    $attr->name = $request->attr_name[$key];
                    $attr->quantity = $request->attr_quantity[$key];
                    $attr->kit_id = $kit->id;
                    $attr->save();
                }
            
            DB::commit();
            return redirect()->back();
        }catch(Exception $e){
            return response()->json($e);
            
        }
    }

    public function show(){
        return Kit::all();
    }

    // public function delete($id){
    //     //$kit = Kit::find($id);
    //     //$kit->delete();
    //     return redirect()->back();
    // }

    public function selecioneKit(Request $request)
    {
        if($request->isMethod('post') && $request->input('kit'))
        {
            \session(['kit' => Kit::find($request->input('kit'))]);

            return view('informe-cep-assinatura');
        }
    }

    public function alterar(Request $request){
        if($request->isMethod('get')){
            return Kit::all();
        }
        

        if($request->session == 'true'){
            if($request->weekday !== null){
                \session(['diaEntrega' => $request->weekday]);
                return redirect()->back()->withInput();
            }

            if($request->period !== null){
                \session(['periodoEntrega' => $request->period]);
                return redirect()->back()->withInput();
            }
        }

        if($request->weekday !== null){
            foreach(Auth::user()->assinatura->orders as $key => $value){
                $data[$key] = strtotime(app('App\Helpers\DateHelper')->getDay($request->weekday));
                if($key == 0){
                    $data[$key] = date('Y-m-d', strtotime('+'.($key+1).' week', $data[$key]));
                }else{
                    $data[$key] = date('Y-m-d', strtotime('+'.$key.' months', $data[$key]));
                }
                $value->delivery->update(['date' => $data[$key]]);
            };
            Auth::user()->assinatura->orders()->update(['weekday' => $request->weekday]);
            return redirect()->back()->with('status', 'Dia alterado');
        }

        if($request->period !== null){
            Auth::user()->assinatura->orders()->update(['period' => $request->period]);
            return redirect()->back()->with('status', 'Periodo alterado');
        }

        if($request->kit !== null){
            
            \session(['kit' => Kit::find($request->kit)]);
            return redirect()->back()->withInput();
            // app('App\Http\PagseguroController')->
            // $order->update(['kit_id' => $request->kit]);
            // return redirect('meu-box')->with('status', 'Box alterado!');
        }

    }
}
