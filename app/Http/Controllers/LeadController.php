<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeadsRequest;
use Illuminate\Http\Request;
use App\Lead;

class LeadController extends Controller
{
    public function add(LeadsRequest $request)
    {
        Lead::create($request->all());
        return redirect('/')->with('msg', 'Cadastrado no sistema');
    }
}
