<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Order;
use App\Role;
use App\User;
use App\UserCard;
use App\Coupon;
use App\Kit;
use App\UserSubscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\boasVindas;
use App\Http\Requests\compraRequest;
use App\Http\Controllers\PagseguroController;
use PagSeguro;

class OrderController extends Controller
{
    
    public function store(compraRequest $request)
    {
        DB::beginTransaction();
        try {
            //usuario
            if(!$request->has('user_id')){
                $usuario = User::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                    ]);
                    $usuario->roles()->attach(Role::where('name', 'cliente')->first());
                    
                    
                    array_merge($request->all(), ['user_id' => $usuario->user_id]);
                    app('App\Http\Controllers\Auth\LoginController')->login($request);
                    //Mail::to(Auth::user()->email)->send(new boasVindas(Auth::user()));
                }else{
                    Auth::user()->update(['phone' => $request->phone, 'cpf' => $request->cpf]);
                }
                    $user = User::find($usuario = Auth::user()->id);
                    $user->update(['birth_date' => app('App\Helpers\DateHelper')->BRtoEN($request->birth_date)]);
                    
                    //endereco
                    $address = app('App\Http\Controllers\AddressController')->store($request);
                    //assinatura
                    $subscription = new UserSubscription;
                    $subscription->user_id = Auth::user()->id;
                    $subscription->fill($request->all());
                    $subscription->code = 0;
                    $subscription->save();
                    
                    $coupon = Coupon::find($request->input('discount_coupon'));
                    if($coupon !== NULL){
                        $coupon->user()->attach(Auth::user()->id);
                        $userCoupon = $coupon->user()->orderBy('id', 'DESC')->first()->pivot;
                        $order->user_coupon_id = $userCoupon->id;
                    }

                    //pedido
                    $price = [
                        'price_full' => \session('store')['price'],
                        'price_discount' => \session('store')['price_discount'],
                    ];
                    
                    for ($i = 0; $i <= 3; $i++){
                        $data[$i] = strtotime(app('App\Helpers\DateHelper')->getDay($request->weekday));
                        if($i == 0){
                            $data[$i] = date('Y-m-d', strtotime('+'.($i+1).' week', $data[$i]));
                        }else{
                            $data[$i] = date('Y-m-d', strtotime('+'.$i.' months', $data[$i]));
                        }
                        $order[$i] = Self::createOrder($request, $subscription, Auth::user()->id, $price, Auth::user()->addresses()->first());
                        //app('App\Http\Controllers\DeliveryController')->store($order[$i], $data[$i]);

                    }
                    
                    //cartao
                    app('App\Http\Controllers\CardController')->store($request);
                    //pagseguro
                    $pagseguro = PagseguroController::checkoutTransparent($request, $subscription);
                    if(array_key_exists('error', $pagseguro)){
                        DB::rollBack();
                        return redirect()->back()->withInput()->withErrors($pagseguro['error']);
                    }
                    app('App\Http\Controllers\PaymentController')->store($subscription, '1', $order[0]);
                    $subscription->code = $pagseguro['code'];
                    $subscription->update(['code' => $pagseguro['code']]);

                    DB::commit();
                    return redirect('finalizar-assinatura');
                } catch (\Exception $e) {
                    DB::rollBack();
                    return response()->json($e->getMessage(), 400);
                    return redirect()->back()->withInput();
                }
            }
            
            public function createOrder($request, $subscription, $user_id, $price, $address){
                $order = new Order;
                $order->address_id = $address->id;
                $order->user_id = $user_id;
                $order->user_subscription_id = $subscription->id;
                $order->fill($request->toArray());
                $order->price = $price['price_full'];
                $order->price_discount = $price['price_discount'];
                $order->save();
                return $order;
            }
            
            public function alterar(Request $request){
                $kit = $request->kit;
                DB::beginTransaction();
                try{
                    $subscription = Auth::user()->assinatura()->first();
                    $card = Auth::user()->cards()->first();
                    $endereco = Auth::user()->addresses()->first();
                    $data = array_merge(Auth::user()->toArray(), $subscription->toArray(), $card->toArray(), $endereco->toArray() );
                    $request = new Request($data);
                    $request->birth_date = app('App\Helpers\DateHelper')->ENtoBR($request->birth_date);
                    $request->senderHash = PagSeguro::startSession();
                    //select kit
                    \session(['kit' => Kit::find($kit)]);
                    //assinatura
                    $subscription->kit_id = $kit;
                    $subscription->update();
                    //pagseguro
                    $pagseguro = PagseguroController::checkoutTransparent($request, $subscription);
                    if(array_key_exists('error', $pagseguro)){
                        DB::rollBack();
                        return redirect()->back()->withInput()->withErrors($pagseguro['error']);
                    }
                    //cancelar assinatura
                    PagseguroController::cancelPlan($subscription->code);
                    $subscription->code = $pagseguro['code'];
                    $subscription->update();

                    DB::commit();
                    return redirect('meu-box');
                } catch (\Exception $e){
                    return response()->json($e->getMessage(), 400);
                }
            }
            
            public function checklogintoorder(Request $request)
            {
                if($request->isMethod('post'))
                {
                    \session(['periodoEntrega' => $request->input('inputPeriodo'), 'diaEntrega' => $request->input('inputDia')]);
                    
                    if(Auth::check())
                    {
                        // return view('checkout-logado-assinatura');
                        return redirect('checar-dados');
                    } else
                    {
                        \session(['login_pagina' => 'assinatura']);
                        return redirect('login-assinatura');
                    }
                }
            }

            public function checkoutlogadoassinatura(Request $request){
                Auth::attempt(['email' => $request->email, 'password' => $request->password]);

                if(Auth::check()){
                    return redirect('checar-dados');
                }else{
                    return redirect()->back()->withInput();
                }
            }
            
            public function startCheckout(){
                \session(['store' => [
                    'price'     => \session('kit')->price,
                    'price_discount'    => 0,
                    'price_freight'     => 0,
                    ]]);
            }
                
            public function completeOrder(Request $request){
                foreach($request->id as $key => $value){
                    $order = Order::find($value);

                    if(isset($request->status[$key]) && $request->status[$key] == 'on'){
                        $order->status = 1;
                    }else{
                        $order->status = 0;
                    }
                    $order->update();
                }
                
                //app('App\Http\Controllers\DeliveryController')->store($order);
                if($order->subscription()->first()->orders()->where('status', 0)->count() == 0){
                    $price = ['price_full' => $order->price, 'price_discount' => $order->price_discount];
                    for($i = 0; $i <= 3; $i++){
                        Self::createOrder($order, $order->subscription()->first(), $order->user_id, $price);
                    }
                    $order->save();
                    if($order->subscription()->first()->orders()->where('status', 0)->count() == 0){
                        $price = ['price_full' => $order->price, 'price_discount' => $order->price_discount];
                        for($i = 0; $i <= 3; $i++){
                            app('App\Http\Controllers\DeliveryController')->store($order);
                            Self::createOrder($order, $order->subscription()->first(), $order->user_id, $price);
                        }
                    };
                }
                return redirect()->back()->with('msg', 'Pedido entregue');
                
            }

            public function get($id){
                return Order::find($id);
            }

            public function show(){
                return Order::all();
            }

            public function cancel($id){
                $subscription = UserSubscription::find($id);
                $pagseguro = PagseguroController::cancelPlan($subscription->code);
                $subscription->delete();
                
                return redirect()->back();
                
            }
        }
            
            