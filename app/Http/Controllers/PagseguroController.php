<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use PagSeguro;
use PagSeguroRecorrente;
use Auth;
use App\UserSubscription;
use App\Order;

use Illuminate\Http\Request;

class PagseguroController extends Controller
{
    public static function checkoutTransparent($request, $subscription){
        try{
            $order = PagSeguroRecorrente::setPlan(\session('kit')['code'])
            ->setReference($subscription->id) // OPCIONAL
            ->setSenderInfo([
                'senderName' => Auth::user()->name,
                'senderPhone' => $request->phone, //Qualquer formato, desde que tenha o DDD
                'senderEmail' => Auth::check() ? Auth::user()->email : $request->email,
                // 'senderIp' => '123.123.123.123', //OPCIONAL
                'senderHash' => $request->senderHash,
                'senderCPF' => $request->cpf //Ou senderCNPJ se for Pessoa Júridica
            ])
            ->setCreditCardHolder([
                'creditCardHolderName' => $request->card_name, //OPCIONAL, se não passar ele usa o que for passado no senderName
                'creditCardHolderBirthDate' => $request->birth_date, //Deve estar nesse formato,
                // 'creditCardHolderPhone' => '12 12345678', //OPCIONAL, se não passar ele usa o que for passado no senderPhone
                // 'creditCardHolderCPF' => '12345678900' //OPCIONAL, se não passar ele usa o que for passado no senderCPF, se for Jurídica tem que passar
            ])
            ->setSenderAddress([
                'senderAddressStreet' => $request->street,
                'senderAddressNumber' => $request->number,
                'senderAddressComplement' => $request->complement, // OPCIONAL
                'senderAddressDistrict' => $request->district,
                'senderAddressPostalCode' => $request->cep,
                'senderAddressCity' => $request->city,
                'senderAddressState' => $request->state
            ])
            // ->setBillingAddress([ // A chamada deste método é opcional, se não chamar serão usados os dados do setSenderAddress
            //     'billingAddressStreet'      => $request->street,
            //     'billingAddressNumber'      => $request->number,
            //     'billingAddressComplement'  => $request->complement, // OPCIONAL
            //     'billingAddressDistrict'    => $request->district,
            //     'billingAddressPostalCode'  => $request->cep,
            //     'billingAddressCity'        => $request->city,
            //     'billingAddressState'       => $request->state
	        // ])
            ->sendPreApproval([
                'paymentMethod' => 'creditCard',
                'creditCardToken' => $request->card_token,
            ]);
            return array('code' => $order);
        }catch(\Artistas\PagSeguro\PagSeguroException $e){
            $e->getCode(); //codigo do erro
            return array('error' => $e->getMessage()); //mensagem do erro
        }
    }

    public static function createPlan($request){
        $plano = PagSeguroRecorrente::sendPreApprovalRequest([
            'preApprovalName' => $request->name, //Nome do plano
            'preApprovalCharge' => 'AUTO', //Tipo de Cobrança
            'preApprovalPeriod' => 'MONTHLY', //Periodicidade do plano
            // 'preApprovalCancelURL' => 'http://sitedocliente.com', //URL de cancelamento OPCIONAL
            'preApprovalAmountPerPayment' => $request->price, //Valor exato da cobrança
            // 'preApprovalMembershipFee' => '150.00', //Taxa de adesão OPCIONAL
            // 'preApprovalTrialPeriodDuration' => '28', //Tempo de teste OPCIONAL
            // 'preApprovalExpirationValue' => '10', //Número de cobranças que serão realizadas OPCIONAL
            'preApprovalExpirationUnit' => 'MONTHS', //Período em que as cobranças serão realizadas OPCIONAL
            // 'maxUses' => '500', //Quantidade máxima de uso do plano OPCIONAL
        ]);

        return array('plano' => $plano);
    }

    public static function cancelPlan($code){
        try{
            $status = PagSeguroRecorrente::cancelPreApproval($code);
            return $status;
        }catch(Artistas\PagSeguro\PagSeguroException $e){
            return array('error' => $e->getMessage());
        }
    }

    public function notificacao(Request $request){
            $notificacao = PagSeguroRecorrente::notification($request->notificationCode, $request->notificationType);
            $subscription = UserSubscription::find($notificacao->reference);
            if($request->notificationType == 'transaction'){
                if($subscription->payments()->orderBy('id', 'desc')->first()->status == '1'){
                    $order_reference = $subscription->payments()->orderBy('id', 'desc')->first()->order_id;
                    $order = Order::find($order_reference);
                    $subscription->payments()->orderBy('id', 'desc')->first()->update(['status' => $notificacao->status]);
                    app('App\Helpers\orderHelper')->calcData($order);
                }else{
                    $order_reference = $subscription->payments()->orderBy('id', 'desc')->first()->order_id;
                    $order = Order::find($order_reference+1);
                    app('App\Http\Controllers\PaymentController')->store($subscription, $notificacao->status, $order);
                    app('App\Helpers\orderHelper')->calcData($order);
                }
            }
    }

    public function alterCard($request){
        try {
            $response = PagSeguroRecorrente::setPreApprovalCode($request[1]->assinatura->code)
                ->setType('CREDITCARD')
                ->setSenderInfo([
                    'senderName' => $request[1]->name,
                    'senderPhone' => $request[1]->phone, //Qualquer formato, desde que tenha o DDD
                    'senderEmail' => $request[1]->email,
                    'senderHash' => $request[0]['senderHash'],
                    'senderCPF' => Auth::user()->cpf //Ou senderCNPJ se for Pessoa Júridica
                ])
                ->setSenderAddress([
                    'senderAddressStreet' => $request[1]->addresses->first()->street,
                    'senderAddressNumber' => $request[1]->addresses->first()->number,
                    // 'senderAddressComplement' => 'Complemento', // OPCIONAL
                    'senderAddressDistrict' => $request[1]->addresses->first()->district,
                    'senderAddressPostalCode' => $request[1]->addresses->first()->cep,
                    'senderAddressCity' => $request[1]->addresses->first()->city,
                    'senderAddressState' => $request[1]->addresses->first()->state
                ])
                ->setCreditCardHolder([
                    'creditCardHolderName' => $request[0]['card_name'], //OPCIONAL, se não passar ele usa o que for passado no senderName
                    'creditCardHolderBirthDate' => app('App\Helpers\DateHelper')->ENtoBR($request[1]->birth_date), //Deve estar nesse formato,
                    //'creditCardHolderPhone' => '12 12345678', //OPCIONAL, se não passar ele usa o que for passado no senderPhone
                    //'creditCardHolderCPF' => '12345678900' //OPCIONAL, se não passar ele usa o que for passado no senderCPF, se for Jurídica tem que passar
                 ])
                ->sendPreApprovalPaymentMethod([
                    'creditCardToken' => $request[0]['card_token']
                ]);
        
                return array('response' => $response);
        
        } catch (\Artistas\PagSeguro\PagSeguroException $e) {
            return $error = [  
                'code' => $e->getCode(),
                'error' => $e->getMessage()
            ];
        }

    }
}
