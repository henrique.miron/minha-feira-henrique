<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Order;
use App\UserCard;
use App\Helpers\PedidoHelper;
use App\User;
use App\UserSubscription;
use Carbon\Carbon;


class PainelController extends Controller
{
    public function meubox()
        {
            $data = null;
            $subscription = UserSubscription::where('user_id', Auth::user()->id);
            if($subscription->count() !== 0){
                $subscription = $subscription->orderBy('id', 'DESC')->first();
                $kit = $subscription->kit()->first();
                $data = array('subscription' => $subscription, 'kit' => $kit);                             
            }
            return view('meu-box', ['data' => $data]);
        }

        public function periodobox()
        {
            $subscription = Auth::user()->assinatura;
            if($subscription){
                $order = $subscription->orders()->where('status', '0')->first();
                $order->weekdayName = PedidoHelper::formatarDiaEntrega($order->weekday);
                $order->period  = PedidoHelper::formatarPeriodoEntrega($order->period);
                if($order->delivery){
                    $order->ramain = app('App\Helpers\DateHelper')->ramain($order->delivery->date);
                }else{
                    $order->ramain = '0';
                }
            }else{
                return redirect('meu-box');
            }
            return view('periodo-box', ['order' => $order]);
        }

        public function cartaobox()
        {
            $data = null;
            $cartao = UserCard::where('user_id', Auth::user()->id)->get();
            if($cartao){
                $data = array('cartoes' => $cartao);
            }
            return view('cartao-cadastrado', $data);
        }

        public function enderecobox(){
            $enderecos = app('App\Http\Controllers\AddressController')->show();
            return view('endereco-cadastrado', ['enderecos' => $enderecos]);
        }

        public function cobrancabox(){
            $pagamentos = Auth::user()->payments()->get();
            return view('historico-cobranca', ['pagamentos' => $pagamentos]);
            
        }

        public function entregabox()
        {
            $orders = Auth::user()->orders()->where('status', '0')->get();
            return view('historico-entrega', ['orders' => $orders]);
        }

        //admin

        public function usuarios(){
            $data = ['clientes' => app('App\Http\Controllers\UserController')->show('cliente')];
            return view('admin-usuarios', $data);
        }

        public function pedido(Request $request, $id){
            // return $request;
            $user = app('App\Http\Controllers\UserController')->get($id);
            if($request->has('filtro')){
                $data = ['orders' => $user->orders()->where('status', $request->filtro)->get(), 'filtro' => $request->filtro];
            }else{
                // return $request;
                $data = ['orders' => $user->orders()->where('status', '0')->get(), 'filtro' => 0];
            }
            $data['id'] = $id;
            return view('admin-pedidos', $data);
        }

        public function pedidoDetalhe($id){
            $data = ['pedido' => app('App\Http\Controllers\OrderController')->get($id)];

            return view('admin-pedido-detalhe', $data);
        }

        public function adminconfigbox()
        {
            $data = ['kits' => app('App\Http\Controllers\KitController')->show()];
            return view('admin-configbox', $data);
        }

        
        public function adminrelatorios()
        {
            $orders = app('App\Http\Controllers\OrderController')->show();
            $deliverys = app('App\Http\Controllers\DeliveryController')->show();
            $orders = $orders->where('status', 0);
            $data['deliverys_t'] = [];
            $data['deliverys_l'] = [];
            foreach($orders as $order){
                if($order->delivery->date == Carbon::today()->toDateString()){
                    $data['deliverys_t'][] = $order->delivery;
                }elseif($order->delivery->date < Carbon::today()->toDateString()){
                    $data['deliverys_l'][] = $order->delivery;
                }
            }
            // $data = [
            //     'deliverys_t' => $deliverys->where('date', Carbon::today()->toDateString()),
            //     'deliverys_l' => $deliverys->where('date','<', Carbon::today()->toDateString())
            // ];
            // return $data;
            // return $data['deliverys_t'][0]->order->address;
            return view('admin-relatorios', $data);
        }

}
