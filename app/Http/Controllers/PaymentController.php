<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;

class PaymentController extends Controller
{
    //
    public function store($request, $status, $order){
        $payment = new Payment;
        $payment->fill([
            'status'            => $status, 
            'subscription_id'   => $request->id, 
            'user_id'           => $request->user_id, 
            'order_id'          => $order->id
            ])->save();
    }
}
