<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Services\SocialFacebookAccountService;
use Illuminate\Support\Facades\Redirect;

class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(SocialFacebookAccountService $service)
    {
        try
        {
            $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
            auth()->login($user);

            if(\session('login_pagina') == 'assinatura'){
                return redirect()->to('/checar-dados');
            }
            return redirect()->to('/painel-home');                
        }catch(\Exception $e){
            return $e;
            return redirect()->to('/login');
        }

    }
}
