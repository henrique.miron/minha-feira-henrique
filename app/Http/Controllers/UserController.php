<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    //
    public function show($type){
        return User::whereHas('roles', function ($query) use ($type) {
            $query->where('name', $type);
        })->paginate(5);
    }

    public function get($id){
        return User::find($id);
    }
}
