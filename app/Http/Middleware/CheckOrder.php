<?php

namespace App\Http\Middleware;
use App\Order;
use Auth;

use Closure;

class CheckOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check()){
            return $next($request);
        }
        if(Order::where('user_id', Auth::user()->id)->count() == 0){
            return $next($request);
        }
        return redirect('painel-home');
    }
}
