<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Role;
use Auth;
use Redirect;

class RoleAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $roleAdmin = Role::where('name', 'admin')->first()->name;
        if(User::find(Auth::user()->id)->roles()->first()->name == $roleAdmin){
            return $next($request);
        }else{
            return redirect('/');
        };
        
        // User::authorizeRoles('admin') ? redirect($next) : redirect::withFlashData('status', 'Sem Acesso');
        
    }
}
