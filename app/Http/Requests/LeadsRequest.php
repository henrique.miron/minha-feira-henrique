<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\FreightCostController;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class LeadsRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }
    
    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'cep'=>'required|numeric',
            'email'=>'required|email|unique:leads,email'
        ];
    }
    
    public function messages()
    {
        return [
            'cep.required'=> 'CEP obrigatório',
            'cep.numeric'=> 'Apenas númericos são aceitos',
            'email.required'=> 'Email obrigatório',
            'email.numeric'=> 'Insira um email válido',
            'email.unique'   => 'Este email já está cadastrado no sistema',
        ];
    }
    
    /**
    * Configure the validator instance.
    *
    * @param  \Illuminate\Validation\Validator  $validator
    * @return void
    */
    
    public function withValidator($validator)
    {
        if(!$validator->fails()){
            $validator->after(function ($validator) {
                $freight = new FreightCostController();
                $result = $freight->cep_available($this->input('cep'));
                if(!isset($result[0]))
                {
                    $validator->errors()->add('cep', 'Não atendemos na sua região :(');
                }
            });
        }
    }
}
