<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use Illuminate\Validation\Rule;

class compraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'sometimes|required',
            'email'             => 'sometimes|required|unique:users,email',
            'cpf'               => 'sometimes|required|unique:users,cpf,'.$this->get('user_id'),
            'password'          => 'sometimes|required',
            'shipping_address'  => 'sometimes|required',
            'shipping_cep'      => 'sometimes|required',
            'shipping_number'   => 'sometimes|required',
            'bairro'            => 'sometimes|required',
            'shipping_city'     => 'sometimes|required',
            'card_number'       => ['sometimes','required', Rule::unique('user_cards')->ignore($this->get('user_id'), 'user_id'), new CardNumber],
            'name'              => 'sometimes|required',
            'valid_thru'        => 'sometimes|required',
            'card_security'     => ['sometimes','required',  new CardCvc($this->get('card_number'))],
        ];
    }

    public function messages()
    {
        return [
            'email.unique'   => 'Este email já está cadastrado no sistema',
            'card_number.unique' => 'Este cartão já está cadastrado no sistema',
        ];
    }
}
