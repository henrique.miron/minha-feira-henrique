<?php
/**
 * Created by PhpStorm.
 * User: andreza
 * Date: 05/04/18
 * Time: 22:22
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Kit extends Model
{
    // use SoftDeletes;

    protected $table = 'kits';
    public $fillable = ['name', 'price', 'code'];

    public function attrs(){
        return $this->hasMany(Attribute::class);
    }

    public function subscriptions(){
        return $this->hasMany(UserSubscription::class());
    }

}