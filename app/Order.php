<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    // use SoftDeletes;

    protected $table = 'order';

    protected $guarded = ['status'];

    public $fillable = ['user_id', 'user_subscription_id','price', 'price_discount', 'price_freight', 'user_coupon_id', 'period', 'weekday', 'address_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function kit()
    {
        return $this->belongsTo(Kit::class, 'user_subscription_id');
    }

    public function subscription(){
        return $this->belongsTo(UserSubscription::class, 'user_subscription_id');
    }

    public function address(){
        return $this->belongsTo(Address::class, 'address_id');
    }

    public function delivery(){
        return $this->hasOne(Delivery::class);
    }

    public function payment(){
        return $this->hasOne(Payment::class);
    }



    // public static function rules()
    // {
    //     return [
    //         'user_id' => 'required|exists:users,id',
    //         'price' => 'required',
    //         'kit_id' => 'required|exists:kits,id',
    //         'price_discount' => 'required',
    //         'price_freight' => 'required',
    //         'weekday' => 'required|numeric|min:1|max:7',
    //         'period' => 'required|numeric|min:1|max:3',
    //         'card_number' => 'required',
    //         'flag' => 'required',
    //         'valid_thru' => 'required',
    //         'name' => 'required'
    //     ];
    // }

    // public static function boot()
    // {
    //     parent::boot();

    //     self::saved(function($model){

    //         $userSettings = UserSettings::findByUser($model->user_id);

    //         if(is_null($userSettings)) {
    //             $userSettings = new UserSettings();
    //             $userSettings->user_id = $model->user_id;
    //         }

    //         $userSettings->weekday = $model->weekday;
    //         $userSettings->period = $model->period;
    //         $userSettings->save();
    //     });
    // }
}