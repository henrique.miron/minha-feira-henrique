<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $table = "user_payments";
    protected $fillable = ['subscription_id', 'user_id', 'order_id', 'status'];

    public function subscription(){
        return $this->belongsTo(UserSubscription::class, 'subscription_id');
    }

    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}
