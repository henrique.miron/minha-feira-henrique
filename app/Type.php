<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    //
    protected $table = 'discount_coupon_type';
    public $fillable = ['name'];
}
