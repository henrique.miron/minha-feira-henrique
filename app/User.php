<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'name', 'email', 'password', 'birth_date', 'phone', 'cpf'
    ];

    protected $with = ['roles'];

    public static function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
        ];
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new Notifications\MailResetPasswordNotification($token));
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

            /**
        * @param string|array $roles
        */
        public static function authorizeRoles($roles)
        {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) || 
                    abort(401, 'Sem autorização.');
        }
            return $this->hasRole($roles) || 
                abort(401, 'Sem autorização.');
        }
        /**
        * Check multiple roles
        * @param array $roles
        */
        public function hasAnyRole($roles)
        {
            return null !== $this->roles()->whereIn('name', $roles)->first();
        }
        /**
        * Check one role
        * @param string $role
        */
        public function hasRole($role)
        {
            return null !== $this->roles()->where('name', $role)->first();
        }

    public function coupon(){
        return $this->belongsToMany('App\Coupon', 'user_x_coupon');
    }
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
    public function cards(){
        return $this->hasMany(UserCard::class);
    }
    public function assinatura(){
        return $this->hasOne(UserSubscription::class);
    }
    public function orders(){
        return $this->hasMany(Order::class);
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }
    public function addresses(){
        return $this->hasMany(Address::class);
    }
}
