<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    protected $table = 'user_cards';

    protected $guarded = [];

    public $fillable = ['user_id', 'flag', 'card_number', 'valid_thru', 'cpf', 'card_name', 'card_token'];
    


}