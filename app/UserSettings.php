<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class UserSettings extends Model
{

    protected $table = 'user_settings';

    public static function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'weekday' => 'required|numeric|min:1|max:7',
            'period' => 'required|numeric|min:1|max:3'
        ];
    }

    public static function findByUser($userId)
    {
        return UserSettings::where('user_id', $userId)->firstOrFail();
    }
}