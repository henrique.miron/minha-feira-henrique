<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $table = 'user_subscriptions';

    protected $guarded = [];

    public $fillable = ['user_id', 'code', 'kit_id', 'payment_id', 'mensagem'];

    public function kit()
    {
        return $this->belongsTo(Kit::class, 'kit_id');
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function payments(){
        return $this->hasMany(Payment::class, 'subscription_id');
    }
}