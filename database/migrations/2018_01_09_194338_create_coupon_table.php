<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('discount_coupon')) {
            Schema::create('discount_coupon', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code',200);
                $table->dateTime('date_start')->nullable();
                $table->dateTime('date_end')->nullable();
                $table->integer('valid')->default(0);
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->integer('type_id')->unsigned();
                $table->foreign('type_id')->references('id')->on('discount_coupon_type');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_coupon');
    }
}
