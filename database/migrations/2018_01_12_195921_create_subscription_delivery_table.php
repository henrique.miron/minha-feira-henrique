<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionDeliveryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_subscription_delivery')) {
            Schema::create('user_subscription_delivery', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('subscription_id')->unsigned();
                $table->foreign('subscription_id')->references('id')->on('user_subscriptions')->onDelete('cascade');
                $table->integer('status')->default(0);
                $table->date('date_delivered')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_subscription_delivery');
    }
}
