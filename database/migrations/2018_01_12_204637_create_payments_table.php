<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('payments')) {
            Schema::create('payments', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on('users');
                $table->integer('kit_id')->unsigned();
                $table->foreign('kit_id')->references('id')->on('kits');
                $table->decimal('price',8,2)->default(0);
                $table->decimal('price_discount',8,2)->default(0);
                $table->decimal('price_freight',8,2)->default(0);
                $table->integer('status')->default(0);
                $table->integer('weekday')->nullable();
                $table->integer('period')->default(0);
                $table->integer('user_coupon_id')->unsigned();
                $table->integer('address_id')->unsigned();
                $table->foreign('user_coupon_id')->references('id')->on('user_x_coupon');
                $table->foreign('address_id')->references('id')->on('address');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
