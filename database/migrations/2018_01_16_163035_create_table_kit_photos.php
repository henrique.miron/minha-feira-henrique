<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKitPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('kit_photos')) {
            Schema::create('kit_photos', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('kit_id')->unsigned();
                $table->foreign('kit_id')->references('id')->on('kits');
                $table->string('url','1000');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kit_photos');
    }
}
