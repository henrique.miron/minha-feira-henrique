<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreightCostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('freight_cost')) {
            Schema::create('freight_cost', function (Blueprint $table) {
                $table->increments('id');
                $table->string('cep_origin',20);
                $table->string('cep_destination',20);
                $table->decimal('price_freight',8,2)->default(0);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freight_cost');
    }
}
