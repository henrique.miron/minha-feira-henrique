<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->integer('user_coupon_id')->nullable()->change();            
            $table->integer('user_coupon_id')->unsigned()->change();
            // if(Schema::hasColumn('order','status'))
            // {
            //     $table->dropColumn('status');
            // }
            // $table->dropColumn('kit_id');
            $table->dropForeign('payments_kit_id_foreign');
            $table->dropColumn('kit_id');
            $table->integer('user_subscription_id')->unsigned()->after('user_id');
            $table->foreign('user_subscription_id')->references('id')->on('user_subscriptions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
