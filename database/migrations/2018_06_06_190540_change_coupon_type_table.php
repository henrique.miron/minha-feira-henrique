<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCouponTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discount_coupon_type', function (Blueprint $table) {
            //
            $table->decimal('percentage', 8,2)->default(0);
            // $table->renameColumn('name', 'percentage')->change();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discount_coupon_type', function (Blueprint $table) {
            //
        });
    }
}
