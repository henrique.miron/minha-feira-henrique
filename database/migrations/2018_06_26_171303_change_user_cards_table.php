<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_cards', function (Blueprint $table) {
            //
            $table->string('card_number')->change();
            $table->renameColumn('name', 'card_name')->change();
            $table->renameColumn('token', 'card_token')->change();
            // $table->string('valid_thru')->change();
            $table->string('cpf')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_card', function (Blueprint $table) {
            //
        });
    }
}
