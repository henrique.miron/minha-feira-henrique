<?php

use Illuminate\Database\Seeder;
use App\Freight_Cost;

class CepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $cep = new Freight_Cost;
        $cep->fill(['cep_origin' => '000000000', 'cep_destination' => '999999999', 'address' => 0]);
        $cep->save();
    }
}
