<?php

use Illuminate\Database\Seeder;
use App\Kit;

class KitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //P
        $kitP = new Kit;
        $kitP->name = 'P';
        $kitP->price = 90;
        $kitP->code = 'D9644DD0CDCD05144485BF8DB1CC32DE';
        $kitP->save();
        // //M
        $kitM = new Kit;
        $kitM->name = 'M';
        $kitM->price = 120;
        $kitM->code = 'DEBEC9C94C4C85C4445A3FA046CAC2CA';
        $kitM->save();
        // //G
        $kitG = new Kit;
        $kitG->name = 'G';
        $kitG->price = 150;
        $kitG->code = '9AB5C402A4A4EB3DD4053F9A865BA81D';
        $kitG->save();

    }
}
