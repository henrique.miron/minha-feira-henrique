<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role_cliente = Role::where('name', 'cliente')->first();
        $role_admin  = Role::where('name', 'admin')->first();

        $role_employee = Role::where('name', 'cliente')->first();
        $role_manager  = Role::where('name', 'admin')->first();
        $cliente = new User();
        $cliente->name = 'cliente Name';
        $cliente->email = 'cliente@example.com';
        $cliente->password = bcrypt('secret');
        $cliente->save();
        $cliente->roles()->attach($role_cliente);
        $admin = new User();
        $admin->name = 'admin Name';
        $admin->email = 'admin@example.com';
        $admin->password = bcrypt('secret');
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
