var elixir = require('laravel-elixir');

elixir(function(mix) {
   mix.sass(['./node_modules/card/dist/card.css','app.scss']);
   mix.scripts(
       [
            './node_modules/jquery/dist/jquery.min.js',
            './node_modules/angular/angular.min.js',
            './node_modules/jquery-mask-plugin/dist/jquery.mask.min.js',
            './node_modules/jquery-validation/dist/jquery.validate.js',
            './node_modules/jquery-validation/dist/additional-methods.js',
            './node_modules/jquery-validation/dist/localization/messages_pt_BR.js',
            './node_modules/card/dist/card.js',
            'resources/assets/js/validacao.js',
            'resources/assets/js/app.js',
            'resources/assets/js/checkout.js',
            'resources/assets/js/data.js',
            './node_modules/bootstrap/dist/js/bootstrap.min.js',

       ],
       './public/js/vendor.js'
   );
   mix.webpack(
       [
       './node_modules/popper.js/dist/popper.js',
       './node_modules/bootstrap/dist/js/bootstrap.min.js',
       ],
       './public/js/app.js'
   );

});

