$(document).ready(function(){
    
    $('.lista-dias .nav-item .nav-link').on('click', function(e) {
      $('input[name=weekday]').val(this.id);
      $('form#alterar').submit()
    })
    
    $('.lista-periodos .nav-item .nav-link').on('click', function(e) {
      $('input[name=period]').val(this.id);
      $('form#alterar').submit()
    });
    
    $('button.kit').on('click', function(e){
      $('input[name="kit"]').val(e.target.id);
      $('form#kit-alterar').submit();
    })
    
  });

  $('form#order').ready(function(){
    //pagseguro    
    $('button#finalizar').on('click', function(e){
      cardToken();
    })
})
  
  
  //credit card token
  function cardToken(){
    let date = $('input[name="valid_thru"]').val().replace(/\s/g, '').split('/');
    PagSeguroDirectPayment.createCardToken({
      cardNumber: $('input[name="card_number"]').val().replace(/\s/g, ''),
      brand: $('input[name="flag"]').val(),
      cvv: $('input[name="card_security"]').val(),
      expirationMonth: date[0],
      expirationYear: date[1],
      success: function(response){
        $('#cardToken').attr('value', response.card['token']);
        senderForm();                
      },
      error: function(response){
        console.log(response)
      },
    });
  }
  
  function senderForm(){
    $('input[name="valid_thru"]').val($('input[name="valid_thru"]').val().replace(/\s/g, ''));
    $('input[name="card_number"').val($('input[name="card_number"').val().replace(/\s/g, ''));
    $('#senderHash').val(PagSeguroDirectPayment.getSenderHash());
    if($('form#order').valid() && $('#cardToken').val() !== 'null'){
      $('form#order').submit();
    }
  }