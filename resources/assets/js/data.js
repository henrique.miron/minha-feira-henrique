function claculateNextDate(dia) {
    var date = new Date();
    var nextDay = dia;
    var day = date.getDay();
    var noOfDaysToAdd = (8 - day) + parseInt(nextDay);
    date.setDate(date.getDate() + noOfDaysToAdd);

    return date;
  }

  function daysRemain(secondDate){
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date();
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

    return diffDays;
  }