//DataBr
jQuery.validator.addMethod("dateBR", function (value, element) {
    //contando chars    
    if (value.length != 10) return (this.optional(element) || false);
    // verificando data
    var data = value;
    var dia = data.substr(0, 2);
    var barra1 = data.substr(2, 1);
    var mes = data.substr(3, 2);
    var barra2 = data.substr(5, 1);
    var ano = data.substr(6, 4);
    if (data.length != 10 || barra1 != "/" || barra2 != "/" || isNaN(dia) || isNaN(mes) || isNaN(ano) || dia > 31 || mes > 12) return (this.optional(element) || false);
    if ((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia == 31) return (this.optional(element) || false);
    if (mes == 2 && (dia > 29 || (dia == 29 && ano % 4 != 0))) return (this.optional(element) || false);
    if (ano < 1900) return (this.optional(element) || false);
    return (this.optional(element) || true);
}, "Informe uma data válida");  // Mensagem padrão 

//Celular
jQuery.validator.addMethod('celular', function (value, element) {
    value = value.replace("(","");
    value = value.replace(")", "");
    value = value.replace("-", "");
    value = value.replace(" ", "").trim();
    if (value == '0000000000') {
        return (this.optional(element) || false);
    } else if (value == '00000000000') {
        return (this.optional(element) || false);
    } 
    if (["00", "01", "02", "03", , "04", , "05", , "06", , "07", , "08", "09", "10"].indexOf(value.substring(0, 2)) != -1) {
        return (this.optional(element) || false);
    }
    if (value.length < 10 || value.length > 11) {
        return (this.optional(element) || false);
    }
    if (["6", "7", "8", "9"].indexOf(value.substring(2, 3)) == -1) {
        return (this.optional(element) || false);
    }
    return (this.optional(element) || true);
}, 'Informe um celular válido'); 

 //Telefone fixo
 jQuery.validator.addMethod('telefone', function (value, element) {
        value = value.replace("(", "");
        value = value.replace(")", "");
        value = value.replace("-", "");
        value = value.replace(" ", "").trim();
        if (value == '0000000000') {
            return (this.optional(element) || false);
        } else if (value == '00000000000') {
            return (this.optional(element) || false);
        }
        if (["00", "01", "02", "03", , "04", , "05", , "06", , "07", , "08", "09", "10"].indexOf(value.substring(0, 2)) != -1) {
            return (this.optional(element) || false);
        }
        if (value.length < 10 || value.length > 11) {
            return (this.optional(element) || false);
        }
        if (["1", "2", "3", "4","5"].indexOf(value.substring(2, 3)) == -1) {
            return (this.optional(element) || false);
        }
        return (this.optional(element) || true);
    }, 'Informe um telefone válido'); 

$(document).ready(function(){
    //mascaras              
    $('.cep').mask('00000-000',{clearIfNotMatch: false});
    $('.phone').mask('(00) 0000-00009');
    $('.cpf').mask('000.000.000-00');
    $('.data').mask('00/00/0000');

    
    $("form").submit(function() {
        $('.cep, .phone, .cpf').unmask();
    });

   
    
    //validacao de campos
    $('form#order').is(function(e){
        $('form#order').validate({
            lang: 'pt_BR',
            rules:{
                cep: {minlength: 8, postalcodeBR: true},
                email: {email:true},
                card_number: {creditcard: true},
                cpf: {cpfBR: true},
                phone: {celular:true},
                birth_date: {dateBR:true}
            },
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.addClass('is-invalid').removeClass('is-valid');
                error.insertAfter(element);
                element.parent().find('div.success-icon').remove();
                element.after('<div class="input-group-addon error-icon"></div>');
                
            },
            unhighlight: function(element){
                $(element).addClass('is-valid').removeClass('is-invalid');
                $(element).parent().find('div.error-icon').remove();
                $(element).parent().find('div.success-icon').remove();
                $(element).after('<div class="input-group-addon success-icon"></div>');
            }
        })
        
        //validacao cartao de credito
        const card = new Card({
            // a selector or DOM element for the form where users will
            // be entering their informationP
            form: 'form#order', // *required*
            // a selector or DOM element for the container
            // where you want the card to appear
            container: '.card-wrapper', // *required*
            
            formSelectors: {
                numberInput: 'input[name="card_number"]', // optional — default input[name="number"]
                expiryInput: 'input[name="valid_thru"]', // optional — default input[name="expiry"]
                cvcInput: 'input[name="card_security"]', // optional — default input[name="cvc"]
                nameInput: 'input[name="card_name"]' // optional - defaults input[name="name"]
            },
            
            width: 300, // optional — default 350px
            formatting: true, // optional - default true
            
            // Strings for translation - optional
            messages: {
                validDate: 'valid\ndate', // optional - default 'valid\nthru'
                monthYear: 'mm/yyyy', // optional - default 'month/year'
            },
            
            // Default placeholders for rendered fields - optional
            placeholders: {
                number: '•••• •••• •••• ••••',
                name: 'NOME COMPLETO',
                expiry: '••/••',
                cvc: '•••'
            },
            
            masks: {
                cardNumber: '•' // optional - mask card number
            },
            
            // if true, will log helpful messages for setting up Card
            debug: true // optional - default false
        });
        
        //desconto
        $("#cupom_button").on('click', function(){
            $.ajax({
                method: "POST",
                url: "/desconto",
                data: $("form").serialize(),
            })
            .done(function( data ) {
                if(data !== 'false'){
                    $("#cupom").removeClass('is-invalid').addClass('is-valid');
                    $('#cupom').parent().find('label').remove();
                    $(".old-price").removeClass('d-none').html('<span>R$</span>'+data['Valor']+'<sup>,00</sup>');
                    $(".price").html('<span>R$</span>'+data['Valor Final']+'<sup>,00</sup>');
                    $('input[name="price"]').val(data['Valor Final']);
                    $('input[name="discount_coupon"]').val(data['Cupom']['id']);
                    $('input[name="price_discount"]').val(data['Desconto']);
                }else{
                    $('#cupom').parent().find('label').remove();
                    $('#cupom').addClass('is-invalid').after('<label class="error invalid-feedback">Cupom Invalido</label>');
                    $(".old-price").addClass('d-none').html('<span>R$</span>'+data['Valor']+'<sup>,00</sup>');

                }
            });
        });
        
        $('input[name="card_number"]').on('change', function(e){
            $('input[name="flag"]').attr('value', card.cardType);
        })

        
    })
})

$('input[name="street"]').is(function(e){
    pesquisacep($('input[name="cep"]').val());
})

// busca cep

function limpa_formulário_cep() {
    //Limpa valores do formulário de cep.
    $('input[name="street"]').val("");
    $('input[name="state"]').val("");
    $('input[name="city"]').val("");
}

function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
        //Atualiza os campos com os valores.
        $('input[name="street"]').val(conteudo.logradouro);
        $('input[name="district"]').val(conteudo.bairro);
        $('input[name="city"]').val(conteudo.localidade);
        $('input[name="state"]').val(conteudo.uf);
    } //end if.
    else {
        //CEP não Encontrado.
        limpa_formulário_cep();
        alert("CEP não encontrado.");
    }
}

function pesquisacep(valor) {
    
    //Nova variável "cep" somente com dígitos.
    let cep = valor.replace(/\D/g, '');
    
    //Verifica se campo cep possui valor informado.
    if (cep != "") {
        
        //Expressão regular para validar o CEP.
        const validacep = /^[0-9]{8}$/;
        
        //Valida o formato do CEP.
        if(validacep.test(cep)) {
            
            //Preenche os campos com "..." enquanto consulta webservice.
            document.getElementsByName('address').value="...";
            document.getElementsByName('state').value="...";
            document.getElementsByName('city').value="...";
            
            //Cria um elemento javascript.
            const script = document.createElement('script');
            
            //Sincroniza com o callback.
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
            
            //Insere script no documento e carrega o conteúdo.
            document.body.appendChild(script);
            
        } //end if.
        else {
            //cep é inválido.
            limpa_formulário_cep();
            alert("Formato de CEP inválido.");
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulário_cep();
    }
};
