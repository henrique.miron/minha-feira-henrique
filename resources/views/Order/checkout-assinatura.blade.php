@extends('layouts.assinatura')
@section('Top')
@endsection
@section('Content')
<section class="box-assinatura">
  <div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="content white-bg">
      <div class="row">
        <div class="col-md-4 offset-md-1">
          <h4>Informações pessoais</h4>
          <form class="text-left mt-4 mb-5" id="order" method="post" action="{{ url('finalizar-compra') }}">
            {{ csrf_field() }}
            <input type="hidden" id="senderHash" name="senderHash" value="">
            <input type="hidden" id="cardToken" name="card_token" value="">
            <div class="form-row">
              <div class="col-md-12 mb-4">
                <label for="exampleInputNome">Nome completo</label>
                <input type="text" name="name" value="{{ old('name') }}" class="form-control required" placeholder="Ex: João da Silva">
              </div>
              <div class="col-md-12 mb-4">
                <label for="exampleInputNome">Data de Nascimento</label>
                <input type="text" name="birth_date" value="{{ old('birth_date') }}" class="form-control data required" placeholder="Ex: João da Silva">
              </div>
              <div class="col-md-12 mb-4 icon-is-valid">
                <label for="exampleInputEmail">Email</label>
                <input type="email" name="email" class="form-control is-valid required" placeholder="joão.adalberto@hotmail.com" value="{{$email}}">
                <i class="fa fa-check" aria-hidden="true"></i>
                <div class="valid-feedback">Muito bem! Seu e-mail é válido.</div>
              </div>
              <div class="col-md-12 mb-4 icon-is-invalid">
                <label for="exampleInputSenha">Senha</label>
                <input type="password" name="password" class="form-control  {{ $errors->has('password') ? 'is-invalid' : '' }} required" placeholder="Insira sua senha">
                @if($errors->has('password'))
                <i class="fa fa-times" aria-hidden="true"></i>
                <div class="invalid-feedback">Sua senha deve contar no mínimo 6 caracteres!</div>
                @endif
              </div>
              <div class="col-md-12 mb-4">
                <label for="exampleInputConfirmSenha">Confirmar senha</label>
                <input type="password" name="confirm-password" class="form-control required" placeholder="Confirme sua senha">
              </div>
              <div class="col-md-12 mb-4">
                <label for="exampleInputTelefone">Telefone</label>
                <input type="text" name="phone" value="{{ old('phone') }}" class="form-control  phone required" placeholder="Ex: (11) 2091-8881">
              </div>
              <div class="col-md-12 mb-4">
                <label for="exampleInputCPF">CPF</label>
                <input type="text" name="cpf" value="{{ old('cpf') }}" class="form-control cpf required" placeholder="Ex: 419.0349.293.94">
              </div>
            </div>
            <h4>Informações de entrega</h4>
            <div class="form-row d-flex align-items-center">
              <div class="col-md-6 mb-4">
                <label for="exampleInputCep">CEP</label>
                <input type="text" name="cep" value="{{$cep}}" class="form-control required" placeholder="Ex: 03503-000">
              </div>
              <div class="col-md-6 mb-4 text-center">
                <label></label>
                <a href="" class="var">Esqueceu sua senha?</a>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-12 mb-4">
                <label for="exampleInputEnd">Endereço</label>
                <input type="text" name="street" value="{{ old('street') }} " class="form-control required" placeholder="Ex: Rua das flores">
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-6 mb-4">
                <label for="exampleInputNum">Número</label>
                <input type="text" name="number" value="{{ old('number') }}" class="form-control required" placeholder="Ex: 375">
              </div>
              <div class="col-md-6 mb-4">
                <label for="exampleInputComple">Complemento</label>
                <input type="text" class="form-control" name="number_complement" value="{{ old('number_complement') }}" placeholder="Ex: Sala XX, Apartamento XX, etc">
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-6 mb-4">
                <label for="exampleInputBairro">Bairro</label>
                <input type="text" class="form-control required" name="district" value="{{ old('district') }}" placeholder="Ex: Vila Madalena">
              </div>
              <div class="col-md-6 mb-4">
                <label for="exampleInputCidade">Cidade</label>
                <input type="text" name="city" class="form-control required" value="{{ old('city') }}" placeholder="Ex: São Paulo">
              </div>
              <div class="col-md-6 mb-4">
                <label for="exampleInputCidade">UF</label>
                <input type="text" name="state" class="form-control required" value="{{ old('state') }}" placeholder="Ex: São Paulo">
              </div>
            </div>
            <h4>Informações pagamento</h4>
            <div class="card-wrapper"></div>
            <div class="form-row">
              <div class="col-md-12 mb-4 security-card">
                <label for="exampleInputNumbCard">Número do cartão</label>
                <input type="text" name="card_number" class="form-control pr-5 required" placeholder="Ex: 1234 4321 5678 8921">
                <input type="hidden" name="flag" value=""/>
                <img src="{{ asset('img/security-card.png') }}" class="img-fluid" alt="">
                <p class="mb-0 mt-2"><img src="{{ asset('img/security-key.png') }}" class="img-fluid" alt=""> Ambiente 100% seguro.</p>
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-12 mb-4">
                <label for="exampleInputNameCard">Nome no cartão</label>
                <input type="text" name="card_name" class="form-control required" placeholder="Ex: JOÃO DA SILVA S.">
              </div>
            </div>
            <div class="form-row">
              <div class="col-md-6 mb-4">
                <label for="exampleInputValidCard">Data de validade</label>
                <input type="text" name="valid_thru" class="form-control required" placeholder="01/2017">
              </div>
              <div class="col-md-6 mb-4">
                <label for="exampleInputCodCard">Cód. de segurança</label>
                <input type="text" name="card_security" class="form-control required" placeholder="Ex: 1234">
              </div>
            </div>
          </div>
          <div class="col-md-5 offset-md-1 text-center">
            <div class="content-checkout">
              <div class="title">
                <h3 class="var mb-0">Revisão de pedido</h3>
              </div>
              <div class="body">
                <div class="table-responsive">
                  <table class="table text-left mb-0">
                    <tbody>
                      <tr>
                        <th scope="row">Tamanho do box:</th>
                        <td><span>{{$kit->name}}</span></td>
                        <td class="text-right"><a href="#" data-toggle="modal" data-target=".modal-tamanho">Alterar</a></td>
                        <input type="hidden" value="{{$kit->id}}" name="kit_id">
                      </tr>
                      <tr>
                        <th scope="row">Dia:</th>
                        <td><span>{{\App\Helpers\PedidoHelper::formatarDiaEntrega($diaEntrega)}}</span></td>
                        <td class="text-right"><a href="#" data-toggle="modal" data-target=".modal-dia">Alterar</a></td>
                        <input type="hidden" value="{{$diaEntrega}}" name="weekday">
                      </tr>
                      <tr>
                        <th scope="row">Período escolhido:</th>
                        <td><span>{{\App\Helpers\PedidoHelper::formatarPeriodoEntrega($periodoEntrega)}}</span></td>
                        <td class="text-right"><a href="#" data-toggle="modal" data-target=".modal-periodo">Alterar</a></td>
                        <input type="hidden" value="{{$periodoEntrega}}" name="period">
                      </tr>
                      <tr>
                        <th scope="row"><big>Valor Mensal:</big></th>
                        <td></td>
                        <td class="text-right value">
                          <div class="old-price d-none"><span>R$</span>99<sup>,00</sup></div>
                          <input type="hidden" value="{{$kit->price}}" name="price">
                          <input type="hidden" name="price_initial" value="{{$kit->price}}">
                          <input type="hidden" name="price_discount" value="{{$kit->price}}">
                          <input type="hidden" name="price_freight" value="0">
                          <div class="price"><span>R$</span>{{$kit->price}}<sup>,00</sup></div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="form-row d-flex align-items-end">
              <div class="col-md-7">
                <label for="exampleInputCupomDisc">Cupom de desconto</label>
                <input name="cupom" type="text" id="cupom" class="form-control" placeholder="Insira seu cupom">
              </div>
              <div class="col-md-5">
                <button type="button" id="cupom_button" class="button button-secondary">Inserir</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 offset-md-3">
          <button type="button" id="finalizar" class="button button-secondary">Finalizar compra</button>
        </div>
      </form>
    </div>
  </div>
</section>
<form id="alterar" class="d-none" method="POST" action="{{url('kit-alterar')}}">
  {{ csrf_field() }}
  <input type="hidden" name="session" value="true">
  <input type="hidden" name="period">
  <input type="hidden" name="weekday">
</form>
<!-- Modal -->
@include('includes.modal.periodo')
@include('includes.modal.tamanho')
@include('includes.modal.dia')

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      ...
    </div>
  </div>
</div>

@endsection
@section('Footer')
@endsection
@push('scripts')
<!-- Scripts -->
<script type="text/javascript" src="/pagseguro/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
  console.log(PagSeguroDirectPayment.setSessionId('{{ PagSeguro::startSession() }}'));    
    $('.lista-periodos .nav-item .nav-link').on('click', function(e) {
          $('input[name=period]').val(this.id);
          $('form#alterar').submit();
      });

    $('.lista-dias .nav-item .nav-link').on('click', function(e) {
      $('input[name=weekday]').val(this.id);
      $('form#alterar').submit();
    })

    $('button.kit').on('click', function(e){
      $('input[name=kit_id]').val(e.target.id);
    })
  })
</script>
{{--  <script>
  $(document).ready(function(){
    $('#pag_pagamento').addClass('active');
    $("form").submit(function(e) {
      $('input[name="shipping_cep"]').unmask();
    });
    pesquisacep($('input[name="shipping_cep"]').val());
  });
  
  // busca cep
  
  function limpa_formulário_cep() {
    //Limpa valores do formulário de cep.
    $('input[name="shipping_address"]').val("");
    $('input[name="shipping_state"]').val("");
    $('input[name="shipping_city"]').val("");
  }
  
  function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
      //Atualiza os campos com os valores.
      $('input[name="shipping_address"]').val(conteudo.logradouro);
      $('input[name="shipping_state"]').val(conteudo.bairro);
      $('input[name="shipping_city"]').val(conteudo.localidade);
    } //end if.
    else {
      //CEP não Encontrado.
      limpa_formulário_cep();
      alert("CEP não encontrado.");
    }
  }
  
  function pesquisacep(valor) {
    
    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');
    
    //Verifica se campo cep possui valor informado.
    if (cep != "") {
      
      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;
      
      //Valida o formato do CEP.
      if(validacep.test(cep)) {
        
        //Preenche os campos com "..." enquanto consulta webservice.
        document.getElementsByName('shipping_address').value="...";
        document.getElementsByName('shipping_state').value="...";
        document.getElementsByName('shipping_city').value="...";
        
        //Cria um elemento javascript.
        var script = document.createElement('script');
        
        //Sincroniza com o callback.
        script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
        
        //Insere script no documento e carrega o conteúdo.
        document.body.appendChild(script);
        
      } //end if.
      else {
        //cep é inválido.
        limpa_formulário_cep();
        alert("Formato de CEP inválido.");
      }
    } //end if.
    else {
      //cep sem valor, limpa formulário.
      limpa_formulário_cep();
    }
  };
</script>  --}}
@endpush
