@extends('layouts.assinatura')
@section('Top')
@endsection
@section('Content') 
  <section class="box-assinatura">
    <div class="container">
      <div class="content white-bg">
        <div class="row">
          <div class="col-md-4 offset-md-1">
            <h4>Informações pessoais</h4>
            <p class="mb-5 mt-5">Conectado como <a href="{{url('/painel-home')}}">{{Auth::user()->name}}</a></p>
            <h4>Informações de entrega</h4>
            <form class="text-left mt-4 mb-5">
              <div class="form-row d-flex align-items-center">
                <div class="col-md-6 mb-4">
                  <label for="exampleInputCep">CEP</label>
                  <input type="text" value="{{$cep}}" class="form-control" placeholder="Ex: 03503-000">
                </div>
                <div class="col-md-6 mb-4 text-center">
                  <label></label>
                  <a href="" class="var">Esqueceu sua senha?</a>
                </div>        
              </div>  
              <div class="form-row">
                <div class="col-md-12 mb-4">                  
                  <label for="exampleInputEnd">Endereço</label>
                  <input type="text" class="form-control" placeholder="Ex: Rua das flores">                 
                </div>
              </div> 
              <div class="form-row">
                <div class="col-md-6 mb-4">                  
                  <label for="exampleInputNum">Número</label>
                  <input type="text" class="form-control" placeholder="Ex: 375">                 
                </div>
                <div class="col-md-6 mb-4">                  
                  <label for="exampleInputComple">Complemento</label>
                  <input type="text" class="form-control" placeholder="Ex: Sala XX, Apartamento XX, etc">                 
                </div>
              </div> 
              <div class="form-row">
                <div class="col-md-6 mb-4">                  
                  <label for="exampleInputBairro">Bairro</label>
                  <input type="text" class="form-control" placeholder="Ex: Vila Madalena">                 
                </div>
                <div class="col-md-6 mb-4">                  
                  <label for="exampleInputCidade">Cidade</label>
                  <input type="text" class="form-control" placeholder="Ex: São Paulo">                 
                </div>
              </div>        
            </form>
            <h4>Informações pagamento</h4>
            <form class="text-left mt-4 mb-5">
              <div class="form-row">
                <div class="col-md-12 mb-4 security-card">
                  <label for="exampleInputNumbCard">Número do cartão</label>
                  <input type="text" class="form-control pr-5" placeholder="Ex: 1234 4321 5678 8921">
                  <input type="hidden" name="flag" class="form-control pr-5" placeholder="Ex: 1234 4321 5678 8921">                  
                  <img src="{{ asset('img/security-card.png') }}" class="img-fluid" alt="">
                  <p class="mb-0 mt-2"><img src="{{ asset('img/security-key.png') }}" class="img-fluid" alt=""> Ambiente 100% seguro.</p>
                </div>      
              </div>  
              <div class="form-row">
                <div class="col-md-12 mb-4">                  
                  <label for="exampleInputNameCard">Nome no cartão</label>
                  <input type="text" class="form-control" placeholder="Ex: JOÃO DA SILVA S.">                 
                </div>
              </div> 
              <div class="form-row">
                <div class="col-md-6 mb-4">                  
                  <label for="exampleInputValidCard">Data de validade</label>
                  <input type="text" class="form-control" placeholder="01/2017">                 
                </div>
                <div class="col-md-6 mb-4">                  
                  <label for="exampleInputCodCard">Cód. de segurança</label>
                  <input type="text" class="form-control" placeholder="Ex: 1234">                 
                </div>
              </div>         
            </form>
          </div>
          <div class="col-md-5 offset-md-1 text-center">
            <div class="content-checkout">
              <div class="title">
                <h3 class="var mb-0">Revisão de pedido</h3>
              </div>
              <div class="body">
                <div class="table-responsive">
                  <table class="table text-left mb-0">                  
                    <tbody>
                      <tr>
                        <th scope="row">Tamanho do box:</th>
                        <td><span>P</span></td>
                        <td class="text-right"><a href="">Alterar</a></td>                      
                      </tr>
                      <tr>
                        <th scope="row">Tamanho do box:</th>
                        <td><span>Segunda</span></td>
                        <td class="text-right"><a href="">Alterar</a></td>                      
                      </tr>
                      <tr>
                        <th scope="row">Período escolhido:</th>
                        <td><span>Tarde</span></td>
                        <td class="text-right"><a href="">Alterar</a></td>                      
                      </tr>
                      <tr>
                        <th scope="row">
                          <big>Valor Mensal:</big>
                          <small class="mt-1">Cupom aplicado: <span>Desconto Maroto</span></small>
                        </th>
                        <td></td>
                        <td class="text-right value">
                          <div class="old-price"><span>R$</span>99<sup>,00</sup></div>
                          <div class="price"><span>R$</span>75<sup>,00</sup></div>
                        </td>                      
                      </tr>
                    </tbody>
                  </table>                  
                </div>                
              </div>
            </div>
            <form class="text-left mt-4 mb-5">            
              <div class="form-row d-flex align-items-end">
                <div class="col-md-7 mb-3">                  
                  <label for="exampleInputCupomDisc">Cupom de desconto</label>
                  <input type="text" class="form-control" placeholder="Insira seu cupom">                 
                </div>
                <div class="col-md-5">                  
                  <button type="submit" class="button button-secondary">Inserir</button>                 
                </div>
              </div>         
            </form>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 offset-md-3">
            <button type="submit" class="button button-secondary">Finalizar compra</button>  
          </div>
        </div>
      </div>      
    </div>
  </section>
@endsection
@section('Footer')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h5 class="var mb-3">Institucional</h5>
        <ul class="list-unstyled">
          <li><a href="/sobre-nos">Sobre Nós</a></li>
          <li><a href="/termos-condicoes">Termos e Condições</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Atendimento</h5>
        <ul class="list-unstyled">
          <li><a href="/fale-conosco">Fale Conosco</a></li>
          <li><a href="/perguntas-frequentes">Perguntas Frequentes</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Formas de pagamento</h5>
        <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Redes sociais</h5>
        <ul class="list-unstyled">
          <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
          <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>   
  <section class="copyright mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
        </div>
      </div>
    </div>
  </section>   
</footer>
@endsection
@push('scripts')
  <!-- Scripts -->
  <script type="text/javascript">
    console.log(PagSeguroDirectPayment.setSessionId('{{ PagSeguro::startSession() }}'));
  </script>
  <script>
      $(document).ready(function(){
        $('#pag_pagamento').addClass('active');
          $("form").submit(function() {
              $('input[name="shipping_cep"]').unmask();
          });
          
          pesquisacep($('input[name="shipping_cep"]').val());          
      });

        // busca cep
  
  function limpa_formulário_cep() {
    //Limpa valores do formulário de cep.
    $('input[name="shipping_address"]').val("");
    $('input[name="shipping_state"]').val("");
    $('input[name="shipping_city"]').val("");
  }
  
  function meu_callback(conteudo) {
    if (!("erro" in conteudo)) {
      //Atualiza os campos com os valores.
      $('input[name="shipping_address"]').val(conteudo.logradouro);
      $('input[name="shipping_state"]').val(conteudo.bairro);
      $('input[name="shipping_city"]').val(conteudo.localidade);
    } //end if.
    else {
      //CEP não Encontrado.
      limpa_formulário_cep();
      alert("CEP não encontrado.");
    }
  }
  
  function pesquisacep(valor) {
    
    //Nova variável "cep" somente com dígitos.
    var cep = valor.replace(/\D/g, '');
    
    //Verifica se campo cep possui valor informado.
    if (cep != "") {
      
      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;
      
      //Valida o formato do CEP.
      if(validacep.test(cep)) {
        
        //Preenche os campos com "..." enquanto consulta webservice.
        document.getElementsByName('shipping_address').value="...";
        document.getElementsByName('shipping_state').value="...";
        document.getElementsByName('shipping_city').value="...";
        
        //Cria um elemento javascript.
        var script = document.createElement('script');
        
        //Sincroniza com o callback.
        script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
        
        //Insere script no documento e carrega o conteúdo.
        document.body.appendChild(script);
        
      } //end if.
      else {
        //cep é inválido.
        limpa_formulário_cep();
        alert("Formato de CEP inválido.");
      }
    } //end if.
    else {
      //cep sem valor, limpa formulário.
      limpa_formulário_cep();
    }
  };
  </script>
@endpush
