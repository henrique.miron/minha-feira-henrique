@extends('layouts.assinatura')
@section('Top')
@endsection
@section('Content') 
<section class="box-assinatura">
  <div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="content white-bg">
      <div class="row">
        <div class="col-md-4 offset-md-1">
          <form class="text-left mt-4 mb-5" method="post" action="{{url('finalizar-compra')}}" id="order">
            {{ csrf_field() }}
            <h4>Informações pessoais</h4>
            <p class="mb-5 mt-5">Conectado como <a href="{{url('/painel-home')}}">{{Auth::user()->name}}</a></p>
            <div class="form-row">
                <div class="form-group col-12 col-md-12 mb-4">
                  <label>CPF</label>
                  <input type="text" name="cpf" value="{{ isset(Auth::user()->cpf) ? Auth::user()->cpf : old('cpf') }}" class="form-control cpf" placeholder="Ex: 000.000.000-00">
                </div>
                <div class="form-group col-12 col-md-12 mb-4">
                  <label>Telefone</label>
                  <input type="text" name="phone" value="{{ isset(Auth::user()->phone) ? Auth::user()->phone : old('phone') }}" class="form-control phone" placeholder="Ex: (11)1111-11111">
                </div>
                <div class="form-group col-12 col-md-12 mb-4">
                  <label>Data de Nascimento</label>
                  <input type="text" name="birth_date" value="{{ isset(Auth::user()->birth_date) ? Auth::user()->birth_date : old('birth_date') }}" class="form-control data required" placeholder="Ex: 11/11/1990">
                </div>
              </div>
            <h4>Informações de entrega</h4>
            <input type="hidden" id="senderHash" name="senderHash" value="">
            <input type="hidden" id="cardToken" name="card_token" value="null">
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <div class="form-row d-flex align-items-center">
              <div class="col-md-6 mb-4">
                <label for="exampleInputCep">CEP</label>
                <input type="text" name="cep" value="{{$cep}}" class="form-control cep" placeholder="Ex: 03503-000">
              </div>
              {{-- <div class="col-md-6 mb-4 text-center">
                <label></label>
                <a href="" class="var">Esqueceu sua senha?</a>
              </div> --}}
            </div>  
            <div class="form-row">
              <div class="col-md-12 mb-4">                  
                <label for="exampleInputEnd">Endereço</label>
                <input type="text" name="street" value="{{ old('street') }}" class="form-control required" placeholder="Ex: Rua das flores">                 
              </div>
            </div> 
            <div class="form-row">
              <div class="col-md-6 mb-4">                  
                <label for="exampleInputNum">Número</label>
                <input type="text" name="number" value="{{ old('number') }}" class="form-control required" placeholder="Ex: 375">                 
              </div>
              <div class="col-md-6 mb-4">                  
                <label for="exampleInputComple">Complemento</label>
                <input type="text" name="complement" value="{{ old('complement') }}" class="form-control" placeholder="Ex: Sala XX, Apartamento XX, etc">                 
              </div>
            </div> 
            <div class="form-row">
              <div class="col-md-6 mb-4">                  
                <label for="exampleInputBairro">Bairro</label>
                <input type="text" name="district" value="{{ old('district') }}" class="form-control required" placeholder="Ex: Vila Madalena">                 
              </div>
              <div class="col-md-6 mb-4">                  
                <label for="exampleInputCidade">Cidade</label>
                <input type="text" name="city" value="{{ old('city') }}" class="form-control required" placeholder="Ex: São Paulo">                 
              </div>
              <div class="col-md-6 mb-4">                  
                  <label for="exampleInputCidade">Estado</label>
                  <input type="text" name="state" value="{{ old('state') }}" class="form-control required" placeholder="Ex: São Paulo">                 
                </div>
            </div>        
            <!-- </form> -->
            <h4>Informações pagamento</h4>
            <!-- <div class="text-left mt-4 mb-5"> -->
              <div class="card-wrapper"></div>
              <div class="form-row">
                <div class="col-md-12 mb-4 security-card">
                  <label for="exampleInputNumbCard">Número do cartão</label>
                  <input type="text" name="card_number" class="form-control pr-5 required" placeholder="Ex: 1234 4321 5678 8921">
                  <input type="hidden" name="flag" value="">
                  <img src="{{ asset('img/security-card.png') }}" class="img-fluid" alt="">
                  <p class="mb-0 mt-2"><img src="{{ asset('img/security-key.png') }}" class="img-fluid" alt=""> Ambiente 100% seguro.</p>
                </div>      
              </div>  
              <div class="form-row">
                <div class="col-md-12 mb-4">                  
                  <label for="exampleInputNameCard">Nome no cartão</label>
                  <input type="text" name="card_name" class="form-control required" placeholder="Ex: JOÃO DA SILVA S.">                 
                </div>
              </div> 
              <div class="form-row">
                <div class="col-md-6 mb-4">                  
                  <label for="exampleInputValidCard">Data de validade</label>
                  <input type="text" name="valid_thru" class="form-control required" placeholder="01/2017">              
                </div>
                <div class="col-md-6 mb-4">                  
                  <label for="exampleInputCodCard">Cód. de segurança</label>
                  <input type="text" name="card_security" class="form-control required" placeholder="Ex: 1234">                 
                </div>
              </div>         
              <!-- </div> -->
            </div>
            <div class="col-md-5 offset-md-1 text-center">
              <div class="content-checkout">
                <div class="title">
                  <h3 class="var mb-0">Revisão de pedido</h3>
                </div>
                <div class="body">
                  <div class="table-responsive">
                    <table class="table text-left mb-0">                  
                      <tbody>
                        <tr>
                          <th scope="row">Tamanho do box:</th>
                          <td><span>{{$kit->name}}</span></td>
                          <input type="hidden" value="{{$kit->id}}" name="kit_id">
                          <td class="text-right"><a href="#" data-toggle="modal" data-target=".modal-tamanho">Alterar</a></td>                      
                        </tr>
                        <tr>
                          <th scope="row">Dia escolhido:</th>
                          <td><span>{{\App\Helpers\PedidoHelper::formatarDiaEntrega($diaEntrega)}}</span></td>
                          <input type="hidden" value="{{$diaEntrega}}" name="weekday">
                          <td class="text-right"><a href="#" data-toggle="modal" data-target=".modal-dia">Alterar</a></td>                      
                        </tr>
                        <tr>
                          <th scope="row">Período escolhido:</th>
                          <td><span>{{\App\Helpers\PedidoHelper::formatarPeriodoEntrega($periodoEntrega)}}</span></td>
                          <input type="hidden" value="{{$periodoEntrega}}" name="period">
                          <td class="text-right"><a href="#" data-toggle="modal" data-target=".modal-periodo">Alterar</a></td>                      
                        </tr>
                        <tr>
                          <th scope="row"><big>Valor Mensal:</big></th>
                          <td></td>
                          <td class="text-right value">
                            <div class="old-price d-none"><span>R$</span>99<sup>,00</sup></div>
                            <div class="price"><span>R$</span>{{intval($kit->price)}}<sup>,00</sup></div>
                            {{--  <input type="hidden" name="price" value="{{$kit->price}}">
                            <input type="hidden" name="price_initial" value="{{$kit->price}}">
                            <input type="hidden" name="price_discount" value="{{$kit->price}}">  --}}
                            <input type="hidden" name="price_freight" value="0">
                          </td>                      
                        </tr>
                      </tbody>
                    </table>                  
                  </div>                
                </div>
              </div>
              <div class="text-left mt-4 mb-5">
                <div class="form-row">
                  <div class="col-md-7 mb-3">                  
                    <label for="exampleInputCupomDisc">Cupom de desconto</label>
                    <input type="text" name="cupom" id="cupom" class="form-control" placeholder="Insira seu cupom">
                    <input type="hidden" name="discount_coupon">
                  </div>
                  <div class="col-md-5 mt-md-4">                  
                    <button type="button" id="cupom_button" class="button button-secondary">Inserir</button>                 
                  </div>
                </div>         
              </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-6 offset-md-3">
              <button type="button" id="finalizar" class="button button-secondary">Finalizar compra</button>  
            </div>
          </div>
        </form>
      </div>      
    </div>
  </section>
  <form id="alterar" class="d-none" method="POST" action="{{url('kit-alterar')}}">
    {{ csrf_field() }}
    <input type="hidden" name="session" value="true">
    <input type="hidden" name="period">
    <input type="hidden" name="weekday">
  </form>
    @include('includes.modal.tamanho')
    @include('includes.modal.dia')
    @include('includes.modal.periodo')
    @endsection
    @section('Footer')
    @endsection
    @push('scripts')
    <!-- Scripts -->
    <script type="text/javascript" src="/pagseguro/javascript"></script>
    <script type="text/javascript">
    PagSeguroDirectPayment.setSessionId('{{ PagSeguro::startSession() }}')
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.lista-periodos .nav-item .nav-link').on('click', function(e) {
              $('input[name=period]').val(this.id);
              $('form#alterar').submit();
          });

        $('.lista-dias .nav-item .nav-link').on('click', function(e) {
          $('input[name=weekday]').val(this.id);
          $('form#alterar').submit();
        })

        $('button.kit').on('click', function(e){
          $('input[name=kit_id]').val(e.target.id);
        })
      })
    </script>
    @endpush
    