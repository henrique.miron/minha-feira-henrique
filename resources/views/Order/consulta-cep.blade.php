@extends('layouts.consulta-cep')
@section('Top')
  <section class="header header-interna">
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light">
              <a class="navbar-brand" href="#"><img src="{{ asset('img/logo-final.png') }}" class="img-fluid" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="/index">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="/kits">Box</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/sobre-nos">Sobre Nós</a>
                  </li>  
                  <li class="nav-item">
                    <a class="nav-link" href="/como-funciona">Como Funciona</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link login" href="/login">Entrar</a>
                  </li>                             
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>
  </section>
@endsection
@section('Content') 
  <section class="cep-box">
    <div class="container">
      <div class="row mb-3">
        <div class="col-md-12 text-center">
          <h2>Nós entregamos no seu endereço!</h2>
          <h4>Não perca tempo, escolha agora mesmo o melhor box para receber em sua casa.</h4>
        </div>
      </div>
    </div>
  </section>
  <section class="planos">
      <div class="container">
        <div class="row p-10 align-items-center">
            <div class="col-md-4">
                <div class="content p white-bg">
                    <img src="{{ asset('img/top-plano-green.png') }}" class="img-fluid position-absolute" alt="">
                    <img src="{{ asset('img/icon-p.png') }}" class="img-fluid position-absolute" alt="">
                    <div class="title text-center mx-auto">
                        <p>Kit ideal para <span>02</span> pessoas</p>
                    </div>
                    <div class="body mt-4">
                        <h4 class="text-center">Quantidade Semanal</h4>
                        <ul class="list-unstyled mt-3">
                            <li><span>03</span>Aqui vai um item da cesta</li>
                            <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                            <li><span>01</span>item dessa cesta lorem lorem</li>
                            <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                            <li><span>03</span>Outro item da cesta</li>
                        </ul>
                    </div>
                    <div class="footer text-center">
                        <p><span>R$</span>99<sup>,00</sup><small>mês</small></p>
                        <a href="" class="button button-secondary">Assinar Agora</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="content m white-bg">
                    <img src="{{ asset('img/top-plano-orange.png') }}" class="img-fluid position-absolute" alt="">
                    <img src="{{ asset('img/icon-m.png') }}" class="img-fluid position-absolute" alt="">
                    <img src="{{ asset('img/icon-sale.png') }}" class="img-fluid position-absolute" alt="">
                    <div class="title text-center mx-auto">
                        <p>Kit ideal para <span>03</span> pessoas</p>
                    </div>
                    <div class="body mt-4">
                        <h4 class="text-center">Quantidade Semanal</h4>
                        <ul class="list-unstyled mt-3">
                            <li><span>03</span>Aqui vai um item da cesta</li>
                            <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                            <li><span>01</span>item dessa cesta lorem lorem</li>
                            <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                            <li><span>03</span>Outro item da cesta</li>
                        </ul>
                    </div>
                    <div class="footer text-center">
                        <p><span>R$</span>120<sup>,00</sup><small>mês</small></p>
                        <a href="" class="button button-tertiary">Assinar Agora</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="content g white-bg">
                    <img src="{{ asset('img/top-plano-green.png') }}" class="img-fluid position-absolute" alt="">
                    <img src="{{ asset('img/icon-g.png') }}" class="img-fluid position-absolute" alt="">
                    <div class="title text-center mx-auto">
                        <p>Kit ideal para <span>04</span> pessoas</p>
                    </div>
                    <div class="body mt-4">
                        <h4 class="text-center">Quantidade Semanal</h4>
                        <ul class="list-unstyled mt-3">
                            <li><span>03</span>Aqui vai um item da cesta</li>
                            <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                            <li><span>01</span>item dessa cesta lorem lorem</li>
                            <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                            <li><span>03</span>Outro item da cesta</li>
                        </ul>
                    </div>
                    <div class="footer text-center">
                        <p><span>R$</span>150<sup>,00</sup><small>mês</small></p>
                        <a href="" class="button button-secondary">Assinar Agora</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
@endsection
@section('Footer')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h5 class="var mb-3">Institucional</h5>
        <ul class="list-unstyled">
          <li><a href="/sobre-nos">Sobre Nós</a></li>
          <li><a href="/termos-condicoes">Termos e Condições</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Atendimento</h5>
        <ul class="list-unstyled">
          <li><a href="/fale-conosco">Fale Conosco</a></li>
          <li><a href="/perguntas-frequentes">Perguntas Frequentes</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Formas de pagamento</h5>
        <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Redes sociais</h5>
        <ul class="list-unstyled">
          <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
          <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>   
  <section class="copyright mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
        </div>
      </div>
    </div>
  </section>   
</footer>
@endsection
@push('scripts')
  <!-- Scripts -->
  <script>
      $(document).ready(function(){
          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
  </script>
@endpush
