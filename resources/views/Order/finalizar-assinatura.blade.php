
@extends('layouts.assinatura')
@section('Top')
@endsection
@section('Content') 
  <section class="box-assinatura">
    <div class="container">
      <div class="content white-bg">
        <div class="row">
          <div class="col-md-8 offset-md-2 text-center">
            <img src="{{ asset('img/cesta-final.png') }}" class="img-fluid d-block mx-auto" alt="">
            <h3 class="var-2 mt-5 mb-4">Pronto! Você já assinou o box!</h3>
            <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os em.</p>
            <p>Sua primeira entrega será <span id="selected-dia">Segunda-Feira</span>, <span id="selected-data">09 de fevereiro</span> no periodo da <span id="selected-periodo">tarde</span>.</p>
            <a href="{{url('painel-home')}}">Ir para o painel do usuário</a>
          </div>
        </div>
      </div>      
    </div>
  </section>
@endsection
@section('Footer')
@endsection
@push('scripts')
  <!-- Scripts -->
  <script>
    const dia = {{ \Session::get('diaEntrega') }};
    const periodo = {{ \Session::get('periodoEntrega') }};
      $(document).ready(function(){
        

        console.log(dia)
        
        $('#selected-data').html(claculateNextDate(dia).toLocaleDateString('pt-br'));
        
              if(periodo == 0){
                $('#selected-periodo').html('manhã');
                $('#horario').html('Entre 08h e 11h.');
              }else if(periodo == 1){
                $('#selected-periodo').html('tarde');
                $('#horario').html('Entre 12h e 18h.')
              }else if(periodo == 2){
                $('#selected-periodo').html('noite');
                $('#horario').html('Entre 19h e 21h.')
              }

              if(dia == 0){
                $('#selected-dia').html('Segunda-Feira');
              }else if(dia == 1){
                $('#selected-dia').html('Terça-Feira');
              }else if(dia == 2){
                $('#selected-dia').html('Quarta-Feira');
              }else if(dia == 3){
                $('#selected-dia').html('Quinta-Feira');
              }else if(dia == 4){
                $('#selected-dia').html('Sexta-Feira');
              }
      });
  </script>
@endpush
