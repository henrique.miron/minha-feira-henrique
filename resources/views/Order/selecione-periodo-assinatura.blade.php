@extends('layouts.assinatura')
@section('Top')
@endsection
@section('Content')
  <form method="POST" action="{{url('verificar-login')}}">
    <input type="hidden" name="inputDia" value="0">
    <input type="hidden" name="inputPeriodo" value="0">
    {{csrf_field()}}
    <section class="box-assinatura">
      <div class="container">
        <div class="content white-bg">
          <div class="row">
            <div class="col-md-6 offset-md-3 text-center">
              <h2>Qual o melhor dia para receber?</h2>
              <ul class="nav nav-pills nav-periodo lista-dias mt-4 d-flex align-items-center justify-content-center" id="pills-tab-dias" role="tablist">
                <li class="nav-item">
                  <a class="nav-link @if(!isset($diaEntrega) || $diaEntrega == 0) active @endif" id="0" data-toggle="pill" href="#pills-seg" role="tab" aria-controls="pills-seg" aria-selected="true">Seg</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link @if(isset($diaEntrega) && $diaEntrega == 1) active @endif" id="1" data-toggle="pill" href="#pills-ter" role="tab" aria-controls="pills-ter" aria-selected="false">Ter</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link @if(isset($diaEntrega) && $diaEntrega == 2) active @endif" id="2" data-toggle="pill" href="#pills-qua" role="tab" aria-controls="pills-qua" aria-selected="false">Qua</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link @if(isset($diaEntrega) && $diaEntrega == 3) active @endif" id="3" data-toggle="pill" href="#pills-qui" role="tab" aria-controls="pills-qui" aria-selected="false">Qui</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link @if(isset($diaEntrega) && $diaEntrega == 4) active @endif" id="4" data-toggle="pill" href="#pills-sex" role="tab" aria-controls="pills-sex" aria-selected="false">Sex</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="row mt-5">
            <div class="col-md-8 offset-md-2 text-center">
              <h2>Qual o melhor período para receber?</h2>
              <ul class="nav nav-pills nav-periodo lista-periodos mt-4 d-flex align-items-center justify-content-center" id="pills-tab-hora" role="tablist">
                <li class="nav-item">
                  <a class="nav-link @if(!isset($periodoEntrega) || $periodoEntrega == 0) active @endif" id="0" data-toggle="pill" href="#pills-manha" role="tab" aria-controls="pills-manha" aria-selected="true">Manhã</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link @if(isset($periodoEntrega) && $periodoEntrega == 1) active @endif" id="1" data-toggle="pill" href="#pills-tarde" role="tab" aria-controls="pills-tarde" aria-selected="false">Tarde</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link @if(isset($periodoEntrega) && $periodoEntrega == 2) active @endif" id="2" data-toggle="pill" href="#pills-noite" role="tab" aria-controls="pills-noite" aria-selected="false">Noite</a>
                </li>
              </ul>
              <small>Extimativa de entrega: <b id="horario">Entre 08h e 11h.</b></small>
            </div>
          </div>
          <div class="row mt-5">
            <div class="col-md-8 offset-md-2 text-center">
              <p>Você receberá seu primeiro box <span id="selected-dia">Segunda-Feira</span>, <span id="selected-data">09 de fevereiro</span> no periodo da <span id="selected-periodo">tarde.</span></p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 offset-md-4">
                <button type="submit" class="button button-secondary">Continuar</button>
            </div>
          </div>
        </div>
      </div>
    </section>
  </form>
@endsection
@section('Footer')
@endsection
@push('scripts')
  <!-- Scripts -->
  <script>
      $(document).ready(function(){
        $('#selected-data').html(claculateNextDate($('.nav-periodo').first().find('a.active').attr('id')).toLocaleDateString('pt-br'));
        
          $('.lista-periodos .nav-item .nav-link').on('click', function(e) {
              $('input[name=inputPeriodo]').val(this.id);
              if(this.id == 0){
                $('#selected-periodo').html('manhã');
                $('#horario').html('Entre 08h e 11h.');
              }else if(this.id == 1){
                $('#selected-periodo').html('tarde');
                $('#horario').html('Entre 12h e 18h.')
              }else if(this.id == 2){
                $('#selected-periodo').html('noite');
                $('#horario').html('Entre 19h e 21h.')
              }
          });

          $('.lista-dias .nav-item .nav-link').on('click', function(e) {
              $('input[name=inputDia]').val(this.id);
              let data = claculateNextDate(this.id).toLocaleDateString('pt-br');
              $('#selected-data').html(data);
              if(this.id == 0){
                $('#selected-dia').html('Segunda-Feira');
              }else if(this.id == 1){
                $('#selected-dia').html('Terça-Feira');
              }else if(this.id == 2){
                $('#selected-dia').html('Quarta-Feira');
              }else if(this.id == 3){
                $('#selected-dia').html('Quinta-Feira');
              }else if(this.id == 4){
                $('#selected-dia').html('Sexta-Feira');
              }
          })
      });
  </script>
@endpush
