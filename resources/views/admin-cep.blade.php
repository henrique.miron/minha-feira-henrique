@extends('layouts.admin')
@section('Top')
@endsection
@section('Content') 
<section class="painel-usuario">
  <div class="container">
    <div class="row">
      @include('layouts.menu-admin')
      <div class="col-md-9">
        <div class="row mb-5">
          <div class="col-md-12">
            <h4 class="border-painel mb-4">Configuração de CEP</h4>
          </div>
          <div class="col-md-12">
            <div class="content white-bg tabela">
              <div class="table-responsive">
                <table class="table table-striped mb-0">
                  <thead>
                    <tr>
                      <th scope="col">CEP</th>
                      <th scope="col">Endereço</th>
                      <th scope="col"><a href="#"  data-toggle="modal" data-target="#novoCep"><i class="fa fa-plus" aria-hidden="true"></i>Adicionar novo CEP</a></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $cep)
                    <tr>
                      <td>{{$cep->cep_origin}} - {{$cep->cep_destination}}</td>
                      <td>{{$cep->address}}</td>
                      <td><a href="#" class="editar-cep" id="{{$cep->id}}">Editar</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- Modal CEP -->
          <div class="modal fade" id="novoCep" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Insira o CEP</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <form method="POST" action="{{url('inserir-cep')}}">
                  {{ csrf_field() }}
                  <input type="hidden" name="id">
                  <div class="modal-body">
                    <div class="form-group">
                      <label>CEP Origem</label>
                      <input class="form-control required cep" name="cep_origin">
                    </div>
                    <div class="form-group">
                      <label>CEP Destino:</label>
                      <input class="form-control required cep" name="cep_destination">
                    </div>
                    <div class="form-group">
                        <label>Endereço:</label>
                        <input class="form-control required" name="address">
                      </div>
                      <div class="form-group">
                        <label>Valor:</label>
                        <input class="form-control required" name="price_freight">
                      </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn button-secondary" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn button-primary">Salvar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('Footer')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h5 class="var mb-3">Institucional</h5>
        <ul class="list-unstyled">
          <li><a href="/sobre-nos">Sobre Nós</a></li>
          <li><a href="/termos-condicoes">Termos e Condições</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Atendimento</h5>
        <ul class="list-unstyled">
          <li><a href="/fale-conosco">Fale Conosco</a></li>
          <li><a href="/perguntas-frequentes">Perguntas Frequentes</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Formas de pagamento</h5>
        <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Redes sociais</h5>
        <ul class="list-unstyled">
          <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
          <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>   
  <section class="copyright mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
        </div>
      </div>
    </div>
  </section>   
</footer>
@endsection
@push('scripts')
<!-- Scripts -->
<script>
  $(document).ready(function(){
    $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
    $("form").submit(function() {
      $('input[name="cep"]').unmask();
    });

    $('a.editar-cep').on('click', function(e){
      $.get( "/get-cep/"+$(e.target).attr('id'), function( data ) {
        console.log(data)
        $.each(data, function(i, e){
          console.log($(document).find('input[name="'+i+'"]').val(e));
        })
        $('div#novoCep').modal('show');
        $('div#novoCep').find('form').attr('action', '{{url("alterar-cep")}}');
      });
    })

    $('#novoCep').on('hidden.bs.modal', function (e) {
      $(e.target).find('input').val('');
      $(e.target).find('form').attr('action', '{{url("inserir-cep")}}');
    })
  });
</script>
@endpush
