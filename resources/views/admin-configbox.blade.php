@extends('layouts.admin')
@section('Top')
@endsection
@section('Content') 
<section class="painel-usuario">
  <div class="container">
    <div class="row">
      @include('layouts.menu-admin')
      <div class="col-md-9">
        <div class="row mb-5">
          <div class="col-md-12">
            <h4 class="border-painel mb-4">Configuração do box</h4>
          </div>
          <div class="col-md-12">
            <div class="content">
              @foreach($kits as $kit)
              <div class="col-md-12">
                <div class="content white-bg interna">
                  <div class="row d-flex align-items-center">
                    <div class="col-md-3 mb-3">
                      <img src="{{ asset('img/cesta-planos.png') }}" class="img-fluid" alt="">
                    </div>
                    <div class="col-md-9">
                      <div class="body">
                        <h5>Kit {{ $kit->name }}</h5>
                        <ul class="list-unstyled mt-3">
                          <li><span>03</span>Aqui vai um item da cesta</li>
                          <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                          <li><span>01</span>item dessa cesta lorem lorem</li>
                          <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                          <li><span>03</span>Outro item da cesta</li>
                        </ul>
                        <div class="form-group input-group">
                          {{--  <form method="GET" action="{{ 'deletar-kit/'.$kit->id }}">  --}}
                            <button type="button" class="button button-primary mt-3" data-toggle="modal" data-target="#{{ $kit->id }}Modal">Editar</button>
                            {{--  <button type="sumit" class="button button-primary mt-3">Deletar</button>  --}}
                          {{--  </form>  --}}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Modal Box -->
              <div id="{{ $kit->id }}Modal" class="modal fade modal-periodo" tabindex="-1" role="dialog" aria-labelledby="modalBox" aria-hidden="true">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <section>
                      <div class="container">
                        <div class="row">
                          <div class="col-md-12 text-center">
                            <h2>Editar Box {{ $kit->name }}</h2>
                          </div>
                          <div class="col-12 text-center">
                            <form method="POST" action="{{ url('editar-kit') }}">
                              {{ csrf_field() }}
                              <input type="hidden" name="id" value="{{ $kit->id }}" class="form-control">
                              <h6>Nome/valor</h6>
                              <div class="form-group input-group">
                                <input name="name" value="{{ $kit->name }}" class="form-control">
                                <input name="price" value="{{ $kit->price }}" class="form-control">
                              </div>
                              <br>
                              <h5>Conteúdo</h5>
                              <h6>Quantidade/nome</h6>
                              @if(!$kit->attrs->isEmpty())
                                @foreach($kit->attrs as $attr)
                                  <div class="form-group input-group atributos">
                                      <input name="attr_id[]" type="hidden" value="{{ $attr->id }}">
                                      <input type="number" name="attr_quantity[]" class="form-control" value="{{ $attr->quantity }}" placeholder="Quantidade"/>
                                      <input type="text" name="attr_name[]" class="form-control" value="{{ $attr->name }}" placeholder="Nome "/>
                                      <button type="button" class="input-group-text add"><i class="fa fa-plus-square"></i></button>
                                      <button type="button" class="input-group-text remove"><i class="fa fa-minus-square"></i></button>
                                  </div>
                                @endforeach
                              @else
                                <div class="form-group input-group atributos">
                                    <input name="attr_id[]" type="hidden" value="null">
                                    <input name="attr_quantity[]" class="form-control" placeholder="Quantidade"/>
                                    <input name="attr_name[]" class="form-control" placeholder="Nome "/>
                                    <button type="button" class="input-group-text add"><i class="fa fa-plus-square"></i></button>
                                    <button type="button" class="input-group-text remove"><i class="fa fa-minus-square"></i></button>
                                </div>
                              @endif
                              <button class="button button-secondary">Atualizar Box</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
              @endforeach
              <button class="button button-secondary"  data-toggle="modal" data-target="#cadastrarModal">Cadastrar novo box</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div id="cadastrarModal" class="modal fade modal-periodo" tabindex="-1" role="dialog" aria-labelledby="modalBox" aria-hidden="true">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <section>
          <div class="container">
            <div class="row">
              <div class="col-md-12 text-center">
                <h2>Cadastrar Box</h2>
              </div>
              <div class="col-12 text-center">
                <form method="POST" action="{{ url('cadastrar-kit') }}" id="novoBox">
                  {{ csrf_field() }}
                  <label for="name">Nome do Kit</label>
                  <div class="form-group input-group">
                    <input name="name" value="" id="name" class="form-control required">
                    <input name="price" value="" id="price" class="form-control required">
                  </div>
                  <br>
                  <h5>Conteudo</h5>
                  <div class="form-group input-group atributos">
                      <input type="number" name="quantity" class="form-control" placeholder="Quantidade"/>
                      <input type="text" name="name_attr" class="form-control" placeholder="Nome "/>
                      <button type="button" class="input-group-text add"><i class="fa fa-plus-square"></i></button>
                      <button type="button" class="input-group-text remove"><i class="fa fa-minus-square"></i></button>
                  </div>
                  <button class="button button-secondary">Cadastrar Box</button>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</section>
@endsection
@section('Footer')
@endsection
@push('scripts')
<script>
  $(document).ready(function(){
    $(document).on('click', 'button.add', function(e){
      $(e.target).closest('.atributos').first().clone().insertAfter($(e.target).closest('.atributos').last());
    })
    $(document).on('click', 'button.remove', function(e){
      if($('.atributos').length !== 1){
        $(e.target).closest('.atributos').fadeOut('slow').remove();
      }
    })
  })
  
</script>
@endpush
