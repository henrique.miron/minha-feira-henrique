@extends('layouts.admin')
@section('Top')
@endsection
@section('Content') 
<section class="painel-usuario">
  <div class="container">
    <div class="row">
      @include('layouts.menu-admin')
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-4">
            <div class="content white-bg box text-center">
              <a href="{{url('admin-configbox')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/meu-box.svg#Camada1') }}"></use>
                </svg><br>
                Configuração do box
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="content white-bg box text-center">
              <a href="{{url('admin-cep')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/marker.svg#Camada1') }}"></use>
                </svg><br>
                Configurar CEP
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="content white-bg box text-center">
              <a href="{{url('admin-usuarios')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/historico.svg#Camada1') }}"></use>
                </svg><br>
                Usuários e pedidos
              </div>
            </div>
        </div>
        <div class="row">          
            <div class="col-md-4">
              <div class="content white-bg box text-center">
                <a href="{{url('admin-relatorios')}}">
                  <svg class="svg-painel icon-box">
                    <use xlink:href="{{ asset('img/icons/entrega.svg#Camada1') }}"></use>
                  </svg><br>
                  Entregas do dia e vendas
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection
  @section('Footer')
  @endsection
