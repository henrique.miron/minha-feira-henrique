@extends('layouts.admin')
@section('Top')
@endsection
@section('Content')
<section class="painel-usuario">
    <div class="container">
        <div class="row">
            @include('layouts.menu-admin')
            <div class="col-md-9">
                <div class="row mb-5">
                    <div class="col-md-12">
                        <h4 class="border-painel mb-4">Usuários e pedidos</h4>
                    </div>
                    <div class="row content white-bg p-2 ml-2">
                        <div class="col-12 col-md-6">
                            <span>Informações Basicas</span>
                            <ul>
                                <li><b>Data de Entrega:</b> {{ app('App\Helpers\DateHelper')->ENtoBR($pedido->created_at) }}</li>
                                <li><b>ID:</b> {{ $pedido->id }}</li>
                                <li><b>Valor:</b> {{ $pedido->price }}</li>
                                <li><b>Desconto:</b> {{ $pedido->price_discount }}</li>
                                <li><b>Status do Pagamento:</b> {{ app('App\Helpers\assinaturaHelper')->formatarStatus($pedido->payments) }}</li>
                            </ul>
                        </div>
                        
                        <div class="col-12 col-md-6">
                            <span>Endereço</span>
                            <ul>
                                <li><b>CEP:</b> {{ $pedido->address->cep }}</li>
                                <li><b>Cidade:</b> {{ $pedido->address->city }}</li>
                                <li><b>Rua:</b> {{ $pedido->address->street }}</li>
                                <li><b>Numero:</b> {{ $pedido->address->number }}</li>
                                <li><b>Estado:</b> {{ $pedido->address->state }}</li>
                            </ul>
                        </div>
                        
                        <div class="col-md-6 offset-md-3 mb-2">
                            <form method="POST" action="{{ url('finalizar-entrega') }}">
                                {{ csrf_field() }}
                                <input name="id" value="{{ $pedido->id }}" hidden>
                                <button class="button button-secondary">Finalizar Entrega</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="indicacao">
            <div class="container">
                <div class="content white-bg">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Que tal receber 10% de desconto na próxima mensalidade?</h3>
                            <p>Você ganha o desconto quando sua indicação fizer uma compra em nosso site usando o link enviado por email.</p>
                        </div>
                        <div class="col-md-6">
                            <form class="mt-5">
                                <div class="form-row">
                                    <div class="col-md-7 mb-3">
                                        <input type="email" name="email" class="form-control" placeholder="Ex: amigo@meuamigo.com">
                                    </div>
                                    <div class="col-md-5">
                                        <button type="submit" class="button button-secondary">Indicar</button>
                                        <small class="form-text text-muted text-center">*limite de 1 por pessoa.</small>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endsection
        @section('Footer')
        @endsection
        @push('scripts')
        <!-- Scripts -->
        <script>
            $(document).ready(function(){
                $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
                $("form").submit(function() {
                    $('input[name="cep"]').unmask();
                });
            });
        </script>
        @endpush
        