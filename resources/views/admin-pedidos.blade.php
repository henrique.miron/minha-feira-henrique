@extends('layouts.admin')
@section('Top')
@endsection
@section('Content') 
<section class="painel-usuario">
  <div class="container">
    <div class="row">
      @include('layouts.menu-admin')
      <div class="col-md-9">
        <div class="row mb-5">
          <div class="col-md-12">
            <h4 class="border-painel mb-4">Usuários e pedidos</h4>
          </div>
          <div class="col-md-12">
            <form method="POST" action="{{ url('pedido/'.$id) }}" id="filtro">
              {{ csrf_field() }}
              <input name="filtro" type="hidden" value="{{ $filtro }}">
            </form>
            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link  @if($filtro == 0) active @endif" id="pendente-tab" data-toggle="tab" href="#pendente" role="tab" aria-controls="pendente" aria-selected="true">Pedidos Pendentes</a>
              </li>
              <li class="nav-item">
                <a class="nav-link @if($filtro == 1) active @endif" id="entregue-tab" data-toggle="tab" href="#entregue" role="tab" aria-controls="entregue" aria-selected="false">Pedidos Entregues</a>
              </li>
            </ul>
            <div class="tab-content white-bg" id="myTabContent">
              {{--  <div class="tab-pane fade show active" id="pendente" role="tabpanel" aria-labelledby="pendente-tab">  --}}
                <div class="table-responsive">
                  <table class="table table-striped mb-0">
                    <thead>
                      <tr>
                        <th scope="col">Data</th>
                        <th scope="col">Código</th>
                        <th scope="col">Entregue?</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    @if(isset($orders))
                    <form method="POST" action="{{ url('finalizar-entrega') }}" id="status">                    
                      <tbody>
                        @foreach($orders as $order)
                        <tr>
                          <td>{{ app('App\Helpers\DateHelper')->ENtoBR($order->delivery->date) }}</td>
                          <td>{{ $order->id }}</td>
                          <td>
                            {{ csrf_field() }}
                            <input name="id[]" value="{{ $order->id }}" hidden>
                            <input type="checkbox" name="status[]" @if($order->status == 1) checked @endif>
                          </td>
                          <td><a href="{{ url('pedido-detalhe/'.$order->id) }}">Detalhes</a></td> 
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr colspan="5">
                          <td></td>
                          <td >
                            <button class="button button-secondary">@if($filtro == 0)Comletar entrega(s) @else Desfazer entrega(s) @endif</button>
                          </td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tfoot>
                    </form>
                    @endif
                  </table>
                </div>
                {{--  </div>  --}}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="indicacao">
        <div class="container">
          <div class="content white-bg">
            <div class="row">
              <div class="col-md-6">
                <h3>Que tal receber 10% de desconto na próxima mensalidade?</h3>
                <p>Você ganha o desconto quando sua indicação fizer uma compra em nosso site usando o link enviado por email.</p>
              </div>
              <div class="col-md-6">
                <form class="mt-5">
                  <div class="form-row">
                    <div class="col-md-7 mb-3">
                      <input type="email" name="email" class="form-control" placeholder="Ex: amigo@meuamigo.com">
                    </div>
                    <div class="col-md-5">
                      <button type="submit" class="button button-primary">Indicar</button>
                      <small class="form-text text-muted text-center">*limite de 1 por pessoa.</small>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      @endsection
      @section('Footer')
      @endsection
      @push('scripts')
      <!-- Scripts -->
      <script>
        $(document).ready(function(){
          $('input[name="status"]').on('click', function(){
            $('form#status').submit();
          })
          
          $('#pendente-tab').on('click', function(){
            $('input[name="filtro"]').val('0');
            $('form#filtro').submit();
          })
          
          $('#entregue-tab').on('click', function(){
            $('input[name="filtro"]').val('1');
            $('form#filtro').submit();
          })
        });
      </script>
      @endpush