@extends('layouts.admin')
@section('Top')
@endsection
@section('Content') 
<section class="painel-usuario">
  <div class="container">
    <div class="row">
      @include('layouts.menu-admin')
      <div class="col-md-9">
        <div class="row mb-5">
          <div class="col-md-12">
            <h4 class="border-painel mb-4">Relatório de Entregas do dia e vendas</h4>
          </div>
          <div class="col-md-12">
            <ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="hoje-tab" data-toggle="tab" href="#hoje" role="tab" aria-controls="hoje" aria-selected="true">Pedidos de Hoje</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="atrasados-tab" data-toggle="tab" href="#atrasados" role="tab" aria-controls="atrasados" aria-selected="false">Pedidos Atrasados</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="hoje" role="tabpanel" aria-labelledby="hoje-tab">
                <div class="table-responsive">
                  <form method="POST" action="{{ url('finalizar-entrega') }}">
                    <table class="table table-light table-hover mb-0">
                      <thead>
                        <tr>
                          <th scope="col">CEP</th>
                          <th scope="col">Endereço</th>
                          <th scope="col">Entregue</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($deliverys_t as $today)
                        <tr>
                          <td>{{ $today->order->address->cep }}</td>
                          <td>{{ $today->order->address->street }}, {{ $today->order->address->district }} - {{ $today->order->address->city }} - {{ $today->order->address->state }}</td>
                          <td><input name="status" type="checkbox" @if($today->order->status == 1) checked @endif value=""></td>
                          <td>
                            <input name="id[]" type="hidden" value="{{ $late->order->id }}">
                            <input name="status[]" type="checkbox" @if($late->order->status == 1) checked @endif>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                          <td></td>
                          <td><button type="submit" class="button button-secondary">Completar Entregas</button></td>
                          <td></td>
                        </tr>
                      </tfoot>
                    </table>
                  </form>
                </div> 
              </div>
              <div class="tab-pane fade" id="atrasados" role="tabpanel" aria-labelledby="atrasados-tab">
                <div class="table-responsive">
                  <form method="POST" action="{{ url('finalizar-entrega') }}">
                    {{ csrf_field() }}
                    <table class="table table-light table-hover mb-0">
                      <thead>
                        <tr>
                          <th scope="col">CEP</th>
                          <th scope="col">Endereço</th>
                          <th scope="col">Entregue</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($deliverys_l as $late)
                        <tr>
                          <td>{{ $late->order->address->cep }}</td>
                          <td>{{ $late->order->address->street }}, {{ $late->order->address->district }} - {{ $late->order->address->city }} - {{ $late->order->address->state }}</td>
                          <td>
                            <input name="id[]" type="hidden" value="{{ $late->order->id }}">
                            <input name="status[]" type="checkbox" @if($late->order->status == 1) checked @endif>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                      <tfoot>
                        <tr>
                          <td></td>
                          <td><button type="submit" class="button button-secondary">Completar Entregas</button></td>
                          <td></td>
                        </tr>
                      </tfoot>
                    </table>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('Footer')
@endsection
