@extends('layouts.admin')
@section('Top')
@endsection
@section('Content') 
  <section class="painel-usuario">
    <div class="container">
      <div class="row">
        @include('layouts.menu-admin')
        <div class="col-md-9">
          <div class="row mb-5">
            <div class="col-md-12 p-relative">
              <h4 class="border-painel mb-4">Usuários e pedidos</h4>
              <div class="form-search-admin">
                <form>
                  <div class="form-group">
                    <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Pesquisar por nome, código ou data">
                  </div>
                </form>
              </div>
            </div>
            <div class="col-md-12">
              <div class="content white-bg tabela">
                <div class="table-responsive">
                  <table class="table table-striped mb-0">
                    <thead>
                      <tr>
                        <th scope="col">Nome</th>
                        <th scope="col">E-mail</th>
                        <th scope="col">Telefone</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($clientes as $cliente)
                        <tr>
                          <td>{{ $cliente->name }}</td>
                          <td>{{ $cliente->email }}</td>
                          <td>{{ $cliente->phone }}</td>
                          <td><a href="{{ url('pedido/'.$cliente->id) }}">Ver Pedido</a></td>
                        </tr>
                      @endforeach
                    </tbody>
                    <tfoot >
                        <tr><td align="center">{{ $clientes->links() }}</td></tr>
                      </tfoot>
                  </table>
                </div>                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('Footer')
@endsection
