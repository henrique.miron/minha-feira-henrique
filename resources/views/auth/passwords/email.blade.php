@extends('layouts.interna')
@section('Top')
<section class="header header-interna">
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.menu-home')
                </div>
            </div>
        </div>
    </header>
</section>
@endsection
@section('Content')
<section class="title-interna">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>Resetar Senha</h2>
                <p>Informe seu e-mail cadastrado para resetar a senha da sua conta.</p>
            </div>
        </div>
    </div>
</section>  
<section class="login-interna">
    <div class="grey-circle"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                        @endif
                        
                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}
                            
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail</label>
                                
                                <div class="col-12">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                    
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="button button-secondary">
                                        Enviar link para resetar senha
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
