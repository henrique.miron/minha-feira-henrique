@extends('layouts.home')
@section('Top')
<section class="header header-interna">
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="#"><img src="{{ asset('img/logo-final.png') }}" class="img-fluid" alt=""></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('kits')}}">Box</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('sobre-nos')}}">Sobre Nós</a>
                                </li>  
                                <li class="nav-item">
                                    <a class="nav-link" href="{{url('como-funciona')}}">Como Funciona</a>
                                </li> 
                                <li class="nav-item">
                                    <a class="nav-link login" href="{{url('login')}}">Entrar</a>
                                </li>                             
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
</section>
@endsection
@section('Content') 
<section>
    <div class="container">
        <div class="content white-bg">
            <div class="row">
                <div class="col-md-12 p-md-5 text-center after-stripe mb-md-0 mb-5">
                    <h2>Cadastre-se!</h2>
                    <form class="text-left" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="form-row p-5 d-flex justify-content-center">
                            <div class="form-group col-md-12 {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name">Nome</label>
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Insira seu email" required autofocus>
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                            
                            <div class="form-group col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Email</label>
                                <input type="email" name="email" class="form-control" placeholder="Insira seu email">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="password">Senha</label>
                                <input type="password" name="password" class="form-control" placeholder="Insira seu email">
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-md-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="password_confirmation">Confirmar Senha</label>
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Insira seu email">
                            </div> 
                        </div>
                        <div class="form-row d-flex justify-content-center">
                            <div class="col-4">
                                <button type="submit" class="button button-secondary">Continuar</button>
                            </div>
                        </div>           
                    </form>
                </div>
            </div>
        </div>      
    </div>
</section>
@endsection
@section('Footer')
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h5 class="var mb-3">Institucional</h5>
                <ul class="list-unstyled">
                    <li><a href="{{url('sobre-nos')}}">Sobre Nós</a></li>
                    <li><a href="{{url('termos-condicoes')}}">Termos e Condições</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5 class="var mb-3">Atendimento</h5>
                <ul class="list-unstyled">
                    <li><a href="{{('fale-conosco')}}">Fale Conosco</a></li>
                    <li><a href="{{url('perguntas-frequentes')}}">Perguntas Frequentes</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <h5 class="var mb-3">Formas de pagamento</h5>
                <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
            </div>
            <div class="col-md-3">
                <h5 class="var mb-3">Redes sociais</h5>
                <ul class="list-unstyled">
                    <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
                    <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
                </ul>
            </div>
        </div>
    </div>   
    <section class="copyright mt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
                </div>
            </div>
        </div>
    </section>   
</footer>
@endsection
