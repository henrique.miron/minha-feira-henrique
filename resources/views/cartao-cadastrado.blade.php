@extends('layouts.painel')
@section('Top')
@endsection
@section('Content') 
<section class="painel-usuario">
  <div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif
    <div class="row">
      @include('layouts.menu-painel')
      <div class="col-md-9">
        <div class="row mb-5">
          <div class="col-md-12">
            <h4 class="border-painel mb-4">Cartão Cadastrado</h4>
          </div>
          @if(!empty($cartoes))
          <div class="col-md-8">
            <div class="content white-bg text-center interna">
              <h5>Seu cartão atual:</h5>
              <div class="alterar-dia">
                <ul class="list-inline text-left mt-4 mb-3">
                  <li class="list-inline-item">
                    <small>Seu plano será renovado automaticamente em <b>23/02/2018</b></small>
                  </li>
                </ul>
              </div>
              
              @foreach($cartoes as $cartao)
              <div class="cartao mx-auto mb-3">
                <ul class="list-inline text-left">
                  <li class="list-inline-item"><img src="{{ asset('img/icons/cartao.png') }}" class="img-fluid" alt=""></li>
                  <li class="list-inline-item">
                    {{ $cartao->flag}} 
                    {{  app('App\Helpers\orderHelper')->ccMask($cartao->card_number) }}
                    <small>Vencimento: {{  app('App\Helpers\DateHelper')->EnToBr($cartao->valid_thru) }}</small>
                    <form method="POST" action="{{ url('cartao-principal') }}">
                      {{ csrf_field() }}
                      <input type="hidden" id="card_token" name="card_token" value="{{ $cartao->card_token }}">
                      <input type="hidden" name="main" value="1">
                      <input type="hidden" class="senderHash" name="senderHash" value="">
                      <input name="id" value="{{ $cartao->id }}" hidden>
                      @if($cartao->main == 0)
                      <div class="form-group">
                          <button class="button button-secondary mt-2">Principal</button>
                <a href="{{ url('cartao-excluir/'.$cartao->id) }}" class="button button-secondary mt-2" > Excluir </a>
                          
                      </div>
                      @endif
                    </form>
                  </li>
                </ul>
              </div>
              @endforeach
              <button type="submit" class="button button-secondary mt-2 col-md-9" data-toggle="modal" data-target="#modal-cartao">Adicionar Cartão</button>
            </div>
          </div>
          @else
          <div class="alert alert-light text-center w-100" role="alert">
            <h5>Nenhum cartão cadastrado</h5>
            <hr>
            <a href="{{url('kits')}}" class="button button-secondary mt-2 w-25">Cadastrar Agora!</a>              
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
<section class="indicacao">
  <div class="container">
    <div class="content white-bg">
      <div class="row">
        <div class="col-md-6">
          <h3>Que tal receber 10% de desconto na próxima mensalidade?</h3>
          <p>Você ganha o desconto quando sua indicação fizer uma compra em nosso site usando o link enviado por email.</p>
        </div>
        <div class="col-md-6">
          <form class="mt-5">
            <div class="form-row">
              <div class="col-md-7 mb-3">
                <input type="email" name="email" class="form-control" placeholder="Ex: amigo@meuamigo.com">
              </div>
              <div class="col-md-5">
                <button type="submit" class="button button-primary">Indicar</button>
                <small class="form-text text-muted text-center">*limite de 1 por pessoa.</small>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Modal Cartao -->
<div id="modal-cartao" class="modal fade modal-dia" tabindex="-1" role="dialog" aria-labelledby="modalDia" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="container p-0">
        <div class="content">
          <div class="row">
            <div class="col-md-10 offset-md-1 text-center">
              <h2>Cadastrar novo cartão</h2>
              <form method="POST" action="{{ url('cartao-cadastrar') }}" class="p-2" id="order">
                {{ csrf_field() }}
                <input type="hidden" id="cardToken" name="card_token" value="">
                <input type="hidden" class="senderHash" name="senderHash" value="">
                <div class="card-wrapper"></div>
                <label>Nome</label>
                <div class="form-group">
                  <input type="text" name="card_number" class="form-control pr-5 required" placeholder="Ex: 1234 4321 5678 8921">
                  <input type="hidden" name="flag" value=""/>
                </div>
                <div class="form-group">
                  <input type="text" name="card_name" class="form-control required" placeholder="Ex: JOÃO DA SILVA S.">
                </div>
                <div class="form-group">
                  <input type="text" name="valid_thru" class="form-control required" placeholder="01/2017">
                </div>
                <div class="form-group">
                  <input type="text" name="card_security" class="form-control required" placeholder="Ex: 1234">
                </div>
                <div class="form-group">
                  <div class="input-group">
                  <label>principal</label>
                  <input type="checkbox" name="main" class="form-control" value="1">
                  </div>
                </div>
                <button id="finalizar" type="button" class="button button-secondary">Cadastrar</button>
              </form>
              <img src="{{ asset('img/security-card.png') }}" class="img-fluid" alt="">
              <p class="mb-0 mt-2"><img src="{{ asset('img/security-key.png') }}" class="img-fluid" alt=""> Ambiente 100% seguro.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('Footer')
@endsection
@push('scripts')
<!-- Scripts -->
<script type="text/javascript" src="{{ PagSeguro::getUrl()['javascript'] }}"></script>
<script>
  PagSeguroDirectPayment.setSessionId('{{ PagSeguro::startSession() }}'); //PagSeguroRecorrente tem um método identico, use o que preferir neste caso, não tem diferença.
</script>

<script type="text/javascript">
  
  $(document).ready(function(){
    $('button').on('click', function(){
      $('.senderHash').val(PagSeguroDirectPayment.getSenderHash());
    })
  })
</script>
@endpush
