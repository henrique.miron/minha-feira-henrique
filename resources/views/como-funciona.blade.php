@extends('layouts.interna')
@section('Top')
  <section class="header header-interna">
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
          @include('layouts.menu-home')
          </div>
        </div>
      </div>
    </header>
  </section>
@endsection
@section('Content')  
  <section class="title-interna">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2>Como Funciona</h2>
          <p>Entenda como levamos o melhor para sua casa!</p>
        </div>
      </div>
    </div>
  </section>  
  <section class="funciona-interna">
  	<div class="grey-circle"></div>
  	<div class="container">
  		<div class="row">
        <div class="col-md-12">
          <div class="funciona-content mt-5">
            <span>1</span>
            <div class="row">
              <div class="col-md-3 offset-md-1 mb-3">
                <img src="{{ asset('img/icon-tamanho.png') }}" class="img-fluid mx-auto d-block" alt="">
              </div>
              <div class="col-md-7">
                <h2>Sobre os box</h2>
                <p>A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de kits. </p>
                <p>Nossos kits possuem alimentos básicos da sua lista de compras, além de produtos da estação, sempre selecionados e prontos para o consumo, ideal para quem busca praticidade e organização no dia-a-dia. Troque as idas à feira ou ao mercado por momentos de lazer e descanso.</p>
              </div>
            </div>            
          </div>
        </div>
        <div class="col-md-12">
          <div class="funciona-content">
            <span>2</span>
            <div class="row">
              <div class="col-md-7 offset-md-1">
                <h2>Sobre os box</h2>
                <p>A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de kits. </p>
                <p>Nossos kits possuem alimentos básicos da sua lista de compras, além de produtos da estação, sempre selecionados e prontos para o consumo, ideal para quem busca praticidade e organização no dia-a-dia. Troque as idas à feira ou ao mercado por momentos de lazer e descanso.</p>
              </div>        
              <div class="col-md-3 text-center mb-3">
                <img src="{{ asset('img/icon-periodo.png') }}" class="img-fluid mx-auto d-block" alt=""> 
            </div>            
          </div>
        </div>        
  		</div>
      <div class="col-md-12">
          <div class="funciona-content mb-0">
            <span>3</span>
            <div class="row">
              <div class="col-md-3 offset-md-1 mb-3">
                <img src="{{ asset('img/icon-casa.png') }}" class="img-fluid mx-auto d-block" alt="">
              </div>
              <div class="col-md-7">
                <h2>Sobre os box</h2>
                <p>A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de kits. </p>
                <p>Nossos kits possuem alimentos básicos da sua lista de compras, além de produtos da estação, sempre selecionados e prontos para o consumo, ideal para quem busca praticidade e organização no dia-a-dia. Troque as idas à feira ou ao mercado por momentos de lazer e descanso.</p>
              </div>
            </div>            
          </div>
        </div>
  	</div>
  </div>
  </section> 
  <section class="funciona-box">
    <div class="container">
      <div class="row mb-3">
        <div class="col-md-12 text-center">
          <h2>Ficou com vontade de assinar?</h2>
          <h4>Veja agora nossas opções de box e escolha a melhor para você!</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 offset-md-4">
          <a href="/kits" class="button button-secondary">Conhecer</a>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('Footer')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h5 class="var mb-3">Institucional</h5>
        <ul class="list-unstyled">
          <li><a href="{{url('sobre-nos')}}">Sobre Nós</a></li>
          <li><a href="{{url('termos-condicoes')}}">Termos e Condições</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Atendimento</h5>
        <ul class="list-unstyled">
          <li><a href="{{('fale-conosco')}}">Fale Conosco</a></li>
          <li><a href="{{url('perguntas-frequentes')}}">Perguntas Frequentes</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Formas de pagamento</h5>
        <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Redes sociais</h5>
        <ul class="list-unstyled">
          <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
          <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>   
  <section class="copyright mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
        </div>
      </div>
    </div>
  </section>   
</footer>
@endsection
@push('scripts')
  <!-- Scripts -->
  <script>
      $(document).ready(function(){
           $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
  </script>
@endpush
