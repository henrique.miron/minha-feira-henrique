@extends('layouts.painel')
@section('Top')
@endsection
@section('Content') 
  @if(Session::has('msg'))
    <div class="alert alert-success">
      {{ Session::get('msg') }}
    </div>
  @endif
  <section class="painel-usuario">
    <div class="container">
      <div class="row">
        @include('layouts.menu-painel')
        <div class="col-md-9">
          <div class="row mb-5">
            <div class="col-md-12">
              <h4 class="border-painel mb-4">Endereço cadastrado</h4>
            </div>
            @if($enderecos->count() !== 0)
            <div class="col-md-6">
              @foreach($enderecos as $endereco)
              <div class="content white-bg text-center interna">
                <h5>Seu endereço atual:</h5>
                <div class="alterar-dia">
                  <ul class="list-inline text-left mt-4 mb-3">
                    <li class="list-inline-item"><img src="{{ asset('img/icons/endereco.png') }}" class="img-fluid" alt=""></li>
                    <li class="list-inline-item">
                      {{ $endereco->street }}, {{ $endereco->number }}
                      <small>{{ $endereco->district }}, {{ $endereco->state }}</small>
                    </li>
                  </ul>
                </div>
                <button type="submit" class="button button-secondary mt-2" data-toggle="modal" data-target="#modal-endereco">Alterar Endereço</button>
              </div>
              @endforeach
            </div>
            <!-- Modal Cartao -->
            <div id="modal-endereco" class="modal fade modal-endereco" tabindex="-1" role="dialog" aria-labelledby="modalEndereco" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="container p-0">
                      <div class="content">
                        <div class="row">
                          <div class="col-md-12 text-center">
                            <h2>Cadastrar novo cartão</h2>
                            <form method="POST" action="{{ url('endereco-editar') }}" class="p-2">
                              {{ csrf_field() }}
                              <input type="hidden" name="id" value="{{ $enderecos->first()->id }}">
                              <label>CEP</label>
                              <div class="form-group">
                                <input type="text" name="state" class="form-control pr-5 cep required" value="{{ $enderecos->first()->cep }}" placeholder="">
                              </div>
                              <label>Estado</label>
                              <div class="form-group">
                                <input type="text" name="state" class="form-control pr-5 required" value="{{ $enderecos->first()->state }}" placeholder="">
                              </div>
                              <label>Cidade</label>
                              <div class="form-group">
                                  <input type="text" name="city" class="form-control pr-5 required" value="{{ $enderecos->first()->city }}" placeholder="">
                              </div>
                              <label>Bairro</label>
                              <div class="form-group">
                                  <input type="text" name="district" class="form-control pr-5 required" value="{{ $enderecos->first()->district }}" placeholder="">
                              </div>
                              <label>Rua</label>
                              <div class="form-group">
                                  <input type="text" name="street" class="form-control pr-5 required" value="{{ $enderecos->first()->street }}" placeholder="">
                              </div> 
                              <label>Número</label>
                              <div class="form-group">
                                  <input type="text" name="number" class="form-control pr-5 required" value="{{ $enderecos->first()->number }}" placeholder="">
                              </div>
                              <label>Complemento</label>
                              <div class="form-group">
                                  <input type="text" name="complement" class="form-control pr-5 required" value="{{ $enderecos->first()->complement }}" placeholder="">
                              </div> 
                              <button id="finalizar" type="submit" class="button button-secondary">Cadastrar</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @else
            <div class="alert alert-light text-center w-100" role="alert">
              <h5>Nenhum endereço box assinado</h5>
              <hr>
              <a href="{{url('kits')}}" class="button button-secondary mt-2 w-25">Assinar Agora!</a>              
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="indicacao">
    <div class="container">
      <div class="content white-bg">
        <div class="row">
          <div class="col-md-6">
            <h3>Que tal receber 10% de desconto na próxima mensalidade?</h3>
            <p>Você ganha o desconto quando sua indicação fizer uma compra em nosso site usando o link enviado por email.</p>
          </div>
          <div class="col-md-6">
            <form class="mt-5">
            <div class="form-row">
              <div class="col-md-7 mb-3">
                <input type="email" name="email" class="form-control" placeholder="Ex: amigo@meuamigo.com">
              </div>
              <div class="col-md-5">
                <button type="submit" class="button button-primary">Indicar</button>
                <small class="form-text text-muted text-center">*limite de 1 por pessoa.</small>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>

@endsection
@section('Footer')
@endsection
@push('scripts')
  <!-- Scripts -->
  <script>
      $(document).ready(function(){
          $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
  </script>
@endpush
