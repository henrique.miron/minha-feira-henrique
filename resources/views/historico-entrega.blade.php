@extends('layouts.painel')
@section('Top')
@endsection
@section('Content') 
  <section class="painel-usuario">
    <div class="container">
      <div class="row">
        @include('layouts.menu-painel')
        <div class="col-md-9">
          <div class="row mb-5">
            <div class="col-md-12">
              <h4 class="border-painel mb-4">Histórico de Entregas</h4>
            </div>
            <div class="col-md-12">
              <div class="content white-bg text-center tabela">
                <div class="table-responsive">
                  <table class="table table-striped mb-0">
                    <thead>
                      <tr>
                        <th scope="col">Data</th>
                        <th scope="col">Endereço</th>
                        <th scope="col">Situação</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($orders as $order)
                      <tr>
                        <td>{{ $order->delivery->date }}</td>
                        <td>{{ $order->address->street }}, n {{ $order->address->number }} - {{ $order->address->city }} - {{ $order->address->state }}</td>
                        <td>{{ app('App\Helpers\orderHelper')->formatarStatus($order->status) }}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="indicacao">
    <div class="container">
      <div class="content white-bg">
        <div class="row">
          <div class="col-md-6">
            <h3>Que tal receber 10% de desconto na próxima mensalidade?</h3>
            <p>Você ganha o desconto quando sua indicação fizer uma compra em nosso site usando o link enviado por email.</p>
          </div>
          <div class="col-md-6">
            <form class="mt-5">
            <div class="form-row">
              <div class="col-md-7 mb-3">
                <input type="email" name="email" class="form-control" placeholder="Ex: amigo@meuamigo.com">
              </div>
              <div class="col-md-5">
                <button type="submit" class="button button-primary">Indicar</button>
                <small class="form-text text-muted text-center">*limite de 1 por pessoa.</small>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('Footer')
@endsection
@push('scripts')
  <!-- Scripts -->
  <script>
      $(document).ready(function(){
          $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
  </script>
@endpush
