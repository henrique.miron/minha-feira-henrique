<!-- Modal Dia -->
<div id="modal-dia" class="modal fade modal-dia" tabindex="-1" role="dialog" aria-labelledby="modalDia" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <section class="box-assinatura p-0">
        <div class="container p-0">
          <div class="content">
            <div class="row">
              <div class="col-md-12 text-center">
                <h2>Qual o melhor dia para receber?</h2>
                <ul class="nav nav-pills nav-periodo lista-dias mt-4 d-flex align-items-center justify-content-center" id="pills-tab-dias" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link {{ isset($order->weekday) && $order->weekday == '0' ? 'active' : null }}" id="0" data-toggle="pill" href="#pills-seg" role="tab" aria-controls="pills-seg" aria-selected="true">Seg</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link  {{ isset($order->weekday) && $order->weekday == '1' ? 'active' : null }}" id="1" data-toggle="pill" href="#pills-ter" role="tab" aria-controls="pills-ter" aria-selected="false">Ter</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link {{ isset($order->weekday) && $order->weekday == '2' ? 'active' : null }}" id="2" data-toggle="pill" href="#pills-qua" role="tab" aria-controls="pills-qua" aria-selected="false">Qua</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link {{ isset($order->weekday) && $order->weekday == '3' ? 'active' : null }}" id="3" data-toggle="pill" href="#pills-qui" role="tab" aria-controls="pills-qui" aria-selected="false">Qui</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link {{ isset($order->weekday) && $order->weekday == '4' ? 'active' : null }}" id="4" data-toggle="pill" href="#pills-sex" role="tab" aria-controls="pills-sex" aria-selected="false">Sex</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>