    <!-- Modal Periodo -->
    <div id="modal-periodo" class="modal fade modal-periodo" tabindex="-1" role="dialog" aria-labelledby="modalPeriodo" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <section class="box-assinatura p-0">
            <div class="container p-0">
              <div class="content white-bg">
                <div class="row mt-5">
                  <div class="col-md-12 text-center">
                    <h2>Qual o melhor período para receber?</h2>
                    <ul class="nav nav-pills nav-periodo lista-periodos mt-4 d-flex align-items-center justify-content-center" id="pills-tab-hora" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link {{ isset($order->period) && $order->period == 'Manhã' ? 'active' : null }}" id="0" data-toggle="pill" href="#pills-manha" role="tab" aria-controls="pills-manha" aria-selected="true">Manhã</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ isset($order->period) && $order->period == 'Tarde' ? 'active' : null }}" id="1" data-toggle="pill" href="#pills-tarde" role="tab" aria-controls="pills-tarde" aria-selected="false">Tarde</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ isset($order->period) && $order->period == 'Noite' ? 'active' : null }}" id="2" data-toggle="pill" href="#pills-noite" role="tab" aria-controls="pills-noite" aria-selected="false">Noite</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>