<!-- Modal Tamanho -->
<div class="modal fade modal-tamanho" tabindex="-1" role="dialog" aria-labelledby="modalTamanho" id="modal-tamanho" aria-hidden="true">
  <div class="modal-dialog modal-lg lg-x2">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <section class="kits-interna">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center">
              <h3>Selecione o melhor tamanho para você!</h3>
            </div>
            <div class="col-md-12">
              <div class="tabs-kit">
                <ul class="nav nav-tabs nav-justified white-bg" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="tamanho-p-tab" data-toggle="tab" href="#tamanho-p" role="tab" aria-controls="tamanho-p" aria-selected="true">
                      P<br>
                      <small>Box ideal para <span>02</span> pessoas</small>
                    </a>
                  </li>
                  <li class="nav-item">
                    <img src="{{ asset('img/icon-sale-interna.png') }}" class="img-fluid position-absolute" alt="">
                    <a class="nav-link" id="tamanho-m-tab" data-toggle="tab" href="#tamanho-m" role="tab" aria-controls="tamanho-m" aria-selected="false">
                      M<br>
                      <small>Box ideal para <span>03</span> pessoas</small>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="tamanho-g-tab" data-toggle="tab" href="#tamanho-g" role="tab" aria-controls="tamanho-g" aria-selected="false">
                      G<br>
                      <small>Box ideal para <span>05</span> pessoas</small>
                    </a>
                  </li>
                </ul>
              </div>
              
              <form method="POST" id="kit-alterar" action="{{url('kit-alterar')}}">
                {{csrf_field()}}
                <input type="hidden" name="kit" value="tamanho-p-tab" />
                <div class="tab-content tabs-kit-content" id="myTabContent">
                  <div class="tab-pane fade show active white-bg" id="tamanho-p" role="tabpanel" aria-labelledby="tamanho-p-tab">
                    <img src="{{ asset('img/top-green-plano-interna.png') }}" class="img-fluid position-absolute" alt="">
                    <img src="{{ asset('img/icon-p.png') }}" class="img-fluid position-absolute" alt="">
                    <div class="row">
                      <div class="col-md-4">
                        <img src="{{ asset('img/cesta-planos.png') }}" class="img-fluid" alt="">
                      </div>
                      <div class="col-md-6">
                        <h2>Box semanal tamanho P</h2>
                        <p>A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de box.</p>
                        <p>*Economia baseada no preço médio dos hortifrutis de São Paulo.</p>
                        <div class="body mt-4">
                          <ul class="list-unstyled mt-3">
                            <li><span>03</span>Aqui vai um item da cesta</li>
                            <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                            <li><span>01</span>item dessa cesta lorem lorem</li>
                            <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                            <li><span>03</span>Outro item da cesta</li>
                          </ul>
                        </div>
                        <div class="footer text-left col-md-7">
                          <p><span>R$</span>90<sup>,00</sup><small>mês</small></p>
                          <button type="button" class="button button-secondary kit" id="1">Alterar</button>
                        </div>
                      </div>
                      <div class="col-md-2 position-relative">
                        <div class="interna-descontos">
                          <p>Ideal para</p>
                          <div class="fly text-center">
                            <p>02</p>
                            <span>pessoas</span>
                            <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                          </div>
                        </div>
                        <div class="interna-descontos">
                          <p>Box com</p>
                          <div class="fly text-center">
                            <p>16%</p>
                            <span>de desconto</span>
                            <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                          </div>
                        </div>
                        <div class="interna-descontos">
                          <p>Restam</p>
                          <div class="fly text-center">
                            <p>07</p>
                            <span>box a venda</span>
                            <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="tab-pane m fade white-bg" id="tamanho-m" role="tabpanel" aria-labelledby="tamanho-m-tab">
                    <img src="{{ asset('img/top-green-plano-interna.png') }}" class="img-fluid position-absolute" alt="">
                    <img src="{{ asset('img/icon-m.png') }}" class="img-fluid position-absolute" alt="">
                    <div class="row">
                      <div class="col-md-4">
                        <img src="{{ asset('img/cesta-planos.png') }}" class="img-fluid" alt="">
                      </div>
                      <div class="col-md-6">
                        <h2>Box semanal tamanho M</h2>
                        <p>A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de box.</p>
                        <p>*Economia baseada no preço médio dos hortifrutis de São Paulo.</p>
                        <div class="body mt-4">
                          <ul class="list-unstyled mt-3">
                            <li><span>03</span>Aqui vai um item da cesta</li>
                            <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                            <li><span>01</span>item dessa cesta lorem lorem</li>
                            <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                            <li><span>03</span>Outro item da cesta</li>
                          </ul>
                        </div>
                        <div class="footer text-left col-md-7">
                          <p><span>R$</span>120<sup>,00</sup><small>mês</small></p>
                          {{--<form method="POST" action="/informe-cep-assinatura">--}}
                            {{--                        {{csrf_field()}}--}}
                            {{--<input type="hidden" name="kitId" value="2" />--}}
                            <button type="button" class="button button-secondary kit" id="2">Alterar</button>
                            {{--</form>--}}
                          </div>
                        </div>
                        <div class="col-md-2 position-relative">
                          <div class="interna-descontos">
                            <p>Ideal para</p>
                            <div class="fly text-center">
                              <p>03</p>
                              <span>pessoas</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                          <div class="interna-descontos">
                            <p>Box com</p>
                            <div class="fly text-center">
                              <p>16%</p>
                              <span>de desconto</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                          <div class="interna-descontos">
                            <p>Restam</p>
                            <div class="fly text-center">
                              <p>07</p>
                              <span>box a venda</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane g fade white-bg" id="tamanho-g" role="tabpanel" aria-labelledby="tamanho-g-tab">
                      <img src="{{ asset('img/top-green-plano-interna.png') }}" class="img-fluid position-absolute" alt="">
                      <img src="{{ asset('img/icon-g.png') }}" class="img-fluid position-absolute" alt="">
                      <div class="row">
                        <div class="col-md-4">
                          <img src="{{ asset('img/cesta-planos.png') }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-md-6">
                          <h2>Box semanal tamanho G</h2>
                          <p>A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de box.</p>
                          <p>*Economia baseada no preço médio dos hortifrutis de São Paulo.</p>
                          <div class="body mt-4">
                            <ul class="list-unstyled mt-3">
                              <li><span>03</span>Aqui vai um item da cesta</li>
                              <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                              <li><span>01</span>item dessa cesta lorem lorem</li>
                              <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                              <li><span>03</span>Outro item da cesta</li>
                            </ul>
                          </div>
                          <div class="footer text-left col-md-7">
                            <p><span>R$</span>150<sup>,00</sup><small>mês</small></p>
                            <button type="button" class="button button-secondary kit" id="3">Alterar</button>
                          </div>
                        </div>
                        <div class="col-md-2 position-relative">
                          <div class="interna-descontos">
                            <p>Ideal para</p>
                            <div class="fly text-center">
                              <p>05</p>
                              <span>pessoas</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                          <div class="interna-descontos">
                            <p>Box com</p>
                            <div class="fly text-center">
                              <p>16%</p>
                              <span>de desconto</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                          <div class="interna-descontos">
                            <p>Restam</p>
                            <div class="fly text-center">
                              <p>07</p>
                              <span>box a venda</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>