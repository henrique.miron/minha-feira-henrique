<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<section class="header">
  <header>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#"><img src="{{ asset('img/logo-final.png') }}" class="img-fluid" alt=""></a>
              <button class="custom-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="/index">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/kits">Box</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/sobre-nos">Sobre Nós</a>
                </li>  
                <li class="nav-item">
                  <a class="nav-link" href="/como-funciona">Como Funciona</a>
                </li> 
                <li class="nav-item">
                  <a class="nav-link login" href="/login">Entrar</a>
                </li>                             
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>
  <div class="container">
    <div class="row justify-content-end">
      <div class="col-md-9">
        <h1><span>Economia</span>, <span>praticidade</span> e <span><br>qualidade</span> em um só lugar.</h1><br>
        <p>Compre seu kit de frutas, legumes, verduras e hortaliças no tamanho ideal para você e sua família. Receba produtos selecionados no conforto da sua casa toda semana, garantindo alimentos sempre prontos para consumo.</p>
        <form class="mt-5" method="POST" action="{{ url('lead/novo') }}" id="order">
            {{ csrf_field() }}
          <div class="form-row">
            <div class="col-md-3 mb-3">
              <input type="text" name="cep" class="form-control required" placeholder="Insira seu CEP">
            </div>
            <div class="col-md-6 mb-3">
              <input type="email" name="email" class="form-control required" placeholder="Insira seu e-mail">
            </div>
            <div class="col-md-3 mb-3">
              <button type="submit" class="button button-primary">Buscar</button>
            </div>
          </div>
        </form>
          @foreach($errors->all() as $error)
              {{ $error }}
          @endforeach

      </div>
    </div>
  </div>
</section>
  @yield('Conteudo')
    <!-- Scripts -->
    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
      $(document).ready(function(){
           $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
    </script>
</body>
</html>


