@php
use Illuminate\Support\Facades\Session;
@endphp

@extends('layouts.assinatura')
@section('Top')@endsection
@section('Content') 
  <section class="box-assinatura">
    <div class="container">
      <div class="content white-bg">
        <div class="row">
          <div class="col-md-6 offset-md-3 text-center">
            @if(Session::has('error'))
              <div class="alert alert-danger"> {{ Session::get('error') }}</div>
            @endif
            <h2>Informe seu CEP!</h2>
            <p>Digite seu CEP para saber se entregamos na sua região!</p>
            <form class="mt-5" id="order" action="{{route('consulta-cep')}}">
              <div class="form-row text-left">
                <div class="col-md-8 offset-md-2 mb-3">
                  <label for="exampleInputCEP">CEP</label>
                  <input name="cep" type="text" class="form-control required cep" placeholder="Ex: 03503-000">
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-6 offset-md-3">
                  <button type="submit" class="button button-secondary">Continuar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>      
    </div>
  </section>
@endsection
@section('Footer')
@endsection
@push('scripts')
  <!-- Scripts -->
  <script>
      $(document).ready(function(){
        $('#pag_cep').addClass('active');
          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
  </script>
@endpush
