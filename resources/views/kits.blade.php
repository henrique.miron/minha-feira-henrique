@extends('layouts.home')
@section('Top')
  <section class="header header-interna">
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
          @include('layouts.menu-home')
          </div>
        </div>
      </div>
    </header>
  </section>
@endsection
@section('Content')  
  <section class="title-interna">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2>Box</h2>
          <p>Qualidade, economia e praticicidade em um só lugar.</p>
        </div>
      </div>
    </div>
  </section>

  <section class="kits-interna">
    <div class="grey-circle"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h3>Selecione o melhor tamanho para você!</h3>
        </div>
        <div class="col-md-12">
          <div class="tabs-kit">
            <ul class="nav nav-tabs nav-justified white-bg" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="tamanho-p-tab" data-toggle="tab" href="#tamanho-p" role="tab" aria-controls="tamanho-p" aria-selected="true">
                  P<br>
                  <small>Box ideal para <span>02</span> pessoas</small>
                </a>
              </li>
              <li class="nav-item">
                <img src="{{ asset('img/icon-sale-interna.png') }}" class="img-fluid position-absolute" alt="">
                <a class="nav-link" id="tamanho-m-tab" data-toggle="tab" href="#tamanho-m" role="tab" aria-controls="tamanho-m" aria-selected="false">
                  M<br>
                  <small>Box ideal para <span>03</span> pessoas</small>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="tamanho-g-tab" data-toggle="tab" href="#tamanho-g" role="tab" aria-controls="tamanho-g" aria-selected="false">
                  G<br>
                  <small>Box ideal para <span>05</span> pessoas</small>
                </a>
              </li>
            </ul>
          </div>

            <form method="POST" action="{{route('informe-cep-assinatura')}}">
              {{csrf_field()}}
              <input type="hidden" name="kit" value="tamanho-p-tab" />
              <div class="tab-content tabs-kit-content" id="myTabContent">
                <div class="tab-pane fade show active white-bg" id="tamanho-p" role="tabpanel" aria-labelledby="tamanho-p-tab">
                      <img src="{{ asset('img/top-green-plano-interna.png') }}" class="img-fluid position-absolute" alt="">
                      <img src="{{ asset('img/icon-p.png') }}" class="img-fluid position-absolute" alt="">
                      <div class="row">
                        <div class="col-md-4">
                          <img src="{{ asset('img/cesta-planos.png') }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-md-6">
                          <h2>Box semanal tamanho P</h2>
                          <p>A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de box.</p>
                          <p>*Economia baseada no preço médio dos hortifrutis de São Paulo.</p>
                          <div class="body mt-4">
                            <ul class="list-unstyled mt-3">
                                <li><span>03</span>Aqui vai um item da cesta</li>
                                <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                                <li><span>01</span>item dessa cesta lorem lorem</li>
                                <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                                <li><span>03</span>Outro item da cesta</li>
                            </ul>
                          </div>
                          <div class="footer text-left col-md-7">
                              <p><span>R$</span>{{ substr($data[0]->price, 0, -3) }}<sup>,00</sup><small>mês</small></p>
                                <button type="button" class="button button-secondary kit" id="1">Assinar Agora</button>
                          </div>
                        </div>
                        <div class="col-md-2 position-relative">
                          <div class="interna-descontos">
                            <p>Ideal para</p>
                            <div class="fly text-center">
                              <p>02</p>
                              <span>pessoas</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                          <div class="interna-descontos">
                            <p>Box com</p>
                            <div class="fly text-center">
                              <p>16%</p>
                              <span>de desconto</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                          <div class="interna-descontos">
                            <p>Restam</p>
                            <div class="fly text-center">
                              <p>07</p>
                              <span>box a venda</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                <div class="tab-pane m fade white-bg" id="tamanho-m" role="tabpanel" aria-labelledby="tamanho-m-tab">
                      <img src="{{ asset('img/top-green-plano-interna.png') }}" class="img-fluid position-absolute" alt="">
                      <img src="{{ asset('img/icon-m.png') }}" class="img-fluid position-absolute" alt="">
                      <div class="row">
                        <div class="col-md-4">
                          <img src="{{ asset('img/cesta-planos.png') }}" class="img-fluid" alt="">
                        </div>
                        <div class="col-md-6">
                          <h2>Box semanal tamanho M</h2>
                          <p>A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de box.</p>
                          <p>*Economia baseada no preço médio dos hortifrutis de São Paulo.</p>
                          <div class="body mt-4">
                            <ul class="list-unstyled mt-3">
                                <li><span>03</span>Aqui vai um item da cesta</li>
                                <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                                <li><span>01</span>item dessa cesta lorem lorem</li>
                                <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                                <li><span>03</span>Outro item da cesta</li>
                            </ul>
                          </div>
                          <div class="footer text-left col-md-7">
                              <p><span>R$</span>{{ substr($data[1]->price, 0, -3) }}<sup>,00</sup><small>mês</small></p>
                              {{--<form method="POST" action="/informe-cep-assinatura">--}}
                    {{--                        {{csrf_field()}}--}}
                                {{--<input type="hidden" name="kitId" value="2" />--}}
                                <button type="button" class="button button-secondary kit" id="2">Assinar Agora</button>
                              {{--</form>--}}
                          </div>
                        </div>
                        <div class="col-md-2 position-relative">
                          <div class="interna-descontos">
                            <p>Ideal para</p>
                            <div class="fly text-center">
                              <p>03</p>
                              <span>pessoas</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                          <div class="interna-descontos">
                            <p>Box com</p>
                            <div class="fly text-center">
                              <p>16%</p>
                              <span>de desconto</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                          <div class="interna-descontos">
                            <p>Restam</p>
                            <div class="fly text-center">
                              <p>07</p>
                              <span>box a venda</span>
                              <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                <div class="tab-pane g fade white-bg" id="tamanho-g" role="tabpanel" aria-labelledby="tamanho-g-tab">
                  <img src="{{ asset('img/top-green-plano-interna.png') }}" class="img-fluid position-absolute" alt="">
                  <img src="{{ asset('img/icon-g.png') }}" class="img-fluid position-absolute" alt="">
                  <div class="row">
                    <div class="col-md-4">
                      <img src="{{ asset('img/cesta-planos.png') }}" class="img-fluid" alt="">
                    </div>
                      <div class="col-md-6">
                        <h2>Box semanal tamanho G</h2>
                        <p>A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de box.</p>
                        <p>*Economia baseada no preço médio dos hortifrutis de São Paulo.</p>
                        <div class="body mt-4">
                          <ul class="list-unstyled mt-3">
                              <li><span>03</span>Aqui vai um item da cesta</li>
                              <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                              <li><span>01</span>item dessa cesta lorem lorem</li>
                              <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                              <li><span>03</span>Outro item da cesta</li>
                          </ul>
                        </div>
                        <div class="footer text-left col-md-7">
                            <p><span>R$</span>{{ substr($data[2]->price, 0, -3) }}<sup>,00</sup><small>mês</small></p>
                            <button type="button" class="button button-secondary kit" id="3">Assinar Agora</button>
                        </div>
                      </div>
                    <div class="col-md-2 position-relative">
                      <div class="interna-descontos">
                        <p>Ideal para</p>
                        <div class="fly text-center">
                          <p>05</p>
                          <span>pessoas</span>
                          <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                        </div>
                      </div>
                      <div class="interna-descontos">
                        <p>Box com</p>
                        <div class="fly text-center">
                          <p>16%</p>
                          <span>de desconto</span>
                          <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                        </div>
                      </div>
                      <div class="interna-descontos">
                        <p>Restam</p>
                        <div class="fly text-center">
                          <p>07</p>
                          <span>box a venda</span>
                          <img src="{{ asset('img/path-planos.png') }}" class="img-fluid position-absolute" alt="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </section>
  <section class="kits-banner">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <p>Alimentos frescos toda semana para sua família</p>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('Footer')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h5 class="var mb-3">Institucional</h5>
        <ul class="list-unstyled">
          <li><a href="{{url('sobre-nos')}}">Sobre Nós</a></li>
          <li><a href="{{url('termos-condicoes')}}">Termos e Condições</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Atendimento</h5>
        <ul class="list-unstyled">
          <li><a href="{{('fale-conosco')}}">Fale Conosco</a></li>
          <li><a href="{{url('perguntas-frequentes')}}">Perguntas Frequentes</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Formas de pagamento</h5>
        <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Redes sociais</h5>
        <ul class="list-unstyled">
          <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
          <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>   
  <section class="copyright mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
        </div>
      </div>
    </div>
  </section>   
</footer>
@endsection
@push('scripts')
  <!-- Scripts -->
  <script type="text/javascript">
      $(document).ready(function(){
          $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});

          // $('.tabs-kit .nav-link').on('click', function (e) {
          //   e.preventDefault();
          //     $('.tabs-kit .nav-link').on('click', function () {
          //         $('input[name="kit"]').val(this.id);
          //     });
          // });

          $('button.kit').on('click', function(e){
            $('input[name="kit"]').val(e.target.id);
            $('form').submit();
          })

          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
  </script>
@endpush
