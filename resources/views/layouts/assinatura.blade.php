@include('includes.head')
<div class="col-12 alert alert-warning text-center">
		<span class="text-center"><i class="fa fa-exclamation-triangle"></i> Site em Testes</span>
	</div>
@yield('Top')
<section class="header header-interna header-breadcrumb">
	<header>
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-md-1 text-center">
					<a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logo-final-bread.png') }}" class="img-fluid d-blok mx-auto" alt=""></a>
				</div>
				<div class="col-md-11">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb d-flex align-items-center mb-0">
							<li class="breadcrumb-item"><a href="{{url('informe-cep-assinatura')}}" class="{{ Request::is('informe-cep-assinatura') ? 'class="active"' : '' }}">Informe seu CEP</a></li>
							<li class="breadcrumb-item"><a href="{{url('selecione-periodo-assinatura')}}" class="{{ Request::is('selecione-periodo-assinatura') ? "active" : '' }}">Selecione o período</a></li>
							<li class="breadcrumb-item"><a href="{{url('checar-dados')}}" class="{{ Request::is('checar-dados') ? "active" : '' }}" >Informações de pagamento</a></li>
							<li class="breadcrumb-item"><a href="{{url('finalizar-assinatura')}}" >Economia e comodidade</a></li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
	</header>
</section>
@yield('Content')
@yield('Footer')
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h5 class="var mb-3">Institucional</h5>
				<ul class="list-unstyled">
					<li><a href="/sobre-nos">Sobre Nós</a></li>
					<li><a href="/termos-condicoes">Termos e Condições</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<h5 class="var mb-3">Atendimento</h5>
				<ul class="list-unstyled">
					<li><a href="/fale-conosco">Fale Conosco</a></li>
					<li><a href="/perguntas-frequentes">Perguntas Frequentes</a></li>
				</ul>
			</div>
			<div class="col-md-3">
				<h5 class="var mb-3">Formas de pagamento</h5>
				<img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
			</div>
			<div class="col-md-3">
				<h5 class="var mb-3">Redes sociais</h5>
				<ul class="list-unstyled">
					<li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
					<li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
				</ul>
			</div>
		</div>
	</div>   
	<section class="copyright mt-3">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
				</div>
			</div>
		</div>
	</section>   
</footer>
@include('includes.js')
@stack('scripts')
</body>
</html>
