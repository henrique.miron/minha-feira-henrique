@include('includes.head')
@yield('Top')
@yield('Content')
@yield('Footer')
@include('includes.js')
@stack('scripts')
</body>
</html>
