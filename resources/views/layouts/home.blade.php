@include('includes.head')
<div class="col-12 alert alert-warning text-center">
    <span class="text-center"><i class="fa fa-exclamation-triangle"></i> Site em Testes</span>
</div>
@yield('Top')
@yield('Content')
@yield('Footer')
@include('includes.js')
@stack('scripts')
</body>
</html>
