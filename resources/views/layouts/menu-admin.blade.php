<div class="col-md-3 d-none d-md-block d-lg-block">
        <div class="content white-bg">
          <ul class="list-unstyled">
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'admin-home') ? 'active' : '' }}">
              <a href="{{url('admin-home')}}">
                <svg class="svg-painel icon-painel">
                  <use xlink:href="{{ asset('img/icons/painel-home.svg#Camada1') }}"></use>
                </svg>
                Painel
              </a>
            </li>
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'admin-configbox') ? 'active' : '' }}">
              <a href="{{url('admin-configbox')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/meu-box.svg#Camada1') }}"></use>
                </svg>
                Configurações do Box
              </a>
            </li>
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'admin-cep') ? 'active' : '' }}">
              <a href="{{url('admin-cep')}}">
                <svg class="svg-painel icon-marker">
                  <use xlink:href="{{ asset('img/icons/marker.svg#Camada1') }}"></use>
                </svg>
                Configurar CEP
              </a>
            </li>
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'admin-usuarios') ? 'active' : '' }}">
              <a href="{{url('admin-usuarios')}}">
                <svg class="svg-painel icon-historico">
                  <use xlink:href="{{ asset('img/icons/historico.svg#Camada1') }}"></use>
                </svg>
                Usuários e Pedidos
              </a>
            </li>           
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'admin-relatorios') ? 'active' : '' }}">
              <a href="{{url('admin-relatorios')}}">
                <svg class="svg-painel icon-entrega">
                  <use xlink:href="{{ asset('img/icons/entrega.svg#Camada1') }}"></use>
                </svg>
                Entregas do dia e vendas
              </a>
            </li>
            <li>
              <a href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                <svg class="svg-painel icon-sair">
                  <use xlink:href="{{ asset('img/icons/sair.svg#Camada1') }}"></use>
                </svg>
                Sair
              </a>
            </li>
          </ul>
        </div>
      </div>