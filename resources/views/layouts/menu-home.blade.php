            <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset('img/logo-final.png') }}" class="img-fluid" alt=""></a>
                <button class="custom-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                  <li class="nav-item {{ (Route::getFacadeRoot()->current()->uri() == '/') ? 'active' : '' }}">
                    <a class="nav-link" href="{{url('/')}}">Home<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item {{ (Route::getFacadeRoot()->current()->uri() == 'kits') ? 'active' : '' }}">
                    <a class="nav-link" href="{{url('kits')}}">Box </a>
                  </li>
                  <li class="nav-item {{ (Route::getFacadeRoot()->current()->uri() == 'sobre-nos') ? 'active' : '' }}">
                    <a class="nav-link" href="{{url('sobre-nos')}}">Sobre Nós</a>
                  </li>  
                  <li class="nav-item {{ (Route::getFacadeRoot()->current()->uri() == 'como-funciona') ? 'active' : '' }}">
                    <a class="nav-link" href="{{url('como-funciona')}}">Como Funciona</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link login" href="{{url('login')}}">@if(Auth::check()) Painel @else Entrar @endif</a>
                  </li>                             
                </ul>
              </div>
            </nav>