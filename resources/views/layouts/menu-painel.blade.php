<div class="col-md-3 d-none d-md-block d-lg-block">
        <div class="content white-bg">
          <ul class="list-unstyled">
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'painel-home') ? 'active' : '' }}">
              <a href="{{url('painel-home')}}">
                <svg class="svg-painel icon-painel">
                  <use xlink:href="{{ asset('img/icons/painel-home.svg#Camada1') }}"></use>
                </svg>
                Painel
              </a>
            </li>
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'meu-box') ? 'active' : '' }}">
              <a href="{{url('meu-box')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/meu-box.svg#Camada1') }}"></use>
                </svg>
                Meu Box
              </a>
            </li>
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'periodo-box') ? 'active' : '' }}">
              <a href="{{url('periodo-box')}}">
                <svg class="svg-painel icon-data">
                  <use xlink:href="{{ asset('img/icons/data.svg#Camada1') }}"></use>
                </svg>
                Data e período do box
              </a>
            </li>
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'cartao-cadastrado') ? 'active' : '' }}">
              <a href="{{url('cartao-cadastrado')}}">
                <svg class="svg-painel icon-cartao">
                  <use xlink:href="{{ asset('img/icons/cartao.svg#Camada1') }}"></use>
                </svg>
                Cartão cadastrado
              </a>
            </li>
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'endereco-cadastrado') ? 'active' : '' }}">
              <a href="{{url('endereco-cadastrado')}}">
                <svg class="svg-painel icon-endereco">
                  <use xlink:href="{{ asset('img/icons/endereco.svg#Camada1') }}"></use>
                </svg>
                Endereço cadastrado
              </a>
            </li>
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'historico-cobranca') ? 'active' : '' }}">
              <a href="{{url('historico-cobranca')}}">
                <svg class="svg-painel icon-historico">
                  <use xlink:href="{{ asset('img/icons/historico.svg#Camada1') }}"></use>
                </svg>
                Histórico de cobrança
              </a>
            </li>
            <li class="{{ (Route::getFacadeRoot()->current()->uri() == 'historico-entrega') ? 'active' : '' }}">
              <a href="{{url('historico-entrega')}}">
                <svg class="svg-painel icon-entrega">
                  <use xlink:href="{{ asset('img/icons/entrega.svg#Camada1') }}"></use>
                </svg>
                Histórico de entrega
              </a>
            </li>
            <li>
              <a href="{{ route('logout') }}" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
                <svg class="svg-painel icon-sair">
                  <use xlink:href="{{ asset('img/icons/sair.svg#Camada1') }}"></use>
                </svg>
                Sair
              </a>
            </li>
          </ul>
        </div>
      </div>