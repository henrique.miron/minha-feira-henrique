@include('includes.head')
@yield('Top')
<section class="header header-interna header-breadcrumb">
        <header>
          <div class="container">
            <div class="row d-flex align-items-center">
              <div class="col-3 col-md-1 text-center">
                <a class="navbar-brand" href="{{url('/')}}"><img src="{{ asset('img/logo-final-bread.png') }}" class="img-fluid d-blok mx-auto" alt=""></a>
              </div>
              <div class="col-8 col-md-11 text-right">
                <div class="dropdown">
                  Olá, {{ Auth::user()->name }}!
                  <a class="" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{ asset('img/icons/arrow-down.png') }}" class="img-fluid d-blok mx-auto" alt="">
                  </a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      Sair
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      </section>
      @if (session('status'))
        <div class="col-12 alert alert-success alert-dismissible fade show" role="alert">
              {{ session('status') }}
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
        </div>
      @endif
@yield('Content')
@yield('Footer')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h5 class="var mb-3">Institucional</h5>
        <ul class="list-unstyled">
          <li><a href="/sobre-nos">Sobre Nós</a></li>
          <li><a href="/termos-condicoes">Termos e Condições</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Atendimento</h5>
        <ul class="list-unstyled">
          <li><a href="/fale-conosco">Fale Conosco</a></li>
          <li><a href="/perguntas-frequentes">Perguntas Frequentes</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Formas de pagamento</h5>
        <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Redes sociais</h5>
        <ul class="list-unstyled">
          <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
          <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>   
  <section class="copyright mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
        </div>
      </div>
    </div>
  </section>   
</footer>
@include('includes.js')
@stack('scripts')
</body>
</html>
