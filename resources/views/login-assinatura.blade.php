@extends('layouts.assinatura')
@section('Top')
@endsection
@section('Content') 
  <section class="box-assinatura">
    <div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
      <div class="content white-bg">
        <div class="row">
          <div class="col-md-4 offset-md-1 text-center after-stripe mb-md-0 mb-5">
            <h2>Faça seu login!</h2>
            <form class="text-left mt-5" method="POST" action="{{route('checkout-logado-assinatura')}}">
              {{ csrf_field() }}
              <div class="form-row">
              <div class="col-md-12 mb-3">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" value="{{ old('email') }}" name="email" class="form-control" placeholder="Insira seu email">
                @if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
              </div>
              <div class="col-md-12">
                <label for="exampleInputEmail1">Senha</label>
                <input type="password" name="password" class="form-control" placeholder="Insira sua senha">
              </div>
            </div>
            <div class="form-row mt-5 d-flex align-items-center">
              <div class="col-6">
                <a href="">Esqueceu sua senha?</a>
              </div>
              <div class="col-6">
                <button type="submit" class="button button-secondary">Continuar</button>
              </div>
            </div>           
          </form>
          </div>
          <div class="col-md-4 offset-md-2 text-center">
            <h2>Crie sua conta!</h2>
            <form class="text-left mt-5" method="POST" action="{{ route('checkout-assinatura') }}">
              <div class="form-row">
                {{ csrf_field() }}
                <div class="col-md-12 mb-3">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" value="{{ old('email') }}" name="email" class="form-control required" placeholder="Insira seu email">
                </div>
              </div>
              <div class="form-row mt-4 d-flex align-items-center">
                <div class="col-md-8 offset-md-2">
                  <button type="submit" class="button button-secondary">Crie sua conta</button>
                </div>
              </div>
            </form>
          </div>
        </div><br><br>
        <div class="row mt-md-5">
          <div class="col-md-4 offset-md-4">
            <a href="{{url('/redirect')}}" type="submit" class="button button-facebook"><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Conecte-se com Facebook</a>
          </div>
        </div>
      </div>      
    </div>
  </section>
@endsection
@section('Footer')
@endsection
@push('scripts')
  <!-- Scripts -->
  <script>
      $(document).ready(function(){
        $('#pag_periodo').addClass('active');
          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
  </script>
@endpush
