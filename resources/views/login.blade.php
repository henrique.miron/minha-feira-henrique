@extends('layouts.interna')
@section('Top')
<section class="header header-interna">
  <header>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        @include('layouts.menu-home')
        </div>
      </div>
    </div>
  </header>
</section>
@endsection
@section('Content')  
<section class="title-interna">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2>Painel do usuário</h2>
        <p>Cadastre-se ou entre com sua conta e gerencie seu box.</p>
      </div>
    </div>
  </div>
</section>  
<section class="login-interna">
  <div class="grey-circle"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <form method="POST" action="{{ route('login') }}">
          {{ csrf_field() }}
          <div class="form-row">
            <div class="col-md-8 offset-md-2 mb-3 {{ $errors->has('email') ? ' is-invalid' : '' }}" >
              <label for="email">Email</label>
              <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Insira seu email">
              @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
              @endif
            </div>
            <div class="col-md-8 offset-md-2">
              <label for="password">Senha</label>
              <input type="password" name="password" class="form-control" placeholder="Insira sua senha">
              @if ($errors->has('password'))
              <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif
            </div>
          </div>
          <div class="form-row mt-5 d-flex align-items-center">
            <div class="col-md-4 offset-md-2">
              <a href="{{ url('password/reset') }}">Esqueceu sua senha?</a>
            </div>
            <div class="col-md-4">
              <button type="submit" class="button button-secondary">Entrar</button>
            </div>
          </div>
          <div class="form-row mt-5 mb-5 text-center justify-content-center">
            <div class="col-5">
              <div class="separated"></div>
            </div>
            <div class="col-1 text-center">
              <p>OU</p>
            </div>
            <div class="col-5">
              <div class="separated"></div>
            </div>
          </div>
          <div class="form-row">
            <div class="col-md-8 offset-md-2 mb-5">
              <a type="button" href="{{url('/redirect')}}" class="button button-facebook"><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Conecte-se com Facebook</a>
            </div>
            <div class="col-md-6 offset-md-3">
              <a href="{{url('register')}}" class="button button-primary">Crie sua conta</a>
            </div>
          </div>
        </form>  
      </div>
    </div>
  </div>
</section> 
@endsection
@section('Footer')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h5 class="var mb-3">Institucional</h5>
        <ul class="list-unstyled">
          <li><a href="{{url('sobre-nos')}}">Sobre Nós</a></li>
          <li><a href="{{url('termos-condicoes')}}">Termos e Condições</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Atendimento</h5>
        <ul class="list-unstyled">
          <li><a href="{{('fale-conosco')}}">Fale Conosco</a></li>
          <li><a href="{{url('perguntas-frequentes')}}">Perguntas Frequentes</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Formas de pagamento</h5>
        <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Redes sociais</h5>
        <ul class="list-unstyled">
          <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
          <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>   
  <section class="copyright mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
        </div>
      </div>
    </div>
  </section>   
</footer>
@endsection
@push('scripts')
<!-- Scripts -->
<script>
  $(document).ready(function(){
    $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
    $("form").submit(function() {
      $('input[name="cep"]').unmask();
    });
  });
</script>
@endpush
