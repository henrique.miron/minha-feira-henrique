@extends('layouts.painel')
@section('Top')
@endsection
@section('Content')
@if ($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach ($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
<section class="painel-usuario">
  <div class="container">
    <div class="row">
      @include('includes.modal.tamanho')
      @include('layouts.menu-painel')
      <div class="col-md-9">
        <div class="row mb-5">
          <div class="col-md-12">
            <h4 class="border-painel mb-4">Meu box</h4>
          </div>
          @if(Session::has('status'))
          <h1>{{Session::get('status')}}</h1>
          @endif
          @if(!empty($data))
          @if(Auth::user()->payments()->orderBy('id', 'desc')->first()->status == '1')
          <div class="col-12 alert alert-danger">
            <span>Pagamento Pedente</span>
          </div>
          @endif
          <div class="col-md-8">
            <div class="content white-bg interna">
              <div class="row d-flex align-items-center">
                <div class="col-md-3 mb-3">
                  <img src="{{ asset('img/cesta-planos.png') }}" class="img-fluid" alt="">
                </div>
                <div class="col-md-9">
                  <div class="body">
                    <h5>Kit Contratado</h5>
                    <p><b>Tamanho {{$data['kit']->name}}</b></p>
                    <ul class="list-unstyled mt-3">
                      <li><span>03</span>Aqui vai um item da cesta</li>
                      <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                      <li><span>01</span>item dessa cesta lorem lorem</li>
                      <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                      <li><span>03</span>Outro item da cesta</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-4">
            <div class="content white-bg text-center interna">
              <img src="{{ asset('img/icons/meu-box-alterar.png') }}" class="img-fluid mb-2" alt="">
              <h5>Quer alterar seu kit?</h5>
              <small>Troque o tamanho do seu kit em apenas dois cliques!</small>
              <button type="button" id="alterar" class="button button-secondary mt-2" data-toggle="modal" data-target="#modal-tamanho">Alterar</button>
              <a href="cancelar-assinatura/{{ $data['subscription']->id }}" id="cancelar" class="button button-secondary mt-2">Cancelar</a>
            </div>
          </div>
          @else
          <div class="alert alert-light text-center w-100" role="alert">
            <h5>Nenhum box assinado</h5>
            <hr>
            <a href="{{url('kits')}}" class="button button-secondary mt-2 w-25">Assinar Agora</a>
          </div>
          @endif
        </div>
        
        <div class="row">
          <div class="col-12" id="kits">
          </div>
        </div>
        
      </div>
    </section>
    <section class="indicacao">
      <div class="container">
        <div class="content white-bg">
          <div class="row">
            <div class="col-md-6">
              <h3>Que tal receber 10% de desconto na próxima mensalidade?</h3>
              <p>Você ganha o desconto quando sua indicação fizer uma compra em nosso site usando o link enviado por email.</p>
            </div>
            <div class="col-md-6">
              {{-- <form class="mt-5">
                <div class="form-row">
                  <div class="col-md-7 mb-3">
                    <input type="email" name="email" class="form-control" placeholder="Ex: amigo@meuamigo.com">
                  </div>
                  <div class="col-md-5">
                    <button type="submit" class="button button-primary">Indicar</button>
                    <small class="form-text text-muted text-center">*limite de 1 por pessoa.</small>
                  </div>
                </div>
              </form> --}}
            </div>
          </div>
        </div>
      </div>
    </section>
    @endsection
    @section('Footer')
    @endsection
    @push('scripts')
    <!-- Scripts -->
    <script type="text/javascript">
      $(document).ready(function(){
        $('form#kit-alterar').attr('action', '{{url("alterar-assinatura")}}');
        
        $('button.kit').on('click', function(e){
          $('input[name="kit"]').val(e.target.id);
        })
      })
    </script>
    @endpush