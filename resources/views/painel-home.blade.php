@extends('layouts.painel')
@section('Top')
@endsection
@section('Content') 
<section class="painel-usuario">
  <div class="container">
    <div class="row">
      @include('layouts.menu-painel')
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-4">
            <div class="content white-bg box text-center">
              <a href="{{url('meu-box')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/meu-box.svg#Camada1') }}"></use>
                </svg><br>
                Meu Box
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="content white-bg box text-center">
              <a href="{{url('periodo-box')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/data.svg#Camada1') }}"></use>
                </svg><br>
                Data e período do box
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="content white-bg box text-center">
              <a href="{{url('cartao-cadastrado')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/cartao.svg#Camada1') }}"></use>
                </svg><br>
                Cartão cadastrado
              </a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="content white-bg box text-center">
              <a href="{{url('endereco-cadastrado')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/endereco.svg#Camada1') }}"></use>
                </svg><br>
                Endereço cadastrado
              </a>
            </div>
          </div>
          <div class="col-md-4">
            <div class="content white-bg box text-center">
              <a href="{{url('historico-cobranca')}}">
                <svg class="svg-painel icon-box">
                  <use xlink:href="{{ asset('img/icons/historico.svg#Camada1') }}"></use>
                </svg><br>
                Histórico de cobrança
              </div>
            </div>
            <div class="col-md-4">
              <div class="content white-bg box text-center">
                <a href="{{url('historico-entrega')}}">
                  <svg class="svg-painel icon-box">
                    <use xlink:href="{{ asset('img/icons/entrega.svg#Camada1') }}"></use>
                  </svg><br>
                  Histórico de entrega
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="indicacao">
    <div class="container">
      <div class="content white-bg">
        <div class="row">
          <div class="col-md-6">
            <h3>Que tal receber 10% de desconto na próxima mensalidade?</h3>
            <p>Você ganha o desconto quando sua indicação fizer uma compra em nosso site usando o link enviado por email.</p>
          </div>
          <div class="col-md-6">
            <form class="mt-5">
              <div class="form-row">
                <div class="col-md-7 mb-3">
                  <input type="email" name="email" class="form-control" placeholder="Ex: amigo@meuamigo.com">
                </div>
                <div class="col-md-5">
                  <button type="submit" class="button button-primary">Indicar</button>
                  <small class="form-text text-muted text-center">*limite de 1 por pessoa.</small>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  @endsection
  @section('Footer')@endsection
  @push('scripts')
  <!-- Scripts -->
  <script>
    $(document).ready(function(){
      $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
      $("form").submit(function() {
        $('input[name="cep"]').unmask();
      });
    });
  </script>
  @endpush
  