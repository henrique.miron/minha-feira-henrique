@extends('layouts.painel')
@section('Top')
@endsection
@section('Content')
<section class="painel-usuario">
  <div class="container">
    <div class="row">
      @include('includes.modal.periodo')
      @include('includes.modal.dia')
      @include('layouts.menu-painel')
      <div class="col-md-9">
        <div class="row mb-5">
          <div class="col-md-12">
            <h4 class="border-painel mb-4">Data e período do box</h4>
          </div>
          @if(!empty($order))
          @if(Auth::user()->payments()->orderBy('id', 'desc')->first()->status == '1')
          <div class="col-12 alert alert-danger">
            <span>Pagamento Pedente</span>
          </div>
          @endif
          <form id="alterar" class="d-none" method="POST" action="{{url('kit-alterar')}}">
            {{ csrf_field() }}
            <input type="hidden" name="period">
            <input type="hidden" name="weekday">
          </form>
          <div class="col-md-6">
            <div class="content white-bg text-center interna">
              <h5>Seu dia atual:</h5>
              <div class="alterar-dia">
                <ul class="list-inline text-left mt-4 mb-3">
                  <li class="list-inline-item"><img src="{{ asset('img/icons/calendar.png') }}" class="img-fluid" alt=""></li>
                  <li class="list-inline-item">
                    {{ $order->weekdayName }}
                    <small>Próxima entrega será em <b id="remain">{{ $order->ramain }} dias</b></small>
                  </li>
                </ul>
              </div>
              <button type="submit" id="alterar-dia" class="button button-secondary mt-2 alterar" data-toggle="modal" data-target="#modal-dia">Alterar</button>
            </div>
          </div>
          <div class="col-md-6">
            <div class="content white-bg text-center interna">
              <h5>Seu período atual:</h5>
              <div class="alterar-dia">
                <ul class="list-inline text-left mt-4 mb-3">
                  <li class="list-inline-item"><img src="{{ asset('img/icons/relogio.png') }}" class="img-fluid" alt=""></li>
                  <li class="list-inline-item">
                    Durante a {{$order->period}}
                    <small>Entregas ocorrem entre <b id="hora">12h até 18h</b></small>
                  </li>
                </ul>
              </div>
              <button type="submit" id="alterar-periodo" class="button button-secondary mt-2 alterar" data-toggle="modal" data-target="#modal-periodo">Alterar</button>
            </div>
          </div>
          <div class="col-12">
            <div id="period" class="d-none">
              <h5>Alterar Periodo de Entrega</h5>
              <div class="white-bg interna p-5">
                  <div class="col-12 text-center">
                    <h2>Qual o melhor dia para receber?</h2>
                    <ul class="nav nav-pills nav-periodo lista-periodos mt-4 d-flex align-items-center justify-content-center " id="pills-tab-hora" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link " id="0" data-toggle="pill" href="#pills-manha" role="tab" aria-controls="pills-manha" aria-selected="true">Manhã</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="1" data-toggle="pill" href="#pills-tarde" role="tab" aria-controls="pills-tarde" aria-selected="false">Tarde</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" id="2" data-toggle="pill" href="#pills-noite" role="tab" aria-controls="pills-noite" aria-selected="false">Noite</a>
                        </li>
                      </ul>
                  </div>
                </div>
            </div>
            <div id="weekday" class="d-none">
              <div class="white-bg interna">
                <div class="col-12 text-center">
                  <h2>Qual o melhor dia para receber?</h2>
                  <ul class="nav nav-pills nav-periodo lista-dias mt-4 d-flex align-items-center justify-content-center" id="pills-tab-dias" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link" id="0" data-toggle="pill" href="#pills-seg" role="tab" aria-controls="pills-seg" aria-selected="true">Seg</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="1" data-toggle="pill" href="#pills-ter" role="tab" aria-controls="pills-ter" aria-selected="false">Ter</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="2" data-toggle="pill" href="#pills-qua" role="tab" aria-controls="pills-qua" aria-selected="false">Qua</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="3" data-toggle="pill" href="#pills-qui" role="tab" aria-controls="pills-qui" aria-selected="false">Qui</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="4" data-toggle="pill" href="#pills-sex" role="tab" aria-controls="pills-sex" aria-selected="false">Sex</a>
                    </li>
                  </ul>
                </div>
              </div>
              @else
              <div class="alert alert-light text-center w-100" role="alert">
                <h5>Nenhum box assinado 😟</h5>
                <hr>
                <a href="{{url('kits')}}" class="button button-secondary mt-2 w-25">Assinar Agora 🙂</a>              
              </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="indicacao">
      <div class="container">
        <div class="content white-bg">
          <div class="row">
            <div class="col-md-6">
              <h3>Que tal receber 10% de desconto na próxima mensalidade?</h3>
              <p>Você ganha o desconto quando sua indicação fizer uma compra em nosso site usando o link enviado por email.</p>
            </div>
            <div class="col-md-6">
              <form class="mt-5">
                <div class="form-row">
                  <div class="col-md-7 mb-3">
                    <input type="email" name="email" class="form-control" placeholder="Ex: amigo@meuamigo.com">
                  </div>
                  <div class="col-md-5">
                    <button type="submit" class="button button-primary">Indicar</button>
                    <small class="form-text text-muted text-center">*limite de 1 por pessoa.</small>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    @endsection
    @section('Footer')
    @endsection
    @push('scripts')
    <!-- Scripts -->
    <script type="text/javascript">
      $(document).ready(function(){
        $('.lista-periodos .nav-item .nav-link').on('click', function(e) {
              $('input[name=period]').val(this.id);
              $('form#alterar').submit();
          });

          $('.lista-dias .nav-item .nav-link').on('click', function(e) {
              $('input[name=weekday]').val(this.id);
              $('form#alterar').submit();
          })

          if( '{{ $order->period }}' == 'Tarde'){
            $('#hora').html('12h as 18h');
          }else if('{{ $order->period }}' == 'Manhã'){
            $('#hora').html('09h as 11h');
          }else if('{{ $order->period }}' == 'Noite'){
            $('#hora').html('19h as 21h');
          }
      })
    </script>
    @endpush
    