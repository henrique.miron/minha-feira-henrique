@extends('layouts.home')
@section('Top')
  <section class="header">
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            @include('layouts.menu-home')
          </div>
        </div>
      </div>
    </header>
    <div class="container">
      <div class="row justify-content-end">
        <div class="col-md-9">
          <h1><span>Economia</span>, <span>praticidade</span> e <span><br>qualidade</span> em um só lugar.</h1><br>
          <p>Compre seu kit de frutas, legumes, verduras e hortaliças no tamanho ideal para você e sua família. Receba produtos selecionados no conforto da sua casa toda semana, garantindo alimentos sempre prontos para consumo.</p>
          <form class="mt-md-5" method="POST" action="{{ url('/lead/novo') }}">
              {{ csrf_field() }} 
            <div class="form-row">
              <div class="col-md-3 mb-3">
                <input type="text" name="cep" class="form-control" placeholder="Insira seu CEP">
              </div>
              <div class="col-md-6 mb-3">
                <input type="email" name="email" class="form-control" placeholder="Insira seu e-mail">
              </div>
              <div class="col-md-3 mb-3">
                <button type="submit" class="button button-primary">Buscar</button>
              </div>
            </div>
          </form>
          
            @foreach($errors->all() as $error)
                <span class="col-12 alert alert-danger">{{ $error }}</span>
            @endforeach

            @if(\Session::has('msg'))
                <span class="col-12 alert alert-success">{{ \Session::get('msg') }}</span>
            @endif
            
        </div>
      </div>
    </div>
  </section>
@endsection
@section('Content')
  <section class="como-funciona">
      <div class="grey-circle"></div>
      <div class="container">
          <div class="row mb-md-5">
              <div class="col-md-12 text-center" id="como-funciona">
                  <h2>Como Funciona</h2>
              </div>
          </div>
          <div class="row align-items-end">
              <div class="col-md-4 mb-lg-3 mb-5 text-center">
                 <img src="{{ asset('img/icon-tamanho.png') }}" class="img-fluid d-block mx-auto" alt="">
                 <h3 class="mt-4 mb-2">Escolha o Tamanho</h3>
                 <p class="var">São três tamanhos diferentes, um deles tem a quantidade certa para você</p>
                <hr class="d-md-none d-lg-none">                
              </div>
              <div class="col-md-4 mb-lg-3 mb-5 text-center">
                 <img src="{{ asset('img/icon-periodo.png') }}" class="img-fluid d-block mx-auto" alt="">
                 <h3 class="mt-4 mb-2">Defina o dia e o período</h3>
                 <p class="var">Você escolhe o dia da semana e o período que deseja receber o seu kit</p>
                <hr class="d-md-none d-lg-none">                
              </div>
              <div class="col-md-4 mb-lg-3 mb-5 text-center">
                 <img src="{{ asset('img/icon-casa.png') }}" class="img-fluid d-block mx-auto" alt="">
                 <h3 class="mt-4 mb-2">Minha Feira em Casa</h3>
                 <p class="var">Pronto! Desfrute dos seus produtos com conforto e mais qualidade de vida</p>
                <hr class="d-md-none d-lg-none">                                 
              </div>
          </div>
          <div class="row mt-lg-5 justify-content-md-center">
              <div class="col-md-3">
                  <a href="#sobre-kits" class="button button-primary">Sobre os Kits</a>
              </div>
          </div>
      </div>
  </section>
  <section class="sobre-kits" id="sobre-kits">
      <div class="container">
          <div class="row mb-4">
              <div class="col-md-12">
                  <h2 class="var">Sobre os Kits</h2>                    
              </div>
          </div>
          <div class="row">
              <div class="col-md-6">
                  <p class="var-white">A Minha Feira em Casa tem a missão de oferecer aos clientes a praticidade e comodidade de receber produtos de hortifruti (frutas, verduras, legumes e hortaliças) em casa semanalmente, na quantidade escolhida e com qualidade, em formato de kits.</p>
                  <p class="var-white">Nossos kits possuem alimentos básicos da sua lista de compras, além de produtos da estação, sempre selecionados e prontos para o consumo, ideal para quem busca praticidade e organização no dia-a-dia. Troque as idas à feira ou ao mercado por momentos de lazer e descanso.</p>
              </div>
          </div>
          <div class="row mt-2">
              <div class="col-md-4">
                  <a href="{{url('kits')}}" class="button button-opacity">Conheça Nossos Kits</a>
              </div>
          </div>
      </div>
  </section>
  <section class="planos">
      <div class="grey-circle"></div>
      <div class="container">
          <div class="row mb-5">
              <div class="col-md-12 text-center">
                  <h2>Escolha o kit ideal para você</h2>
                  <p class="var mt-4">São três tamanhos diferentes de kits, escolha o que melhor se encaixa no seu dia-a-dia</p>
              </div>
          </div>
          <form method="POST" id="form-kit" action="{{route('informe-cep-assinatura')}}">
              {{csrf_field()}}
              <input type="hidden" name="kit" value="tamanho-p-tab" />
          </form>
          <div class="row p-10 align-items-center">
              <div class="col-md-4">
                  <div class="content p white-bg">
                      <img src="{{ asset('img/top-plano-green.png') }}" class="img-fluid position-absolute" alt="">
                      <img src="{{ asset('img/icon-p.png') }}" class="img-fluid position-absolute" alt="">
                      <div class="title text-center mx-auto">
                          <p>Kit ideal para <span>02</span> pessoas</p>
                      </div>
                      <div class="body mt-4">
                          <h4 class="text-center">Quantidade Semanal</h4>
                          <ul class="list-unstyled mt-3">
                              <li><span>03</span>Aqui vai um item da cesta</li>
                              <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                              <li><span>01</span>item dessa cesta lorem lorem</li>
                              <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                              <li><span>03</span>Outro item da cesta</li>
                          </ul>
                      </div>
                      <div class="footer text-center">
                          <p><span>R$</span>{{ substr($data[0]->price, 0, -3) }}<sup>,00</sup><small>mês</small></p>
                          <button type="button" id="1" class="button button-secondary kit">Assinar Agora</button>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="content m white-bg">
                      <img src="{{ asset('img/top-plano-orange.png') }}" class="img-fluid position-absolute" alt="">
                      <img src="{{ asset('img/icon-m.png') }}" class="img-fluid position-absolute" alt="">
                      <img src="{{ asset('img/icon-sale.png') }}" class="img-fluid position-absolute" alt="">
                      <div class="title text-center mx-auto">
                          <p>Kit ideal para <span>03</span> pessoas</p>
                      </div>
                      <div class="body mt-4">
                          <h4 class="text-center">Quantidade Semanal</h4>
                          <ul class="list-unstyled mt-3">
                              <li><span>03</span>Aqui vai um item da cesta</li>
                              <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                              <li><span>01</span>item dessa cesta lorem lorem</li>
                              <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                              <li><span>03</span>Outro item da cesta</li>
                          </ul>
                      </div>
                      <div class="footer text-center">
                          <p><span>R$</span>{{ substr($data[1]->price, 0, -3) }}<sup>,00</sup><small>mês</small></p>
                          <button type="button" id="2" class="button button-tertiary kit">Assinar Agora</button>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="content g white-bg">
                      <img src="{{ asset('img/top-plano-green.png') }}" class="img-fluid position-absolute" alt="">
                      <img src="{{ asset('img/icon-g.png') }}" class="img-fluid position-absolute" alt="">
                      <div class="title text-center mx-auto">
                          <p>Kit ideal para <span>04</span> pessoas</p>
                      </div>
                      <div class="body mt-4">
                          <h4 class="text-center">Quantidade Semanal</h4>
                          <ul class="list-unstyled mt-3">
                              <li><span>03</span>Aqui vai um item da cesta</li>
                              <li><span>02</span>Aqui vai outro item dessa cesta lorem</li>
                              <li><span>01</span>item dessa cesta lorem lorem</li>
                              <li><span>06</span>Outro item dessa cesta lorem lorem</li>
                              <li><span>03</span>Outro item da cesta</li>
                          </ul>
                      </div>
                      <div class="footer text-center">
                          <p><span>R$</span>{{ substr($data[2]->price, 0, -3) }}<sup>,00</sup><small>mês</small></p>
                          <button type="button" id="3" class="button button-secondary kit">Assinar Agora</button>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <section class="faq">
      <div class="orange-circle"></div>
      <div class="container">
          <div class="row mb-5">
              <div class="col-md-12 text-center">
                  <h2 class="var">Tire suas dúvidas</h2>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <div id="accordion" class="accordion">
                    <div class="card">
                      <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                          <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Vocês entregam na minha região?
                          </button>
                        </h5>
                      </div>

                      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                          Consulte a <a href="">área de entrega aqui</a> colocando o CEP do endereço desejado.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Qual o valor do frete?
                          </button>
                        </h5>
                      </div>
                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                         Consulte a <a href="">área de entrega aqui</a> colocando o CEP do endereço desejado.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            De onde vem os produtos da Minha Feira em Casa?
                          </button>
                        </h5>
                      </div>
                      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                         Consulte a <a href="">área de entrega aqui</a> colocando o CEP do endereço desejado.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="headingFour">
                        <h5 class="mb-0">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Como funciona a entrega?
                          </button>
                        </h5>
                      </div>
                      <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                         Consulte a <a href="">área de entrega aqui</a> colocando o CEP do endereço desejado.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" id="headingFive">
                        <h5 class="mb-0">
                          <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Não encontrei minha dúvida, o que fazer?
                          </button>
                        </h5>
                      </div>
                      <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                         Consulte a <a href="">área de entrega aqui</a> colocando o CEP do endereço desejado.
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
          </div>
          <div class="row mt-5 justify-content-md-center">
              <div class="col-md-4">
                  <a href="" class="button button-primary">Ver todas as dúvidas</a>
              </div>
          </div>
      </div>
  </section>
<!--   <section class="depoimentos">
      <div class="container">
          <div class="row mb-5">
              <div class="col-md-12 text-center">
                  <h2>O que nossos clientes dizem</h2>
                  <p class="var mt-4">Veja os depoimentos de quem já recebe a feira em casa</p>
              </div>
          </div>
      </div>
  </section> -->
  <section class="news">
      <div class="grey-circle"></div>
      <div class="container">
          <div class="row">
              <div class="col-md-12 text-center">
                  <h2>Fique por dentro das novidades</h2>
                  <p class="mt-4">Cadastre-se e receba cupons de desconto, promoções, receitas e mais novidades em primeira mão.</p>
              </div>
          </div>
          <form class="mt-2">
              <div class="form-row justify-content-center">
                  <div class="col-md-4 mb-3">
                      <input type="text" class="form-control" placeholder="Insira seu CEP">
                  </div>
                  <div class="col-md-2 mb-3">
                      <a href="#" class="button button-primary">Cadastrar</a>
                  </div>
              </div>
          </form>
      </div>
  </section>
@endsection
@section('Footer')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h5 class="var mb-3">Institucional</h5>
        <ul class="list-unstyled">
          <li><a href="{{url('sobre-nos')}}">Sobre Nós</a></li>
          <li><a href="{{url('termos-condicoes')}}">Termos e Condições</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Atendimento</h5>
        <ul class="list-unstyled">
          <li><a href="{{('fale-conosco')}}">Fale Conosco</a></li>
          <li><a href="{{url('perguntas-frequentes')}}">Perguntas Frequentes</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Formas de pagamento</h5>
        <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Redes sociais</h5>
        <ul class="list-unstyled">
          <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
          <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>   
  <section class="copyright mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
        </div>
      </div>
    </div>
  </section>   
</footer>
@endsection
@push('scripts')
  <!-- Scripts -->
  <script type="text/javascript" src="{{ PagSeguro::getUrl()['javascript'] }}"></script>
  <script>
      $(document).ready(function(){
        $('button.kit').on('click', function(e){
            $('input[name="kit"]').val(e.target.id);
            $('#form-kit').submit();
          })
           $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
  </script>
@endpush
