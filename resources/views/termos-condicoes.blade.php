@extends('layouts.interna')
@section('Top')
  <section class="header header-interna">
    <header>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <nav class="navbar navbar-expand-lg navbar-light">
              <a class="navbar-brand" href="#"><img src="{{ asset('img/logo-final.png') }}" class="img-fluid" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{url('kits')}}">Box</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{url('sobre-nos')}}">Sobre Nós</a>
                  </li>  
                  <li class="nav-item">
                    <a class="nav-link" href="{{url('como-funciona')}}">Como Funciona</a>
                  </li> 
                  <li class="nav-item">
                    <a class="nav-link login" href="{{url('login')}}">Entrar</a>
                  </li>                             
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>
  </section>
@endsection
@section('Content')  
  <section class="title-interna">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2>Termos e condições</h2>
          <p>Entenda como levamos o melhor para sua casa!</p>
        </div>
      </div>
    </div>
  </section>  
  <section class="sobre-interna">
  	<div class="grey-circle"></div>
  	<div class="container">
  		<div class="row">
  			<div class="col-md-7">
  				<h3>Título dos termos</h3>
  				<p>É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de “Conteúdo aqui, conteúdo aqui”, fazendo com que ele tenha uma aparência similar a de um texto legível. Muitos softwares de publicação e editores de páginas na internet agora usam Lorem Ipsum como texto-modelo padrão, e uma rápida busca por ‘lorem ipsum’ mostra vários websites ainda em sua fase de construção. Várias versões novas surgiram ao longo dos anos, eventualmente por acidente, e às vezes de propósito (injetando humor, e coisas do gênero).</p>
  				<p>É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver examinando sua diagramação. A vantagem de usar Lorem Ipsum é que ele tem uma distribuição normal de letras, ao contrário de “Conteúdo aqui, conteúdo.</p>
  			</div>
  			<div class="col-md-4 offset-1">
				<form class="form-news mt-5">
					<div class="head text-center">
						<div class="orange-circle"></div>
						<p>Receba nossas novidades!</p>
					</div>
					<div class="content">
						<p class="text-center mb-4">
							Cadastre-se e receba cupons de desconto, promoções, receitas e muito mais!
						</p>
						<div class="form-group">
							<input type="email" class="form-control mb-4" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Insira seu e-mail">
						</div>
						<div class="form-group">
							<input type="text" class="form-control mb-4" id="exampleInputPassword1" placeholder="Insira seu nome">
						</div>
						<button type="submit" class="button button-primary">Quero Receber</button>
					</div>					
				</form>
  			</div>
  		</div>
  	</div>
  </section> 
  <section class="sobre-box">
  	<div class="orange-circle"></div>
  	<div class="container">
  		<div class="row mb-3">
  			<div class="col-md-6 offset-3 text-center">
  				<h2 class="var">Nossos box</h2>
  				<h4 class="var">Que tal receber sua feira em casa toda semana?</h4>
  			</div>
  		</div>
  		<div class="row">
  			<div class="col-md-4 offset-4">
  				<a href="/kits" class="button button-tertiary">Conhecer</a>
  			</div>
  		</div>
  	</div>
  </section>
@endsection
@section('Footer')
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h5 class="var mb-3">Institucional</h5>
        <ul class="list-unstyled">
          <li><a href="/sobre-nos">Sobre Nós</a></li>
          <li><a href="/termos-condicoes">Termos e Condições</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Atendimento</h5>
        <ul class="list-unstyled">
          <li><a href="/fale-conosco">Fale Conosco</a></li>
          <li><a href="/perguntas-frequentes">Perguntas Frequentes</a></li>
        </ul>
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Formas de pagamento</h5>
        <img src="{{ asset('img/icons-pagamento.png') }}" class="img-fluid d-block" alt="">
      </div>
      <div class="col-md-3">
        <h5 class="var mb-3">Redes sociais</h5>
        <ul class="list-unstyled">
          <li><a href=""><i class="fa fa-fw fa-facebook" aria-hidden="true"></i>Facebook</a></li>
          <li><a href=""><i class="fa fa-fw fa-instagram" aria-hidden="true"></i>Instagram</a></li>
        </ul>
      </div>
    </div>
  </div>   
  <section class="copyright mt-3">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="var mb-0">Desenvolvido por: <a href="http://ad4pixels.com.br/" target="_blank"><img src="{{ asset('img/logo-4p.png') }}" class="img-fluid" alt=""></a></p>
        </div>
      </div>
    </div>
  </section>   
</footer>
@endsection
@push('scripts')
  <!-- Scripts -->
  <script>
      $(document).ready(function(){
           $('input[name="cep"]').mask('00000-000',{clearIfNotMatch: true});
          $("form").submit(function() {
              $('input[name="cep"]').unmask();
          });
      });
  </script>
@endpush
