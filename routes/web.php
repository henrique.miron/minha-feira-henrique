<?php
header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Autenticação
Auth::routes();

//Autenticação com facebook
Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');

//Rotas do sistema
Route::get('/', 'HomeController@index')->name('home');
Route::get('/kits', 'HomeController@kits');
Route::any('/kit-alterar', 'KitController@alterar')->name('kit-alterar');
Route::get('/sobre-nos', 'HomeController@sobre');
Route::get('/como-funciona', 'HomeController@funciona');
Route::get('/termos-condicoes', 'HomeController@termos');
Route::get('/consulta-cep', 'FreightCostController@consultacep')->name('consulta-cep');
Route::get('/informe-cep-assinatura', 'HomeController@cep')->name('informe-cep-assinatura');
Route::post('/desconto', 'CouponController@desconto')->name('desconto');
Route::post('/verificar-login', 'OrderController@checklogintoorder')->name('check-login-to-order');
Route::get('/login-assinatura', 'HomeController@loginassinatura');
Route::get('/get-cep/{id}', 'FreightCostController@getCep');
Route::post('/finalizar-entrega', 'OrderController@completeOrder')->name('complete-order');
Route::post('/alterar-cep', 'FreightCostController@alterarCep')->name('alterar-cep');
Route::post('/registrar-usuario', 'RegisterController@create')->name('registrar-usuario');
Route::post('/finalizar-compra', 'OrderController@store')->name('finalizar-compra');

Route::middleware('checkOrder')->group(function(){
    Route::post('/checkout-logado-assinatura', 'OrderController@checkoutlogadoassinatura')->name('checkout-logado-assinatura');
    Route::any('/checkout-assinatura', 'HomeController@checkoutassinatura')->name('checkout-assinatura');
    Route::get('/selecione-periodo-assinatura', 'HomeController@selecioneassinatura');
    Route::post('/informe-cep-assinatura', 'KitController@selecioneKit')->name('informe-cep-assinatura');
});
Route::middleware('auth')->group(function(){
    Route::get('/painel-home', 'HomeController@painelhome');
    Route::get('/checkout-desconto-assinatura', 'HomeController@checkoutdescontoassinatura');
    Route::get('/meu-box', 'PainelController@meubox');
    Route::get('/periodo-box', 'PainelController@periodobox');
    Route::get('/endereco-cadastrado', 'PainelController@enderecobox');
    Route::get('/cartao-cadastrado', 'PainelController@cartaobox');
    Route::get('/historico-cobranca', 'PainelController@cobrancabox');
    Route::get('/historico-entrega', 'PainelController@entregabox');
    Route::post('/alterar-assinatura', 'OrderController@alterar')->name('alterar-assinatura');
    Route::get('/finalizar-assinatura', 'HomeController@finalizarassinatura');
    Route::get('/cancelar-assinatura/{id}', 'OrderController@cancel');
    // Route::get('/usuarios-pedidos', 'PainelController@usuarios');
    
    Route::any('/cartao-cadastrar', 'CardController@newCard')->name('cartao-cadastrar');
    Route::post('/cartao-principal', 'CardController@alterar')->name('cartao-principal');
    Route::get('/cartao-excluir/{id}', 'CardController@excluir')->name('cartao-excluir');
    Route::any('/endereco-editar', 'AddressController@update')->name('endereco-editar');
});
Route::get('/checar-dados', 'HomeController@checkoutlogadoassinatura')->middleware('auth', 'checkOrder');


Route::post('/lead/novo','LeadController@add');

Route::middleware(['auth', 'admin'])->group(function(){
    Route::get('/admin-home', 'HomeController@adminhome');
    Route::get('/admin-configbox', 'PainelController@adminconfigbox');
    Route::get('/admin-relatorios', 'PainelController@adminrelatorios');
    Route::get('/admin-usuarios', 'PainelController@usuarios');
    Route::get('/admin-cep', 'AdminController@cep');
    Route::post('/inserir-cep', 'FreightCostController@inserirCep')->name('inserir-cep');
    Route::any('/pedido/{id}', 'PainelController@pedido');
    Route::get('/pedido-detalhe/{id}', 'PainelController@pedidoDetalhe');
    Route::post('/editar-kit', 'KitController@update')->name('editar-kit');
    Route::get('/deletar-kit/{id}', 'KitController@delete')->name('deletar-kit');
    Route::post('/cadastrar-kit', 'KitController@store')->name('cadastrar-kit');
});

// Route::middleware('cors')->group(function(){
    Route::post('/pagseguro-notificacao', 'PagseguroController@notificacao')->name('pagseguro-notificacao');
// });




